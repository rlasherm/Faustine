# Faustine

The **Faustine** platform is the LHS' EM fault injection platform.

## What you can find in this repository

- Code: all the code developed for this platform. Other projects (bench management, target applications, ...) may be linked without being included in this git.
- Communication: our data to communicate around the platform (e.g. poster, ...).
- Documentation: datacheets, training, ...
- Safety: all safety related documents.
