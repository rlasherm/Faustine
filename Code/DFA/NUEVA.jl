
include("AES.jl")

k = [0x2B, 0x7E, 0x15, 0x16, 0x28, 0xAE, 0xD2, 0xA6, 0xAB, 0xF7, 0x15, 0x88, 0x09, 0xCF, 0x4F, 0x3C]

function shannon_entropy(s::Array{UInt8, 1})
    acc::Float64 = 0.0
    counters = zeros(UInt64, 256)

    for val in s
        counters[1+val] += 1
    end

    probas::Array{Float64, 1} = counters / size(s, 1)

    for p in probas
        if p > 0
            acc += p * log2(p)
        end
    end

    return -acc
end

function revertSBox(c::UInt8, k::UInt8)
    return invsbox(xor(c, k))
end

function compute_error(c::UInt8, faulty_c::UInt8, k::UInt8)
    if c == faulty_c
        return 0
    else
        return xor(revertSBox(c, k), revertSBox(faulty_c, k))
    end
end

function compute_error_entropy(c::Array{UInt8, 1}, faulty_c::Array{UInt8, 1}, k::UInt8)
    s = size(c, 1)
    errors = zeros(UInt8, s)

    for i =1:s
        errors[i] = compute_error(c[i], faulty_c[i], k)
    end

    return shannon_entropy(errors)
end

function NUEVA_single(c::Array{UInt8, 1}, faulty_c::Array{UInt8, 1})

    entropies = zeros(256)

    for k = 0:255
        entropies[k+1] = compute_error_entropy(c, faulty_c, UInt8(k))
    end

    return entropies

end
