## Secure-IC Analyzer

Example code to add a new target to Secure-IC Analyzer software.

## STM32 AES Example

An application example that run an AES on a STM32VALUEDISCOVERY board.

## Triggy

An IP that can be put on Digilent's Arty FPGA board to provide synchronization and reseting services.
