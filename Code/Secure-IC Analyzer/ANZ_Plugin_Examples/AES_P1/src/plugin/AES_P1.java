/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package plugin;

import AnalyzerAPI.Interfaces.TargetInterface;
import Crypto.AESTools.AES;
import FoundationTools.FoundationTools;
import java.util.ArrayList;
import java.util.HashMap;

/**
 *
 * @author ANZ-User
 */
public class AES_P1 implements TargetInterface {
    
    private boolean status = true;
    private String statusMsg = "";
    private byte[] saved_key = new byte[16];

    @Override
    public void configure(String string) {
        
    }

    @Override
    public ArrayList<byte[]> getSetKeyCommand(byte[] bytes) {
        if(bytes != null)
            saved_key = bytes.clone();
        byte[] cmd = new byte[17];
        cmd[0] = 'k';
        for(int i = 0; i < 16 && i < bytes.length; i++) {
            cmd[i+1] = bytes[i];
        }
        ArrayList<byte[]> res =  new ArrayList<byte[]>();
        res.add(cmd);
        return  res;
    }

    @Override
    public ArrayList<byte[]> getProcessDataCommand(byte[] bytes) {
        byte[] cmd = new byte[17];
        cmd[0] = 'p';
        for(int i = 0; i < 16 && i < bytes.length; i++) {
            cmd[i+1] = bytes[i];
        }
        ArrayList<byte[]> res =  new ArrayList<byte[]>();
        res.add(cmd);
        byte[] cmd2 = new byte[1];
        cmd2[0] = 'g';
        res.add(cmd2);
        return  res;
    }

    @Override
    public ArrayList<byte[]> getExpectedResult(byte[] bytes) {
        if(bytes == null  || saved_key == null)
        {
            return null;
        }
        //Logger.getLogger(AES_P1.class.getName()).log(Level.INFO, "Plaintext: " + FoundationTools.byteArray2HexString(bytes, false));
        //Logger.getLogger(AES_P1.class.getName()).log(Level.INFO, "Key: " + FoundationTools.byteArray2HexString(saved_key, false));
        byte[] AES128 = AES.AES128(bytes, saved_key);
        ArrayList<byte[]> res =  new ArrayList<byte[]>();
        res.add(AES128);
        return  res;
        /*byte[] empty = new byte[]{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
        ArrayList<byte[]> res =  new ArrayList<byte[]>();
        res.add(empty);
        return  res;*/
    }

    @Override
    public ArrayList<byte[]> getGetResultCommand() {
        byte[] cmd = new byte[1];
        cmd[0] = 'c';
        ArrayList<byte[]> res =  new ArrayList<byte[]>();
        res.add(cmd);
        return  res;
    }

    @Override
    public ArrayList<byte[]> getInitCommand(byte[] bytes) {
        return new ArrayList<byte[]>();
    }

    @Override
    public ArrayList<byte[]> getCloseCommand(byte[] bytes) {
        return new ArrayList<byte[]>();
    }

    @Override
    public byte[] extractResult(byte[] bytes) {
        return bytes;
    }

    @Override
    public byte[] getRandomInput() {
        return FoundationTools.createRandomByteArray(16);
    }

    @Override
    public boolean getStatus() {
        return status;
    }

    @Override
    public String getStatusMsg() {
        return statusMsg;
    }

    @Override
    public HashMap<String, Object> getProperties() {
        HashMap prop = new HashMap<String, Object>(5);
        prop.put(TargetInterface.PROPERTIES_KEY_CRYPTO          , "AES");
        prop.put(TargetInterface.PROPERTIES_KEY_DESCRIPTION     , "AES PROTOCOL 1");
        prop.put(TargetInterface.PROPERTIES_KEY_INPUTSIZE       , 16);
        prop.put(TargetInterface.PROPERTIES_KEY_OUTPUTSIZE      , 16);
        prop.put(TargetInterface.PROPERTIES_KEY_KEYSIZE         , 16);
        prop.put(TargetInterface.PROPERTIES_KEY_IVSIZE          , 0);
        prop.put(TargetInterface.PROPERTIES_KEY_CRYPTOREFERENCE , true);
        prop.put(TargetInterface.PROPERTIES_KEY_HARDCODED       , false);
        
        status = true;
        statusMsg = "OK";
        return prop;
    }

    @Override
    public void close() {
        
    }
    
}
