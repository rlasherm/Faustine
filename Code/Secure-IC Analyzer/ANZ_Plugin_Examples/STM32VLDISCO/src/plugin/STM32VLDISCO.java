/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package plugin;

import AnalyzerAPI.COMManager.COMPortDescriptor.COMDataBits;
import AnalyzerAPI.COMManager.COMPortDescriptor.COMParity;
import AnalyzerAPI.COMManager.COMPortDescriptor.COMStopBits;
import AnalyzerAPI.COMManager.*;
import AnalyzerAPI.Interfaces.AdvancedTestPlatformInterface;
import AnalyzerAPI.Interfaces.TargetInterface;
import FoundationTools.FoundationTools;
import java.io.*;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author ANZ-User
 */
public class STM32VLDISCO implements AdvancedTestPlatformInterface {
    

    
    //Members
    private COMPortHandle handle = null;
    private TargetInterface target = null;
    private HashMap<String, Object> prop = new HashMap<String, Object>();
    private boolean status = true;
    private String statusMsg = "";
    private Openocd_server openocd_server = null;
    private Thread server_thread = null;
    
    private String telnet_reset_cmd = "reset run";
    
    public STM32VLDISCO() {
        prop.put(AdvancedTestPlatformInterface.PROPERTIES_KEY_BAUDRATE, 115200);
        prop.put(AdvancedTestPlatformInterface.PROPERTIES_KEY_DATABITS, COMDataBits.EIGHT);
        prop.put(AdvancedTestPlatformInterface.PROPERTIES_KEY_PARITY, COMParity.NONE);
        prop.put(AdvancedTestPlatformInterface.PROPERTIES_KEY_STOPBITS, COMStopBits.ONE);
        
        status = true;
        statusMsg = "OK";
    }
    
    private boolean openocd_reset() {
        boolean success = false;
        
        try {
            Thread.currentThread().sleep(500);
            Socket openocd_client = new Socket("localhost", 4444);
      
            BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(openocd_client.getOutputStream()));
            writer.write(telnet_reset_cmd);
            writer.newLine();
            writer.flush();
            Thread.currentThread().sleep(500);
            writer.close();
            openocd_client.close();
           
           
            Logger.getLogger(STM32VLDISCO.class.getName()).log(Level.INFO, "RESET!");

            success = true;
        } catch (IOException ex) {
            Logger.getLogger(STM32VLDISCO.class.getName()).log(Level.SEVERE, null, ex);
        } catch (InterruptedException ex) {
            Logger.getLogger(STM32VLDISCO.class.getName()).log(Level.SEVERE, null, ex);
        }
      
        
        if(this.handle != null) {
            handle.lock();
            byte[] cmd = new byte[2];
            cmd[0] = 'r';
            cmd[1] = 9;
            try {
                handle.getOutputStream().write(cmd);
            } catch (IOException ex) {
                Logger.getLogger(STM32VLDISCO.class.getName()).log(Level.SEVERE, null, ex);
            }
            handle.unlock();
        }
        
        /*if(openocd_reader != null)
        {
            try {
                String line = openocd_reader.readLine();
                Logger.getLogger(STM32VLDISCO.class.getName()).log(Level.INFO, "telnet answer: " + line);
            } catch (IOException ex) {
                Logger.getLogger(STM32VLDISCO.class.getName()).log(Level.SEVERE, null, ex);
            }
        }*/
        
        return success;
    }

    @Override
    public boolean resetTarget() {
        return openocd_reset();
    }

    @Override
    public boolean resetPlatform() {
        return openocd_reset();
    }
    

    @Override
    public void initialize(COMPortHandle comph, String string) {
        this.handle = comph;
        /*this.openocd_server = new Openocd_server();
        this.server_thread = new Thread(openocd_server);
        this.server_thread.start();*/
        try {
            Thread.currentThread().sleep(500);//wait for server launch
        } catch (InterruptedException ex) {
            Logger.getLogger(STM32VLDISCO.class.getName()).log(Level.SEVERE, null, ex);
        }        
    }

    @Override
    public void initialize(String string, String string1) {
        throw new UnsupportedOperationException("Deprecated.");
    }

    @Override
    public void configureTarget(String string) {
        this.target.configure(string);
    }

    @Override
    public void attachTarget(TargetInterface ti) {
        this.target = ti;
    }

    @Override
    public boolean checkTarget() {
        return this.target != null;
    }
    
    private Byte[] read_line(InputStream in) {
        ArrayList<Byte> res = new ArrayList<Byte>();
        boolean end = false;
        while(!end) {
            int read = 0;
            try {
                read = in.read();
            } catch (IOException ex) {
                Logger.getLogger(STM32VLDISCO.class.getName()).log(Level.SEVERE, null, ex);
            }
            if(read == (byte)'\n')
            {
                end = true;
            }
            else {
                res.add((byte)read);
            }
        }
        
        Byte[] data = new Byte[res.size()];
        res.toArray(data);
        return data;
    }

    @Override
    public void initTarget(byte[] bytes) {
        send_commands(this.target.getInitCommand(bytes));
    }
    
    private void send_commands(ArrayList<byte[]> cmds)
    {
        if(cmds.isEmpty())
        {
            return;
        }
        this.handle.lock();
        this.handle.flushInputStream();
        OutputStream outputStream = this.handle.getOutputStream();
        
        for(byte[] cmd : cmds)
        {
            if(cmd == null)
            {
                break;
            }
            //Logger.getLogger(STM32VLDISCO.class.getName()).log(Level.INFO, FoundationTools.byteArray2HexString(cmd, false));
            try {
                outputStream.write(cmd);
            } catch (IOException ex) {
                Logger.getLogger(STM32VLDISCO.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        this.handle.unlock();
    }
    
    public byte[] send_received_command(ArrayList<byte[]> cmds, int answer_len)
    {
        if(cmds.isEmpty())
        {
            return null;
        }
        this.handle.lock();
        this.handle.flushInputStream();
        IOStream io = new IOStream(this.handle.getInputStream(), this.handle.getOutputStream());
        
        for(byte[] cmd : cmds)
        {
            if(cmd == null)
            {
                break;
            }
            //Logger.getLogger(STM32VLDISCO.class.getName()).log(Level.INFO, FoundationTools.byteArray2HexString(cmd, false));
            try {
                io.write(cmd);
            } catch (IOException ex) {
                Logger.getLogger(STM32VLDISCO.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
       
        byte[] ans = new byte[answer_len];
        try {
            io.read(ans);
        } catch (IOException ex) {
            Logger.getLogger(STM32VLDISCO.class.getName()).log(Level.SEVERE, null, ex);
        }
        //Logger.getLogger(STM32VLDISCO.class.getName()).log(Level.INFO, "Answer: " + FoundationTools.byteArray2HexString(ans, false));
        
        this.handle.unlock();
        
        return ans;
    }

    @Override
    public void closeTarget(byte[] bytes) {
       send_commands(this.target.getCloseCommand(bytes));
    }

    @Override
    public void setKey(byte[] bytes) {
        //Logger.getLogger(STM32VLDISCO.class.getName()).log(Level.INFO, "Set key");
        send_commands(this.target.getSetKeyCommand(bytes));
    }
    

    @Override
    public void processData(byte[] bytes) {
        //Logger.getLogger(STM32VLDISCO.class.getName()).log(Level.INFO, "Process Data");
        send_commands(this.target.getProcessDataCommand(bytes));
    }
    
   
    @Override
    public byte[] getResult() {
        //Logger.getLogger(STM32VLDISCO.class.getName()).log(Level.INFO, "Get Result");
        return send_received_command(this.target.getGetResultCommand(), 16);//BAD!!!!!
    }

    @Override
    public boolean getStatus() {
        return this.status;
    }

    @Override
    public String getStatusMsg() {
        return this.statusMsg;
    }

    @Override
    public HashMap<String, Object> getProperties() {
        return this.prop;
    }

    @Override
    public void close() {
        this.handle.close();
        
        if(this.openocd_server != null)
        {
            this.server_thread.interrupt();
            this.openocd_server.close();
        }
    }

    public static void main(String[] args) {
        try {
            COMLogger.setup();
        } catch (IOException ex) {
            System.out.println("Logger error");
        }
        COMManager mngr = null;
        try
        {
            mngr = new COMManager();
        }
        catch(Exception ex) {
            
        }
        COMPortDescriptor desc = new COMPortDescriptor("COM16", COMParity.NONE, COMStopBits.ONE, COMDataBits.EIGHT, 9600);
        COMPortHandle handle = mngr.connectTo(desc);
        
        AES_P1 target = new AES_P1();
        STM32VLDISCO stm = new STM32VLDISCO();
        
        stm.attachTarget(target);
        stm.configureTarget("");
        stm.initialize(handle, "");
        
        byte[] key = new byte[]{0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15};
        byte[] plaintext = new byte[]{(byte)0x00, (byte)0x11, (byte)0x22, (byte)0x33, (byte)0x44, (byte)0x55, (byte)0x66, (byte)0x77, (byte)0x88, (byte)0x99, (byte)0xAA, (byte)0xBB, (byte)0xCC, (byte)0xDD, (byte)0xEE, (byte)0xFF};
        
        stm.setKey(key);
        stm.processData(plaintext);
        byte[] ciphertext = stm.getResult();
        
        System.out.println("Ciphertext: " + FoundationTools.byteArray2HexString(ciphertext, true));
    }
}
