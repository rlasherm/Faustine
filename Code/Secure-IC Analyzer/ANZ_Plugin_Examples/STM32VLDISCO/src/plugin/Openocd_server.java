package plugin;


import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.logging.Level;
import java.util.logging.Logger;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author ANZ-User
 */
public class Openocd_server implements Runnable {

    private String openocd_path = "C:\\Program Files\\openocd-0.9.0\\";
    private String openocdString_cmd = openocd_path + "bin-x64\\openocd.exe -f \"" + openocd_path + "scripts\\board\\stm32vldiscovery.cfg\"";
    private Process openocd_server = null;
    
    @Override
    public void run() {
        try {
            this.openocd_server = Runtime.getRuntime().exec(openocdString_cmd);
            
            Logger.getLogger(Openocd_server.class.getName()).log(Level.INFO, "OpenOCD:");
            
            BufferedReader reader = new BufferedReader(new InputStreamReader(this.openocd_server.getErrorStream()));
            
            while(this.openocd_server != null)//infinite loop for server
            {
                /*while(reader.ready())
                {
                    Logger.getLogger(Openocd_server.class.getName()).log(Level.INFO, "OpenOCD: " + reader.readLine());
                }*/
                
            }
            
        } catch (IOException ex) {
            
        }
    }

    public void close() {
        if(this.openocd_server != null) {
            this.openocd_server.destroy();
            this.openocd_server = null;
        }
    }
}
