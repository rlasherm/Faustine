/**
* @Author: hélène, ronan
* @Date:   12-09-2016
* @Email:  helene.lebouder@inri.fr, ronan.lashermes@inria.fr
* @Last modified by:   ronan
* @Last modified time: 19-09-2016
* @License: GPL
*/

//#define _POSIX_C_SOURCE 199309L
#include "stm32f1xx_hal.h"
//#include "core_cm4.h"
//void Trigger(int temps);
//void delayUS_DWT(uint32_t us);
#include "trigger.h"
#include <string.h>
static GPIO_InitTypeDef  GPIO_InitStruct;
UART_HandleTypeDef UartHandle;
char text[256];
int CYCLES_PER_LOOP=8;
void asm_reg(int, int);
//int *regtrig=0x40011000;
//-----------------------------------------------------------------------------------------------------------------------------
// Trigger
//-----------------------------------------------------------------------------------------------------------------------------
void wait_cycles( uint32_t n ) {
    uint32_t l = n/CYCLES_PER_LOOP;
    asm volatile( "0:" "SUBS %[count], 1;" "BNE 0b;" :[count]"+r"(l) );
}

uint32_t getUs(void) {
	uint32_t usTicks = HAL_RCC_GetSysClockFreq() / 1000000;
	register uint32_t ms, cycle_cnt;
	do {
		ms = HAL_GetTick();
		cycle_cnt = SysTick->VAL;
	} while (ms != HAL_GetTick());
	return (ms * 1000) + (usTicks * 1000 - cycle_cnt) / usTicks;
}
 

void delayUs(uint16_t micros) {
	uint32_t start = getUs();
	while (getUs()-start < (uint32_t) micros) {
		asm("nop");
	}
}


void Trigger(uint32_t temps)
{	
	//int *regtrig=43444444;
	//asm("mov R1,#0x43444444;" "mov R2,#0x40011000;" "str R1,[R2];");
	asm_reg(0x40011000,0x43444444);
	//sprintf(text, "start trig1\n");
        //HAL_UART_Transmit(&UartHandle, (uint8_t *)text, strlen(text), 0xFFFF);	
	HAL_GPIO_WritePin(GPIOC, GPIO_PIN_6, GPIO_PIN_SET);
	//sprintf(text, "start trig2\n");
        //HAL_UART_Transmit(&UartHandle, (uint8_t *)text, strlen(text), 0xFFFF);	
	//wait_cycles(temps);
	//sprintf(text, "stop trig1\n");
        //HAL_UART_Transmit(&UartHandle, (uint8_t *)text, strlen(text), 0xFFFF);	
	HAL_GPIO_WritePin(GPIOC, GPIO_PIN_6, GPIO_PIN_RESET);
	//sprintf(text, "stop trig2\n");
        //HAL_UART_Transmit(&UartHandle, (uint8_t *)text, strlen(text), 0xFFFF);
	return;
}
