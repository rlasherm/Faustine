.section .text
.globl asm_code
.globl asm_add
.globl asm_branch
.globl asm_eor
.globl asm_mov
.globl asm_lsl
.globl asm_ldr
.globl asm_str
.globl asm_reg

#.type asm_code,@function


asm_eor:
	mov R3, R0
	nop
	nop
	nop
	nop
	nop
	nop
	eor R0, R2 
	nop
	nop
	nop
	nop
	nop
	nop
	mov R4, PC
	add R4, R4, #5
	cmp R0, R1
	bne testBP
	bx lr

asm_lsl:
	mov R3, R0
	nop
	nop
	nop
	nop
	nop
	nop
	lsl R2, R0, #8
	nop
	nop
	nop
	nop
	nop
	nop
	mov R4, PC
	add R4, R4, #5
	cmp R2, R1
	bne testBP
	mov R0, R2
	bx lr

asm_add:
	mov R3, R7
	nop
	nop
	nop
	nop
	nop
	nop
	add R2,R0,#6
	nop
	nop
	nop
	nop
	nop
	nop
	mov R4, PC
	add R4, R4, #5
	cmp R2, R1
	bne testBP
	mov R0,R2
	bx lr

asm_branch:
	mov R5, PC
	add R5, R5, #29
	nop
	nop
	nop
	nop
	nop
	nop
	cmp R0, R1
	nop
	nop
	nop
	nop
	nop
	nop
	beq testBranch
	nop
	nop
	nop
	nop
	nop
	nop
	mov R4, PC
	add R4, R4, #5
	cmp R2, R1
	bne testBP
	mov R0,R2
	bx lr


testBP:
	nop
	nop
	nop
	nop
	nop
	nop
	mov R3,R2
	nop
	nop
	nop
	nop
	nop
	nop
	bx R4


testBranch:
	nop
	nop
	nop
	mov R3, R0
	nop
	nop
	nop
	mov R2,R0
	nop
	nop
	nop
	nop
	nop
	nop
	bx R5

asm_mov:
	mov R3, R2
	nop
	nop
	nop
	nop
	nop
	nop
	movs R2, R0
	nop
	nop
	nop
	nop
	nop
	nop
	mov R4, PC
	add R4, R4, #5
	cmp R2, R1
	bne testBP
	mov R0,R2
	bx lr

asm_ldr:
	mov R3, R0
	nop
	nop
	nop
	nop
	nop
	nop
	ldr R2,[R0,#4]
	nop
	nop
	nop
	nop
	nop
	nop
	mov R4, PC
	add R4, R4, #5
	cmp R2, R1
	bne testBP
	mov R0,R2
	bx lr

asm_str:
	mov R3, R0
	nop
	nop
	nop
	nop
	nop
	nop
	str R1,[R0]
	nop
	nop
	nop
	nop
	nop
	nop
	ldr R2,[R3]
	mov R4 , PC
	add R4 , R4 , #5
	cmp R2 , R1
	bne testBP
	mov R0,R2
	bx lr


asm_reg:
	str R1,[R0]
	bx lr
