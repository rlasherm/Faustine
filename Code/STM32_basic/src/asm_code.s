.cpu cortex-m3
.thumb

.section .text
.globl asm_ldr
.globl asm_reg

#.type asm_code,@function


asm_ldr:
	mov R3, R0
	nop
	nop
	nop
	nop
	nop
	nop
	nop
	nop
	ldr R2,[R0,#4]
	nop
	nop
	nop
	nop
	nop
	nop
	nop
	nop
	mov R4, PC
	add R4, R4, #5
	cmp R2, R1
	bne testBP
	mov R0,R2
	bx lr

testBP:
	nop
	nop
	nop
	nop
	nop
	nop
	nop
	nop
	mov R3,R2
	nop
	nop
	nop
	nop
	nop
	nop
	nop
	nop
	bx R4

asm_reg:
	str R1,[R0]
	bx lr
