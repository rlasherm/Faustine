/**
* @Author: Ludovic
* @Date:   09-08-2016
* @Email:  ludovic.claudepierre@inria.fr, ronan.lashermes@inria.fr
* @Last modified by:   Ludovic
* @Last modified time: 13-03-2018
* @License: GPL
*/

/* Includes ------------------------------------------------------------------*/
//#include "trigger.h"
#include <string.h>
#include "main.h"
/** @addtogroup STM32F1xx_HAL_Examples
  * @{
  */

/** @addtogroup Templates
  * @{
  */

/* Private typedef -----------------------------------------------------------*/
/* Private define ------------------------------------------------------------*/
/* Private macro -------------------------------------------------------------*/
/* Private variables ---------------------------------------------------------*/

static GPIO_InitTypeDef  GPIO_InitStruct;
UART_HandleTypeDef UartHandle;
const char *welcome = "Shall we play a game?\n";
uint8_t command[1];//single byte commands
char text[256];
//unsigned char plaintext[DATA_SIZE];
//unsigned char ciphertext[DATA_SIZE];
//unsigned char key[DATA_SIZE]={121,27,229,29,20,83,109,221,136,255,194,140,136,168,13,101};
int tableau[20];
int id;
int result;

int bla01 = 1;
int bla02 = 2;
int bla03 = 3;
int bla04 = 4;
/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);
static void Error_Handler(void);
void OutputMCO();
int asm_code(int*, int);
int asm_add(int, int);
int asm_branch(int, int);
int asm_eor(int, int, int);
int asm_mov(int, int);
int asm_lsl(int, int);
int asm_ldr(int*, int);
int asm_str(int*, int);
//int testBP;
//void delayUs(int);
void asm_reg(int, int);
/* Private functions ---------------------------------------------------------*/
//#ifdef __GNUC__
/* With GCC/RAISONANCE, small printf (option LD Linker->Libraries->Small printf
   set to 'Yes') calls __io_putchar() */
//#define PUTCHAR_PROTOTYPE int __io_putchar(int ch)
//#else
#define PUTCHAR_PROTOTYPE int fputc(int ch, FILE *f)
//#endif
/**
  * @brief  Main program
  * @param  None
  * @retval None
  */
int main(void)
{
  int verbose = 0;
  //int* add01=&bla01;
  //int* add02=&bla02;
  //int* add03=&bla03;
  /* STM32F1xx HAL library initialization:
       - Configure the Flash prefetch
       - Systick timer is configured by default as source of time base, but user
         can eventually implement his proper time base source (a general purpose
         timer for example or other time source), keeping in mind that Time base
         duration should be kept 1ms since PPP_TIMEOUT_VALUEs are defined and
         handled in milliseconds basis.
       - Set NVIC Group Priority to 4
       - Low Level Initialization
     */
  HAL_Init();

  /* Configure the system clock to 24 MHz */
  SystemClock_Config();

  /* Ouput MCO signal on PIN PA8*/
  OutputMCO();

  /* -1- Enable each GPIO Clock (to be able to program the configuration registers) */
  LED3_GPIO_CLK_ENABLE();
  LED4_GPIO_CLK_ENABLE();
  __HAL_RCC_GPIOC_CLK_ENABLE();

  /* -2- Configure IOs in output push-pull mode to drive external LEDs */
  GPIO_InitStruct.Mode  = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull  = GPIO_PULLUP;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_HIGH;

  GPIO_InitStruct.Pin = LED3_PIN;
  HAL_GPIO_Init(LED3_GPIO_PORT, &GPIO_InitStruct);

  GPIO_InitStruct.Pin = LED4_PIN;
  HAL_GPIO_Init(LED4_GPIO_PORT, &GPIO_InitStruct);

  GPIO_InitStruct.Pull  = GPIO_NOPULL;
  GPIO_InitStruct.Pin = GPIO_PIN_6;
  HAL_GPIO_Init(GPIOC, &GPIO_InitStruct);

  /*##-1- Configure the UART peripheral ######################################*/
  /* Put the USART peripheral in the Asynchronous mode (UART Mode) */
  /* UART configured as follows:
      - Word Length = 8 Bits (8 data bit + 0 parity bit)
      - Stop Bit    = One Stop bit
      - Parity      = ODD parity
      - BaudRate    = 115200 baud
      - Hardware flow control disabled (RTS and CTS signals) */
  UartHandle.Instance        = USARTx;

  UartHandle.Init.BaudRate   = 115200;
  UartHandle.Init.WordLength = UART_WORDLENGTH_8B;
  UartHandle.Init.StopBits   = UART_STOPBITS_1;
  UartHandle.Init.Parity     = UART_PARITY_NONE;
  UartHandle.Init.HwFlowCtl  = UART_HWCONTROL_NONE;
  UartHandle.Init.Mode       = UART_MODE_TX_RX;

  if(HAL_UART_DeInit(&UartHandle) != HAL_OK)
  {
    Error_Handler();
  }
  if (HAL_UART_Init(&UartHandle) != HAL_OK)
  {
    /* Initialization Error */
    Error_Handler();
  }

  //Send welcoming message
  HAL_UART_Transmit(&UartHandle, (uint8_t *)welcome, strlen(welcome), 0xFFFF);
	// initialisation d'un tableau de 20 cases
	for (id=0;id<20;id++)
	{
		tableau[id]=id;
	}

  /* Infinite loop */
  while (1)
  {
    //read command
    if(HAL_UART_Receive(&UartHandle, &command[0], 1, 0xFFFF) == HAL_OK)
    {
      HAL_GPIO_TogglePin(LED3_GPIO_PORT, LED3_PIN);
      switch (command[0]) {
        case 't'://test
          sprintf(text, "STM32 AES Test OK\n");
          HAL_UART_Transmit(&UartHandle, (uint8_t *)text, strlen(text), 0xFFFF);
          break;
        case 'g' : // trigger + timer
	  //Trigger(0);
	  asm_reg(0x40011000,0x43444444); // reset de la valeur pour le trigger
	  HAL_GPIO_WritePin(GPIOC, GPIO_PIN_6, GPIO_PIN_SET); // trigger on
	  //result=asm_code(&tableau[0], 8);
	  //result=asm_lsl(12,3072); // Test de l'instruction LSL ---- valeur reference : 3072 ou 12
	  //result=asm_eor(0x305d1f25, 0x10453698, 0x201829bd ); // Test de l'instruction EOR ---- valeur reference : 0x10453698 = 272971416
	  //result=asm_branch(5,5); // Test de l'instruction Beq ---- valeur reference : 5
	  //result=asm_mov(5, 5); // Test de l'instruction MOV ---- valeur reference : 5
	  //result=asm_add(1097, 1103); // Test de l'instruction ADD ---- valeur reference : 8
	  result=asm_ldr(&tableau[1], 2); // Test de l'instruction LDR ---- valeur reference : 2
	  //result=asm_str(&tableau[1], 0x43444444); // Test de l'instruction STR ---- valeur reference : 5 ou 1128547396
	  //tableau[1]=1; // reinitialiser la case du tableau modifiée lors du STR
	  //result = 2;
	  sprintf(text, "%d\n\r",result);
          HAL_UART_Transmit(&UartHandle, (uint8_t *)text, strlen(text), 0xFFFF); //envoie les données
	  HAL_GPIO_WritePin(GPIOC, GPIO_PIN_6, GPIO_PIN_RESET); // trigger OFF
        	break;
        case 's':
          verbose = 0;
          break;
        case 'v':
          verbose = 1;
          break;
        default:
          sprintf(text, "Unknown command: %c\n", command[0]);
          HAL_UART_Transmit(&UartHandle, (uint8_t *)text, strlen(text), 0xFFFF);
          break;
      }

    }
    else
    {
      //sprintf(text, "Didn't heard\n");
      //HAL_UART_Transmit(&UartHandle, (uint8_t *)text, strlen(text), 0xFFFF);
    }
  }
}

/**
  * @brief  Retargets the C library printf function to the USART.
  * @param  None
  * @retval None
  */
PUTCHAR_PROTOTYPE
{
  /* Place your implementation of fputc here */
  /* e.g. write a character to the USART1 and Loop until the end of transmission */
  HAL_UART_Transmit(&UartHandle, (uint8_t *)&ch, 1, 0xFFFF);

  return ch;
}

/**
  * @brief  System Clock Configuration
  *         The system Clock is configured as follow :
  *            System Clock source            = PLL (HSE)
  *            SYSCLK(Hz)                     = 24000000
  *            HCLK(Hz)                       = 24000000
  *            AHB Prescaler                  = 1
  *            APB1 Prescaler                 = 1
  *            APB2 Prescaler                 = 1
  *            HSE Frequency(Hz)              = 8000000
  *            HSE PREDIV1                    = 2
  *            PLLMUL                         = 6
  *            Flash Latency(WS)              = 0
  * @param  None
  * @retval None
  */
void SystemClock_Config(void)
{
  RCC_ClkInitTypeDef clkinitstruct = {0};
  RCC_OscInitTypeDef oscinitstruct = {0};

  /* Enable HSE Oscillator and activate PLL with HSE as source */
  //oscinitstruct.OscillatorType  = RCC_OSCILLATORTYPE_HSI;
  //oscinitstruct.HSIState        = RCC_HSI_ON;
  //oscinitstruct.PLL.PLLSource   = RCC_PLLSOURCE_HSI;
  oscinitstruct.OscillatorType  = RCC_OSCILLATORTYPE_HSE;
  oscinitstruct.HSEState        = RCC_HSE_ON;
  oscinitstruct.HSEPredivValue  = RCC_HSE_PREDIV_DIV2; // DIV1 : 48 MHz, DIV2 : 24 MHz, DIV3 : 16 MHz, DIV4 : 12 MHz
  oscinitstruct.PLL.PLLState    = RCC_PLL_ON;
  oscinitstruct.PLL.PLLSource   = RCC_PLLSOURCE_HSE;
  oscinitstruct.PLL.PLLMUL      = RCC_PLL_MUL6;
  if (HAL_RCC_OscConfig(&oscinitstruct)!= HAL_OK)
  {
    /* Initialization Error */
    while(1);
  }

  /* Select PLL as system clock source and configure the HCLK, PCLK1 and PCLK2
     clocks dividers */
  clkinitstruct.ClockType = (RCC_CLOCKTYPE_SYSCLK | RCC_CLOCKTYPE_HCLK | RCC_CLOCKTYPE_PCLK1 | RCC_CLOCKTYPE_PCLK2);
  clkinitstruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
  clkinitstruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  clkinitstruct.APB2CLKDivider = RCC_HCLK_DIV1;
  clkinitstruct.APB1CLKDivider = RCC_HCLK_DIV1;
  if (HAL_RCC_ClockConfig(&clkinitstruct, FLASH_LATENCY_0)!= HAL_OK)
  {
    /* Initialization Error */
    while(1);
  }
}

/**
* Cortex M0 - STM32F0 Discovery board code spew out one of the clocks on PA.8
*/

void OutputMCO() {
GPIO_InitTypeDef GPIO_InitStruct;

//RCC_AHBPeriphClockCmd(RCC_AHBPeriph_GPIOA, ENABLE);
//RCC_APB2PeriphClockCmd(HAL_RCC_GPIOA, ENABLE);
__HAL_RCC_GPIOA_CLK_ENABLE();
// Output clock on MCO pin ---------------------------------------------//

GPIO_InitStruct.Pin = GPIO_PIN_8;
GPIO_InitStruct.Mode = GPIO_MODE_AF_PP;
GPIO_InitStruct.Speed = GPIO_SPEED_HIGH;
//GPIO_InitStruct.OType = GPIO_OType_PP;
GPIO_InitStruct.Pull = GPIO_NOPULL;
HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);

// pick one of the clocks to spew
//RCC_MCOConfig(RCC_MCOSource_SYSCLK); // Put on MCO pin the: System clock selected
//HAL_RCC_MCOConfig(RCC_MCO1,RCC_MCOSOURCE_HSE, RCC_MCODIV_1); // Put on MCO pin the: freq. of external crystal
HAL_RCC_MCOConfig(RCC_MCO1,RCC_MCO1SOURCE_SYSCLK, RCC_MCODIV_1);
}


/**
  * @brief  This function is executed in case of error occurrence.
  * @param  None
  * @retval None
  */
static void Error_Handler(void)
{
  while (1)
  {
  }
}

#ifdef  USE_FULL_ASSERT

/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t* file, uint32_t line)
{
  /* User can add his own implementation to report the file name and line number,
     ex: printf("Wrong parameters value: file %s on line %d\n", file, line) */

  /* Infinite loop */
  while (1)
  {
  }
}
#endif

/**
  * @}
  */

/**
  * @}
  */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
