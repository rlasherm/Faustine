/**
* @Author: Ludovic
* @Date:   09-08-2016
* @Email:  ludovic.claudepierre@inria.fr, ronan.lashermes@inria.fr
* @Last modified by:   Ludovic
* @Last modified time: 13-03-2018
* @License: GPL
*/
#include "stm32f1xx_hal.h"
/* Includes ------------------------------------------------------------------*/
void delayUS(uint16_t);
void Trigger(uint32_t);
uint32_t getUs(void);
void wait_cycles(uint32_t);
