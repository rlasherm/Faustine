Utiliser le script shell launch_ocd.sh remplace ce tuto.

Mise en place d'openOCD STM32VLDiscovery

On utilisera openocd pour communiquer avec la carte (autre possibilité: stlink)

** Lancer openocd **
""The STLINKv1's SCSI emulation is very broken, so the best thing to do
is tell your operating system to completely ignore it.""

sudo modprobe -r usb-storage && sudo modprobe usb-storage quirks=483:3744:i

Si erreur car le module usb_storage est utilisé, il faut chercher le prog qui l'utilise avec:
lsmod | grep usb_storage

Puis tuer le prog indiquer dans la colonne 4 (moi, il s'appelle uas):
sudo modprobe -r uas

On lance la connection (la carte doit être connectée).
sudo openocd -f /usr/share/openocd/scripts/board/stm32vldiscovery.cfg

** Communiquer via openocd **
ouvrir un autre terminal et se connecter via telnet au port 4444
telnet localhost 4444

** via GDB **
arm-none-eabi-gdb

Dans gdb se connecter à openocd via:
tar ext :3333




