function timings(timings)
    mapslices(measure_timing, T, [2])
end

threshold = 0

function greater_th(t)
    t >= threshold
end

function measure_timing(trace)
    up = find(greater_th, trace)
    maximum(up) - minimum(up)
end
