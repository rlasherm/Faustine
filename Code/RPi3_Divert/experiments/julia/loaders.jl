# TODO Ronan tu nous commentes ces fonctions ?

function to_text(hex)
	round(Int64,hex2bytes(hex)')
end

function to_hex(arr)
	reduce(string,map(x->hex(x,2),arr))
end


function load_binary_matrix_i8(filepath::ASCIIString)
  f = open(filepath, "r")

  if(eof(f) == true)
    close(f)
    return 0
  end

  trace_len::Int32 = read(f, Int32)
  trace_count::Int32 = read(f, Int32)

  println("Trace len $(trace_len), trace count $(trace_count )")

  M=zeros(Int8, trace_count, trace_len)

  i::Int32 = 1

  while eof(f) == false && i <= trace_count
    M[i, :] = read(f, Int8, trace_len)
    i+=1
  end

  close(f)
  return M

end

function load_binary_matrix_i8(filepath::ASCIIString, nmax)
  f = open(filepath, "r")

  if(eof(f) == true)
    close(f)
    return 0
  end

  trace_len::Int32 = read(f, Int32)
  trace_count::Int32 = min(read(f, Int32),nmax)

  println("Trace len $(trace_len), trace count $(trace_count )")

  M=zeros(Int8, trace_count, trace_len)

  i::Int32 = 1

  while eof(f) == false && i <= trace_count
    M[i, :] = read(f, Int8, trace_len)
    i+=1
  end

  close(f)
  return M

end

function load_binary_matrix_i16(filepath::ASCIIString)
  f = open(filepath, "r")

  if(eof(f) == true)
    close(f)
    return 0
  end

  trace_len::Int32 = read(f, Int32)
  trace_count::Int32 = read(f, Int32)

  println("Trace len $(trace_len), trace count $(trace_count )")

  M=zeros(Int16, trace_count, trace_len)

  i::Int32 = 1

  while eof(f) == false && i <= trace_count
    M[i, :] = read(f, Int16, trace_len)
    i+=1
  end

  close(f)
  return M

end

function load_binary_matrix_i16(filepath::ASCIIString, nmax)
  f = open(filepath, "r")

  if(eof(f) == true)
    close(f)
    return 0
  end

  trace_len::Int32 = read(f, Int32)
  trace_count::Int32 = min(read(f, Int32),nmax)

  println("Trace len $(trace_len), trace count $(trace_count )")

  M=zeros(Int16, trace_count, trace_len)

  i::Int32 = 1

  while eof(f) == false && i <= trace_count
    M[i, :] = read(f, Int16, trace_len)
    i+=1
  end

  close(f)
  return M

end

function load_texts(filepath::ASCIIString)
  f_texts = open(filepath, "r")
	texts=map(x->hex2bytes(strip(x)),readlines(f_texts))
	close(f_texts)

  n=size(texts,1)
	p=size(texts[1],1)
	mt=zeros(n,p)
	for i in 1:n
			mt[i,:]=texts[i]
	end
	round(UInt8,mt)

end

function load_texts(filepath::ASCIIString, nmax)
  f_texts = open(filepath, "r")
	texts=map(x->hex2bytes(strip(x)),readlines(f_texts))
	close(f_texts)

  n=min(size(texts,1), nmax)
	p=size(texts[1],1)
	mt=zeros(n,p)
	for i in 1:n
			mt[i,:]=texts[i]
	end
	round(UInt8,mt)

end

#------------------------------------------------------------------------------------------------------------------------
# Fonction qui lit un fichier binaire binf64 et le met dans une matrice M
#------------------------------------------------------------------------------------------------------------------------
function load_binary_matrix_f64(filepath)
	# ouverture du fichier
	f = open(filepath, "r")

	if(eof(f) == true)
		close(f)
	   return 0
	end

	#lit le nombre de lignes
	trace_len::Int32 = read(f, Int32)
	#lit le nombre de colonne
	trace_count::Int32 = read(f, Int32)


  	M=zeros(Float64, trace_count, trace_len)
	i::Int32 = 1

  	while eof(f) == false && i <= trace_count
		M[i, :] = read(f, Float64, trace_len)
   	 	i+=1
  	end

	close(f)
	return M

end

#------------------------------------------------------------------------------------------------------------------------
# Fonction qui lit un fichier binaire binf32 et le met dans une matrice M
#------------------------------------------------------------------------------------------------------------------------
function load_binary_matrix_f32(filepath)
	# ouverture du fichier
	f = open(filepath, "r")

	if(eof(f) == true)
		close(f)
	   return 0
	end

	#lit le nombre de lignes
	trace_len::Int32 = read(f, Int32)
	#lit le nombre de colonne
	trace_count::Int32 = read(f, Int32)


  	M=zeros(Float64, trace_count, trace_len)
	i::Int32 = 1

  	while eof(f) == false && i <= trace_count
		M[i, :] = read(f, Float32, trace_len)
   	 	i+=1
  	end

	close(f)
	return M

end


#------------------------------------------------------------------------------------------------------------------------
# Fonction qui écrit la matrice M dans un fichier binaire bin*
#------------------------------------------------------------------------------------------------------------------------
function save_binary_matrix(filepath, M)
	# ouverture du fichier
	f = open(filepath, "w")

	#ecrit le nombre de lignes
	trace_len::Int32 = size(M,2)
	write(f,trace_len)
	#ecrit le nombre de colonne
	trace_count::Int32 = size(M,1)
	write(f,trace_count)



  	for i = 1:trace_count
  		for j = 1:trace_len
  			write(f, M[i,j])
  		end
  		print("\r$i")
  	end
  	println("\r")

	close(f)

end
