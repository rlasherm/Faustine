/**
* @Author: ronan
* @Date:   13-01-2017
* @Email:  ronan.lashermes@inria.fr
* @Last modified by:   ronan
* @Last modified time: 13-01-2017
*/

#include "arm.h"
#include "init.h"

void set_vbar_elx(unsigned int el, void* address)
{
  switch(el) {
    case 3:
      setVBAR_EL3(address);
      break;
    case 2:
      setVBAR_EL2(address);
      break;
    case 1:
      setVBAR_EL1(address);
      break;
  }
}
