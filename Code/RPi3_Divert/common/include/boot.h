/**
* @Author: Lashermes Ronan <ronan>
* @Date:   26-01-2017
* @Email:  ronan.lashermes@inria.fr
* @Last modified by:   ronan
* @Last modified time: 26-01-2017
* @License: MIT
*/

#ifndef BOOT_H
#define BOOT_H

typedef struct boot_metadata_struct {
  void* pub_key_add;
  size_t pub_key_len;
  void* tos_entry;
  size_t tos_len;
  void* tos_sig_add;
  size_t tos_sig_len;
  void* nos_entry;
  size_t nos_len;
  void* nos_sig_add;
  size_t nos_sig_len;
  void* boot_reentry;
} boot_metadata;

#endif
