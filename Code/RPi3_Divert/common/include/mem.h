/**
* Define all primitives relative to basic memory management here.
*
* @Author: ronan, kevin
* @Date:   31-08-2016
* @Email:  ronan.lashermes@inria.fr, sebanjila.bukasa@inria.fr
* @Last modified by:   ronan
* @Last modified time: 02-12-2016
* @License: GPL
*/
#ifndef MEM_H
#define MEM_H

#include <stddef.h>

#define MEM_R 1 //read flag
#define MEM_W 2 //write flag
#define MEM_X 3 //execute flag

typedef struct MEMFILE_struct {
  void* file_start;
  void* cursor;
  size_t file_len;
  int flags;
} MEMFILE;

typedef struct MEMFILE_descriptor_struct {
  void* file_start;
  size_t file_len;
} MEMFILE_descriptor;

// int strcmp(const char *s1, const char *s2, size_t n);
// void *memcpy(void *dst, const void *src, size_t len);
// int memcmp(const void *str1, const void *str2, size_t n);
// int strcount(const char* str);
void *mo_memset(void *str, int c, size_t n);//mmu off memset

MEMFILE* memopen(MEMFILE_descriptor desc, int flags);
int memclose(MEMFILE* stream);
size_t memread(void* ptr, size_t size, size_t nmemb, MEMFILE* stream);
size_t memwrite(const void* ptr, size_t size, size_t nmemb, MEMFILE* stream);

#endif
