/**
* @Author: Lashermes Ronan <ronan>
* @Date:   31-01-2017
* @Email:  ronan.lashermes@inria.fr
* @Last modified by:   ronan
* @Last modified time: 31-01-2017
* @License: MIT
*/

#if !defined(MBEDTLS_CONFIG_FILE)
#include "mbedtls/config.h"
#else
#include MBEDTLS_CONFIG_FILE
#endif

#if defined(MBEDTLS_PLATFORM_C)
#include "mbedtls/platform.h"
#else
#include <stdio.h>
#define mbedtls_fprintf    fprintf
#define mbedtls_printf     printf
#endif

#include "mbedtls/md_internal.h"
#include "mbedtls/md.h"
#include "mbedtls/pk.h"
#include "mem.h"
#include "uart.h"
#include "secure_boot.h"

#include <string.h>

//Modified from mbedtls sources
//here f is opened/closed outside
int mbedtls_md_memfile( MEMFILE_descriptor path, unsigned char *output )
{
  const mbedtls_md_info_t *md_info = mbedtls_md_info_from_type( MBEDTLS_MD_SHA256 );
  int ret;
  size_t n;
  mbedtls_md_context_t ctx;
  unsigned char buf[1024];
  MEMFILE* f = memopen(path, MEM_R);

  if( md_info == NULL )
      return( MBEDTLS_ERR_MD_BAD_INPUT_DATA );

  mbedtls_md_init( &ctx );


  if( ( ret = mbedtls_md_setup( &ctx, md_info, 0 ) ) != 0 )
      goto cleanup;

  md_info->starts_func( ctx.md_ctx );

  while( ( n = memread( buf, 1, sizeof( buf ), f ) ) > 0 )
  {
    md_info->update_func( ctx.md_ctx, buf, n );
  }

  md_info->finish_func( ctx.md_ctx, output );

cleanup:
  mbedtls_md_free( &ctx );
  memclose(f);

  return( ret );
}


int verify_signature(MEMFILE_descriptor file_to_verify, MEMFILE_descriptor file_signature, MEMFILE_descriptor pubkey)
{
  MEMFILE* f;
  int ret = 1;
  size_t i;
  mbedtls_pk_context pk;
  unsigned char hash[32];
  unsigned char buf[MBEDTLS_MPI_MAX_SIZE];

  mbedtls_pk_init( &pk );

  uart_print( " - Reading public key...");


  //1# parse public key
  if( ( ret = mbedtls_pk_parse_public_memkeyfile( &pk, pubkey ) ) != 0 )
  {
      uart_print(" failed\n\rCannot read public key.\n\r");
      return ret;
  }

  if( !mbedtls_pk_can_do( &pk, MBEDTLS_PK_RSA ) )
  {
      ret = 1;
      uart_print( " failed\n\rKey is not an RSA key.\n\r" );
      return ret;
  }

  mbedtls_rsa_set_padding( mbedtls_pk_rsa( pk ), MBEDTLS_RSA_PKCS_V21, MBEDTLS_MD_SHA256 );


  //2# extract signature
  uart_print("OK.\n\r - Extract image signature...");
  f = memopen(file_signature, MEM_R);
  if(f == NULL)
  {
    uart_print(" failed\n\rCannot read signature.\n\r");
  }
  i = memread(buf, 1, MBEDTLS_MPI_MAX_SIZE, f);
  memclose(f);

  //3# compute file hash
  uart_print("OK.\n\r - Compute file SHA-256...");
  if((ret = mbedtls_md_memfile( file_to_verify, hash )) != 0)
  {
      uart_print(" failed\n\rCannot compute hash.\n\r");
      return ret;
  }

  int c;
  for(c=0;c<32;c++)
  {
    uart_printhex8(hash[c]);
  }
  uart_print(" ");

  //4# verify signature
  uart_print("OK.\n\r - Verifying signature...");
  if( ( ret = mbedtls_pk_verify( &pk, MBEDTLS_MD_SHA256, hash, 0,
                         buf, i ) ) != 0 )
  {
      uart_print( " failed\n\rWrong signature!\n\r" );
      return ret;
  }

  uart_print("OK.\n\r");


  mbedtls_pk_free( &pk );


  return 0;
}

static void mbedtls_zeroize( void *v, size_t n ) {
    volatile unsigned char *p = v; while( n-- ) *p++ = 0;
}



 // * Load all data from a file into a given buffer.
 // *
 // * The file is expected to contain either PEM or DER encoded data.
 // * A terminating null byte is always appended. It is included in the announced
 // * length only if the data looks like it is PEM encoded.
 //
int mbedtls_pk_load_memfile( MEMFILE_descriptor path, unsigned char **buf, size_t *n )
{
    MEMFILE *f;

    if( ( f = memopen( path, MEM_R ) ) == NULL )
    {
      return( MBEDTLS_ERR_PK_FILE_IO_ERROR );
    }


    *n = (size_t)f->file_len;

    if( *n + 1 == 0 ||
        ( *buf = mbedtls_calloc( 1, *n + 1 ) ) == NULL )
    {
        memclose( f );
        return( MBEDTLS_ERR_PK_ALLOC_FAILED );
    }

    if( memread( *buf, 1, *n, f ) != *n )
    {
        memclose( f );
        mbedtls_free( *buf );
        return( MBEDTLS_ERR_PK_FILE_IO_ERROR );
    }

    memclose( f );

    (*buf)[*n] = '\0';

    if( strstr( (const char *) *buf, "-----BEGIN " ) != NULL )
        ++*n;

    return( 0 );
}

 //
 // * Load and parse a public key. Modified from mbedtls to deal with memfile
 //
int mbedtls_pk_parse_public_memkeyfile( mbedtls_pk_context *ctx, MEMFILE_descriptor path )
{
    int ret;
    size_t n;
    unsigned char *buf;

    if( ( ret = mbedtls_pk_load_memfile( path, &buf, &n ) ) != 0 )
        return( ret );

    ret = mbedtls_pk_parse_public_key( ctx, buf, n );

    mbedtls_zeroize( buf, n );
    mbedtls_free( buf );

    return( ret );
}
