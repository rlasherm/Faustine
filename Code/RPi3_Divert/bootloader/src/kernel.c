/**
* @Author: ronan, kevin
* @Date:   31-08-2016
* @Email:  ronan.lashermes@inria.fr, sebanjila.bukasa@inria.fr
* @Last modified by:   ronan
* @Last modified time: 09-01-2017
* @License: GPL
*/


#include "types.h"
#include "kernel.h"
#include "arm.h"
#include "gpio.h"
#include "uart.h"
#include "timer.h"
#include "init.h"
#include "jtag.h"
#include "mem.h"
#include "interrupt_handler.h"
#include "boot.h"
#include "secure_boot.h"
#include "mmu.h"
#include "myaes.h"
#include <stdlib.h>
#include <stdio.h>
// #include "mbedtls/error.h"

#define DATA_SIZE 16

void gpio_init() {
  //set trig pin as output
  set_pin_function(TRIG_PIN, FSEL_OUTPUT);
}

void initialise_monitor_handles(void)
{

}

void boot_init ( void )
{

  //Erase BSS
  unsigned int* bss  = &__bss_start__;
  unsigned int*  bss_end = &__bss_end__ ;

  //Initialize bss section to 0
  while(bss < bss_end) {
    *bss++ = 0;
  }

  // call construtors of static objects
	extern void (*__init_start) (void);
	extern void (*__init_end) (void);
	for (void (**pFunc) (void) = &__init_start; pFunc < &__init_end; pFunc++)
	{
		(**pFunc) ();
	}


  gpio_init();
  //fast_trig_up();
  //fast_trig_down();

  // initiate_jtag();
  timer_init();
  uart_init();


  // uart_print("BOOTLOADER: Init...\n\r");

  mmu_region page_mem = { .start_add = &_pages_start, .len = (&_pages_end - &_pages_start)};
  mmu_context mmu_ctx = mmu_init_tables(page_mem);
  mmu_enable(mmu_ctx);
  // enableMMU_EL3(mmu_ctx.ttbr, TCR_EL3);

  enable_icache_EL3();

  //write vbar_el3
  set_vbar_elx(3, &vbar_elx_vec);
  set_vbar_elx(2, &vbar_elx_vec);
  set_vbar_elx(1, &vbar_elx_vec);

  //start caches
  // start_l1cache();
  // start_vfp();

  // start other cores (initialize stack vectors)
  // start_other_core (MBXSETC13, core_wrapper);
  // start_other_core (MBXSETC23, core_wrapper);
  // start_other_core (MBXSETC33, core_wrapper);

  //call main after preinit
  boot_main();
}

/**************************************************/
/*                  Main                          */
/**************************************************/


int boot_main ( void )
{
  unsigned char plaintext[DATA_SIZE];
  unsigned char ciphertext[DATA_SIZE];
  unsigned char key[DATA_SIZE]={121,27,229,29,20,83,109,221,136,255,194,140,136,168,13,101};

  // uart_print("BOOTLOADER: Shall we play a game?\n\r");
  uart_print("Shall we play a game?\n");


  // uart_print("BOOTLOADER: EL");
  unsigned long el = getEL();
  // uart_printhex64(el);
  // uart_print("\n\r");
  // printf("BOOTLOADER: EL%d\n\r", el);

  //heap
  // uart_print("BOOTLOADER: heap from 0x");
  // uart_printhex64((unsigned long)&_heap_start);
  // uart_print(" to 0x");
  // uart_printhex64((unsigned long)&_heap_end);
  // uart_print("\n\r");

  //stack
  // uart_print("BOOTLOADER: stack from 0x");
  // uart_printhex64((unsigned long)&_stack_bottom);
  // uart_print(" to 0x");
  // uart_printhex64((unsigned long)&_stack_top);
  // uart_print("\n\r");
  //
  // if(el == 3)
  // {
  //   uart_print("BOOTLOADER: (EL3) SCR_EL3=0x");
  //   unsigned long scr_el3 = getSCR_EL3();
  //   uart_printhex64(scr_el3);
  //   uart_print("\n\r");
  // }

  while(1)
  {
    // invalidate_icache();
    char cmd = uart_recv();

    switch(cmd) {
      case 't':
        uart_print("RPi3 DIV Test OK\n");
        break;
      case 'k'://set key (binary)
        uart_read_vector(key, DATA_SIZE);
        break;

      case 'p'://set Plaintext (binary)
        uart_read_vector(plaintext, DATA_SIZE);
        break;

      case 'g'://go!
        AESEncrypt(ciphertext, plaintext, key);
        break;

      case 'c'://get ciphertext
        uart_send_vector(ciphertext, DATA_SIZE);
        break;

      case 'f'://fast mode
        uart_read_vector(plaintext, DATA_SIZE);//receive Plaintext
        AESEncrypt(ciphertext, plaintext, key);//AES
        uart_send_vector(ciphertext, DATA_SIZE);//transmit ciphertext
        break;
      case 'r':
        trigger_sel = uart_recv();
      default:
        //uart_print("Unknown command\n");
        //printf("Unknown command: %02x\n", cmd);
        break;
    }

  }



  return 0;
}
