/**
* @Author: Lashermes Ronan <ronan>
* @Date:   31-01-2017
* @Email:  ronan.lashermes@inria.fr
* @Last modified by:   ronan
* @Last modified time: 31-01-2017
* @License: MIT
*/



#ifndef SECURE_BOOT_H
#define SECURE_BOOT_H

#include "mbedtls/md.h"
#include "mbedtls/pk.h"

//for hashing
int mbedtls_md_memfile( MEMFILE_descriptor path, unsigned char *output );

//for verifying signature
int verify_signature(MEMFILE_descriptor file_to_verify, MEMFILE_descriptor file_signature, MEMFILE_descriptor pubkey);
int mbedtls_pk_load_memfile( MEMFILE_descriptor path, unsigned char **buf, size_t *n );
int mbedtls_pk_parse_public_memkeyfile( mbedtls_pk_context *ctx, MEMFILE_descriptor path );

#endif
