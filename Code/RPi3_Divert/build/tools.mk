#generate the tools required to sign and pack the application

.PHONY: tools
tools: $(KEY_CREATOR) $(SIGNER) $(PACKER) $(VERIFIER)

.PHONY: $(KEY_CREATOR)
$(KEY_CREATOR):
	$(MAKE) -C ../tools/key_creator

.PHONY: $(SIGNER)
$(SIGNER):
	$(MAKE) -C ../tools/signer

.PHONY: $(PACKER)
$(PACKER):
	$(MAKE) -C ../tools/packer

.PHONY: $(VERIFIER)
$(VERIFIER):
	$(MAKE) -C ../tools/verifier

tools-prepare:
	$(MAKE) -C ../tools/key_creator prepare
	$(MAKE) -C ../tools/signer prepare
	$(MAKE) -C ../tools/packer prepare
	$(MAKE) -C ../tools/verifier prepare

tools-clean:
	$(MAKE) -C ../tools/key_creator clean
	$(MAKE) -C ../tools/signer clean
	$(MAKE) -C ../tools/packer clean
	$(MAKE) -C ../tools/verifier clean
