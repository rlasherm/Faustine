PRIV_KEY_FILE = $(BINDIR)/private.key
PUB_KEY_FILE = $(BINDIR)/public.key

#generate signing key pair
$(PRIV_KEY_FILE) $(PUB_KEY_FILE):
	@echo
	@echo ----------------- KEY GENERATION -------------------------
	@echo
	$(KEY_CREATOR) $(BINDIR)
	@echo
	@echo ----------------------------------------------------------
	@echo

#sign nos.bin file
$(NOS_SIG): $(PRIV_KEY_FILE) $(NOS_BIN)
	@echo
	@echo ----------------- SIGNING -------------------------
	@echo
	$(SIGNER) $(PRIV_KEY_FILE) $(NOS_BIN)
	@echo
	@echo ---------------------------------------------------
	@echo

#sign tos.bin file
$(TOS_SIG): $(PRIV_KEY_FILE) $(TOS_BIN)
	@echo
	@echo ----------------- SIGNING -------------------------
	@echo
	$(SIGNER) $(PRIV_KEY_FILE) $(TOS_BIN)
	@echo
	@echo ---------------------------------------------------
	@echo
