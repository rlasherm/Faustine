#pack all the relevant data into one single image
.PHONY: $(IMAGE)
$(IMAGE):	 $(BOOTLOADER_BIN) $(NOS_BIN) $(TOS_BIN) $(NOS_SIG) $(TOS_SIG) $(PUB_KEY_FILE)
	@echo
	@echo ----------------- PACKING -------------------------
	@echo
	$(PACKER) $(BOOTLOADER_BIN) $(TOS_BIN) $(NOS_BIN) $(PUB_KEY_FILE) $(BINDIR)
	@echo
	@echo ---------------------------------------------------
