# MMU configuration

granule = 4K = 2^(12) (because peripherals are 4K spaced: we can have one page per peripheral)
T0SZ=32 : IA = [31:0] (32 bits = 4GB)

We start at level 1 (level 1 manages bits 31 and 30, its size is therfore reduced).
Tables must be aligned to their size.

# TCR_EL3
TCR_EL3.TG0 =
TCR_EL3.SH0 =
TCR_EL3.IRGN0 =
TCR_EL3.ORGN0 =

cf p. 1764

# Address translation instructions

AT*
