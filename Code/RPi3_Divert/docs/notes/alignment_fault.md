# Why -mstrict-align?

From [StackOverflow](http://stackoverflow.com/questions/23266411/linaro-g-aarch64-compilation-cause-unalignment-fault):

"After some study on ARMV8 architecture, I got deeper understandings about the data abort exception that I met.

Why did this align fault exception occur?

As @IgorSkochinsky has mentioned, AArch64/ARMv8 supports unaligned access. But as I'm working on a simple bare-metal environment, MMU wasn't enbaled, so in this case, memory is regarded as a device, and device doesn't support unaligned access. If MMU is enabled, this exception is gone.
How to force GCC to compile an unaligned-access free elf file?

From the manual, -mno-unaligned-access should be enough, but for my GCC version:

gcc 4.8.1 20130506 (prerelease) (crosstool-NG linaro-1.13.1-4.8-2013.05 - Linaro GCC 2013.05)
it says there's no such option. In my case, another option -mstrict-align solved this problem."
