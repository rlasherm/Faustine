<!--
@Author: Lashermes Ronan <ronan>
@Date:   20-01-2017
@Email:  ronan.lashermes@inria.fr
@Last modified by:   ronan
@Last modified time: 20-01-2017
@License: MIT
-->



# Some special registers

## SCR_EL3

32-bit Secure Configuration Register **(p. 2090)**, accessible only in EL3.
Defines the configuration of the current Security state. It specifies:
• The Security state of EL0 and EL1, either Secure or Non-secure.
• The Execution state at lower Exception levels.
• Whether IRQ, FIQ, and External Abort interrupts are taken to EL3.

Bit 0 is NS bit.

## CPTR_ELX

32-bit Architectural Feature Trap Register, variants are CPTR_EL2 **(p. 1918)**, CPTR_EL3 **(p. 1920)**.
Controls trapping to EL3 of access to CPACR_EL1, CPTR_EL2, trace functionality and registers
associated with Advanced SIMD and floating-point execution. Also controls EL3 access to trace
functionality and registers associated with Advanced SIMD and floating-point execution.

## SCTLR_ELX

32-bit System Control Register, variants are SCTLR_EL1 for EL1 and EL0 **(p. 2094)**, SCTLR_EL2 **(p. 2101)**, SCTLR_EL3 **(p. 2105)**.
Provides top level control of the system, including its memory system.

## SP_ELX

Stack pointer for this exception level. Variants are SP_EL0 **(p. 313)**, SP_EL1, SP_EL2, SP_EL3.
Holds the stack pointer associated with ELX. At higher Exception levels, SP_EL0 is used as the current
stack pointer when the value of SPSel.SP is 0.

## VBAR_ELX

Vector Base Address Register, where interrupt points to. Variants are VBAR_EL1 **(p. 2133)**, VBAR_EL2, VBAR_EL3.

## SPSR_ELX

Saved Program Status Register, the PSTATE that will be set after *eret*. Variants are SPSR_EL1 **(p. 323)**, SPSR_EL2, SPSR_EL3.
When transitionning from EL3 to EL2, the value of SPSR_EL3 will be written to PSTATE on *eret*.

## ELR_ELX

Exception Link Register, contains the address where we branch on *eret*. Variants are ELR_EL1 **(p. 300)**, ELR_EL2, ELR_EL3.
When transitionning from EL3 to EL2, the value of ELR_EL3 contains the address where we branch to on *eret*.
