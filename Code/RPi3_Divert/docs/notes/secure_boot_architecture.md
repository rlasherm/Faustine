# Secure boot architecture

Our toy secure boot consists in 3 programs:

## Stages

Real secure boots are more complex than this toy implementation. But we are interested in precise features of such systems, namely the image verification.

### Bootloader
Simulate the root of trust that would normally be in secure ROM.
The bootloader contains the public key (Pk) used to verify latter stage binaries.

### TOS

The trusted OS is the only code allowed to run in *secure* mode. It must be signed by Pk and verified in oreder to be allowed to be loaded and run.

### NOS

The normal OS is the *unsecure* OS (not running in *secure* mode). Usually android or iOS on your phone.

## How these stages are deployed?

The ARM-TF used different files for the stages of the secure boot (BL1, BL2, BL31, ...).
Here, because we do not want to implement a SD card driver + file system, all the stages are included in one binary file.
