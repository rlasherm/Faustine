library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use work.triggy_common.all;
use work.common.all;

entity apdu_follower is
  port(clk:           in std_logic;
       reset:         in std_logic;
       reset_ext:     in std_logic;

       io_data:       in byte;
       io_rdy:        in std_logic;

       apdu_conf:     in APDUConf;

       --apdu follower output
       counter_updated: out std_logic;
       counter_value:   out u32;

       debug: out byte
       );
end apdu_follower;

architecture behavior of apdu_follower is

  -- Protocol for T=0
  type APDU_FSM_STATE is (CLA, INS, P1, P2, P3, DATABATCH, DATABYTE, PB, SW2);

  signal active_counter: std_logic;
  signal reset_counter: std_logic;
  signal timing_counter: u32;

  signal active_data_counter, reset_data_counter: std_logic;
  signal data_timing_counter: u32;


  signal state: APDU_FSM_STATE;

  signal timeout_counter: u32;

  -- to extract data, save values when they are received
  signal bcla: byte;
  signal bins: byte;
  signal bp1:  byte;
  signal bp2:  byte;
  signal bp3:  byte;

  signal bsw1: byte;
  signal bsw2: byte;

  signal matching_vector: std_logic_vector(APDU_FOLLOWER_HEADER_SELECT_SIZE-1 downto 0);
  signal matching_and: std_logic_vector(APDU_FOLLOWER_HEADER_SELECT_SIZE-1 downto 0);

  signal end_trig: std_logic;
  signal header_select_valid: std_logic;
  signal reset_timeout: std_logic;
  signal counter_updated_buf: std_logic;
  signal expected_len: u16;
  signal datacounter: u16;

  begin

    --debug

    debug(0) <= io_rdy;
    debug(1) <= end_trig;
    debug(2) <= header_select_valid;
    debug(3) <= apdu_conf.header_select_en;
    debug(4) <= counter_updated_buf;
    debug(7 downto 5) <= "000";
    -- debug(7 downto 4) <= matching_vector;

    -- header select

    matching_vector(0) <= '1' when bcla = apdu_conf.header_select(0) else '0';
    matching_vector(1) <= '1' when bins = apdu_conf.header_select(1) else '0';
    matching_vector(2) <= '1' when bp1 = apdu_conf.header_select(2) else '0';
    matching_vector(3) <= '1' when bp2 = apdu_conf.header_select(3) else '0';

    matching_and(APDU_FOLLOWER_HEADER_SELECT_SIZE-1) <= matching_vector(APDU_FOLLOWER_HEADER_SELECT_SIZE-1);

    and_comp: for i in 0 to APDU_FOLLOWER_HEADER_SELECT_SIZE-2 generate
      matching_and(i) <= matching_vector(i) and matching_and(i+1);
    end generate;
    header_select_valid <= matching_and(0);

    counter_updated_buf <=  end_trig when header_select_valid = '1' and apdu_conf.header_select_en = '1' else
                        end_trig when apdu_conf.header_select_en = '0' else
                        '0';

    counter_updated <= counter_updated_buf;

    expected_len <= resize(unsigned(bp3), expected_len'length) when bp3 /= X"00" else to_unsigned(256, expected_len'length); -- do not manage P3 = 0 for ingoing comms (no direction information available)

    timeout_counter_update: process(clk, reset)
    begin
      if reset = '1' then
        timeout_counter <= (others => '0');
      elsif rising_edge(clk) then
        if reset_timeout = '1' then
          timeout_counter <= (others => '0');
        else
          timeout_counter <= timeout_counter+1;
        end if;
      end if;
    end process;

    -- counter

    counter_update: process(clk, reset)
    begin
      if reset = '1' then
        timing_counter <= (others => '0');
      elsif rising_edge(clk) then
        if reset_counter = '1' then
          timing_counter <= (others => '0');
        elsif active_counter = '1' then
          timing_counter <= timing_counter + 1;
        end if;
      end if;
    end process;

    data_timing_counter_update: process(clk, reset)
    begin
      if reset = '1' then
        data_timing_counter <= (others => '0');
      elsif rising_edge(clk) then
        if reset_data_counter = '1' then
          data_timing_counter <= (others => '0');
        elsif active_data_counter = '1' then
          data_timing_counter <= data_timing_counter + 1;
        end if;
      end if;
    end process;

    -- FSM

    FSM : process(clk, reset)
    begin
      if reset = '1' then
        state <= CLA;
        end_trig <= '0';
        bcla <= X"00";
        bins <= X"00";
        bp1 <= X"00";
        bp2 <= X"00";
        bp3 <= X"00";
        bsw1 <= X"00";
        bsw2 <= X"00";
        datacounter <= (others => '0');
        active_counter <= '0';
        active_data_counter <= '0';
        reset_data_counter <= '0';
        reset_counter <= '0';
        reset_timeout <= '0';
        counter_value <= (others => '0');
      elsif rising_edge(clk) then
        if (timeout_counter > apdu_conf.wait_counter and reset_timeout = '0') or reset_ext = '1' then
          state <= CLA;--timeout
          reset_timeout <= '1';
          reset_counter <= '0';
          active_counter <= '0';
          active_data_counter <= '0';
          reset_data_counter <= '0';
          end_trig <= '0';
        else
          reset_timeout <= '0';
          active_counter <= '0';
          reset_counter <= '0';
          active_data_counter <= '0';
          reset_data_counter <= '0';
          case state is
            when CLA =>
              end_trig <= '0';
              if io_rdy = '1' then
                state <= INS;
                bcla <= io_data;
                reset_timeout <= '1';
              else
                state <= CLA;
              end if;

            when INS =>
              if io_rdy = '1' then
                state <= P1;
                bins <= io_data;
                reset_timeout <= '1';
              else
                state <= INS;
              end if;

            when P1 =>
              if io_rdy = '1' then
                state <= P2;
                bp1 <= io_data;
                reset_timeout <= '1';
              else
                state <= P1;
              end if;

            when P2 =>
              if io_rdy = '1' then
                state <= P3;
                bp2 <= io_data;
                reset_timeout <= '1';
              else
                state <= P2;
              end if;

            when P3 =>
              if io_rdy = '1' then
                state <= PB;
                datacounter <= (others => '0');
                bp3 <= io_data;
                reset_timeout <= '1';
                reset_counter <= '1';
                reset_data_counter <= '1';
              else
                state <= P3;
              end if;

            when PB => -- procedure byte that's where the FSM diverges
              active_counter <= '1';
              if io_rdy = '1' then
                bsw1 <= io_data;
                reset_timeout <= '1';
                if io_data = X"60" then -- wait for next PB
                  state <= PB;
                elsif ((io_data and X"F0") = X"60") or ((io_data and X"F0") = X"90") then -- SW1
                  state <= SW2;
                elsif io_data = bins then -- data about to transit
                  state <= DATABATCH;
                  reset_counter <= '1';
                elsif (io_data xor X"FF") = bins then
                  state <= DATABYTE;
                  reset_counter <= '1';
                else
                  state <= CLA;-- ERROR
                end if;
              else
                state <= PB;
              end if;

            when DATABATCH => -- transfer all data byte at once
              active_data_counter <= '1';
              active_counter <= '1';
              if io_rdy = '1' then
                datacounter <= datacounter + 1;
                reset_timeout <= '1';
                if datacounter = expected_len-1 then
                  state <= PB;
                else
                  state <= DATABATCH;
                end if;
              else
                state <= DATABATCH;
              end if;

            when DATABYTE => -- transfer one databyte then procedure byte
              active_counter <= '1';
              if io_rdy = '1' then
                datacounter <= datacounter + 1;
                state <= PB;
                reset_timeout <= '1';
              else
                state <= DATABYTE;
              end if;

            when SW2 =>
              if io_rdy = '1' then
                bsw2 <= io_data;
                state <= CLA;
                end_trig <= '1';
                counter_value <= timing_counter - data_timing_counter;
              else
                state <= SW2;
              end if;

          end case;
        end if;
      end if;
    end process;

end behavior;
