library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use work.triggy_common.all;
use work.common.all;

entity uart_trig is
  port(clk:           in std_logic;
       reset:         in std_logic;

       conf:          in UartTrigConf;

       data_en:       in std_logic;
       data:          in byte;

       trig_o:        out std_logic
       );
end uart_trig;

architecture behavior of uart_trig is

  component pulser is
    port(clk_in:       in std_logic;
         clk_out:       in std_logic;
         reset:     in std_logic;
         pulse_in:  in std_logic;
         pulse_out: out std_logic
         );
  end component;

  signal last_bytes: UartSequence;--memorize last bytes
  signal matching_vector: std_logic_vector(SEQUENCE_SIZE-1 downto 0);
  signal matching_and: std_logic_vector(SEQUENCE_SIZE-1 downto 0);
  signal matching: std_logic;

  begin

    matching_and(SEQUENCE_SIZE-1) <= matching_vector(SEQUENCE_SIZE-1);

    and_comp: for i in 0 to SEQUENCE_SIZE-2 generate
      matching_and(i) <= matching_vector(i) and matching_and(i+1);
    end generate;
    matching <= matching_and(0);

    fill_last_bytes: process(clk, reset)
      begin
        if reset = '1' then
          last_bytes <= (others => (others => '0'));
        elsif rising_edge(clk) then
          if data_en = '1' then
            for i in 1 to SEQUENCE_SIZE-1 loop
              last_bytes(i-1) <= last_bytes(i);
            end loop;
            last_bytes(SEQUENCE_SIZE-1) <= data;
          end if;
        end if;
    end process;

    gen_vec: for i in 0 to SEQUENCE_SIZE-1 generate
      matching_vector(i) <= '1' when (last_bytes(i) = conf.sequence(i)) or (i < (SEQUENCE_SIZE-conf.sequence_size)) else '0';
    end generate;


    trig_pulser: pulser
      port map(
        clk_in      => clk,
        clk_out     => clk,
        reset       => reset,
        pulse_in    => matching,
        pulse_out   => trig_o
      );


end behavior;
