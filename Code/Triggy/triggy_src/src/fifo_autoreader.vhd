library IEEE;
use IEEE.std_logic_1164.all;

-- generate a pulse of one clock cycle on rising_edge of pulse_in
entity fifo_autoreader is
  port(clk:             in std_logic;
       reset:           in std_logic;
       enable:          in std_logic;
       fifo_empty:      in std_logic;
       fifo_data:       in std_logic_vector(7 downto 0);
       fifo_read_order: out std_logic;

       data:            out std_logic_vector(7 downto 0);
       data_en:         out std_logic
       );
end fifo_autoreader;

architecture behavior of fifo_autoreader is

  signal fifo_read_order_buf: std_logic;
  signal wait_sig: std_logic;

  begin

    fifo_read_order <= fifo_read_order_buf;
    -- data_en <= wait_sig;

    process(clk, reset)-- read from tx_fifo to send to PC
      begin
        if reset = '1' then
          fifo_read_order_buf <= '0';
        elsif rising_edge(clk) then
          if fifo_empty = '0' and enable = '1' and fifo_read_order_buf = '0' then -- we have data and we can send it
            fifo_read_order_buf <= '1';
          else
            fifo_read_order_buf <= '0';
          end if;
        end if;
    end process;

    process(clk, reset)-- read from tx_fifo to send to PC, next clock cycle data go from fifo to uart
      begin
        if reset = '1' then
          data <= (others => '0');
          data_en <= '0';
          wait_sig <= '0';
        elsif rising_edge(clk) then
          if wait_sig = '1' then
            data <= fifo_data;
            data_en <= '1';
          else
            data_en <= '0';
          end if;
          if fifo_read_order_buf = '1' then
            wait_sig <= '1';
          else
            wait_sig <= '0';
          end if;
        end if;
    end process;

end behavior;
