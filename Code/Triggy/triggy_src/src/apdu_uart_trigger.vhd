library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use work.triggy_common.all;
use work.common.all;

entity apdu_uart_trigger is
  port(clk:           in std_logic;
       reset:         in std_logic;

       io:            in std_logic; --the intercepted uart wire

       apdu_conf:       in APDUConf;
       uart_trig_conf:  in UartTrigConf;
       uart_conf:       in UartConf;

       --uart trig
       uart_trig_o:     out std_logic;

       --apdu follower output
       counter_updated: out std_logic;
       counter_value:   out u32;
       debug:           out byte
       );
end apdu_uart_trigger;

architecture behavior of apdu_uart_trigger is



  component baud_gen is
    port(main_clk:  in std_logic;
         reset:     in std_logic;
         baud_clk:  out std_logic;--generated signal
         --tot for divider is 2^32
         uart_conf:  in UartConf);--step for divider
  end component;

  component uart_rx is
    port(baud_clk:    in std_logic;
         reset:       in std_logic;
         uart_conf:   in UartConf;
         rx:          in std_logic;--rx wire
         rx_data:     out std_logic_vector(7 downto 0);--data byte read
         rx_rdy:      out std_logic;
         error_flag:  out std_logic;
         tick_o:    out std_logic
         );
  end component;

  component fifo IS
    PORT (
      clk : IN STD_LOGIC;
      rst : IN STD_LOGIC;
      din : IN STD_LOGIC_VECTOR(7 DOWNTO 0);
      wr_en : IN STD_LOGIC;
      rd_en : IN STD_LOGIC;
      dout : OUT STD_LOGIC_VECTOR(7 DOWNTO 0);
      full : OUT STD_LOGIC;
      empty : OUT STD_LOGIC
    );
  end component;

  component pulser is
    port(clk_in:       in std_logic;
         clk_out:       in std_logic;
         reset:     in std_logic;
         pulse_in:  in std_logic;
         pulse_out: out std_logic
         );
  end component;

  component fifo_autoreader is
    port(clk:             in std_logic;
         reset:           in std_logic;
         enable:          in std_logic;
         fifo_empty:      in std_logic;
         fifo_data:       in std_logic_vector(7 downto 0);
         fifo_read_order: out std_logic;

         data:            out std_logic_vector(7 downto 0);
         data_en:         out std_logic
         );
  end component;

  component uart_trig is
    port(clk:           in std_logic;
         reset:         in std_logic;

         conf:          in UartTrigConf;

         data_en:       in std_logic;
         data:          in byte;

         trig_o:        out std_logic
         );
  end component;

  component apdu_follower is
    port(clk:           in std_logic;
         reset:         in std_logic;
         reset_ext:     in std_logic;

         io_data:       in byte;
         io_rdy:        in std_logic;

         apdu_conf:     in APDUConf;

         --apdu follower output
         counter_updated: out std_logic;
         counter_value:   out u32;
         debug:           out byte
         );
  end component;

  signal baud_clk: std_logic;
  signal rx_rdy, pulse_rx_rdy, error_flag: std_logic;
  signal rx_data: byte;

  signal fifo_rx_read, fifo_rx_empty, fifo_rx_data_en: std_logic;
  signal fifo_rx_dout, fifo_rx_data: byte;

  signal tick_rx: std_logic;

  begin


    -- UART -> FIFO -> autoreader -> apdu follower
    --                            -> uart trig
    -- debug(0) <= rx_rdy;
    -- debug(1) <= error_flag;
    -- debug(2) <= pulse_rx_rdy;
    -- debug(3) <= fifo_rx_read;
    -- debug(4) <= fifo_rx_data_en;
    -- debug(5) <= baud_clk;
    -- debug(6) <= io;
    -- debug(7) <= tick_rx;

    -- debug(6 downto 0) <= fifo_rx_dout(6 downto 0);
    -- debug(7) <= fifo_rx_read;

    -- debug <= X"00";

    baud_gen_inst: baud_gen
      port map(main_clk   => clk,
         reset            => reset,
         baud_clk         => baud_clk,
         uart_conf        => uart_conf);--step for divider

     rx_component: uart_rx port map(
       baud_clk      => baud_clk,
       reset         => reset,
       uart_conf     => uart_conf,
       rx            => io,
       rx_data       => rx_data,
       rx_rdy        => rx_rdy,
       error_flag    => error_flag,
       tick_o        => tick_rx
     );

      --rx_rdy is in the baud clk domain, we need a trigger in the clk domain
     rx_pulser: pulser
       port map(
         clk_in      => baud_clk,
         clk_out     => clk,
         reset       => reset,
         pulse_in    => rx_rdy,
         pulse_out   => pulse_rx_rdy
       );

     rx_fifo: fifo
         port map(
         rst           => reset,
         clk           => clk,
         din           => rx_data,
         wr_en         => pulse_rx_rdy,
         rd_en         => fifo_rx_read,
         dout          => fifo_rx_dout,
         full          => open,
         empty         => fifo_rx_empty
         );

     fifo_rx_reader: fifo_autoreader
       port map(
         clk               => clk,
         reset             => reset,
         enable            => '1',
         fifo_empty        => fifo_rx_empty,
         fifo_data         => fifo_rx_dout,
         fifo_read_order   => fifo_rx_read,

         data              => fifo_rx_data,
         data_en           => fifo_rx_data_en
       );

       -- APDU follower

       follower: apdu_follower
        port map(clk      => clk,
            reset         => reset,
            reset_ext     => '0', -- for now rely on timeout only

            io_data       => fifo_rx_data,
            io_rdy        => fifo_rx_data_en,

            apdu_conf     => apdu_conf,

            --apdu follower output
            counter_updated     => counter_updated,
            counter_value       => counter_value,
            debug               => debug
            );

       -- Uart trig
       uart_trigger: uart_trig
         port map(clk     => clk,
              reset       => reset,

              conf        => uart_trig_conf,

              data_en     => fifo_rx_data_en,
              data        => fifo_rx_data,

              trig_o      => uart_trig_o
              );

end behavior;
