library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use work.common.all;

package triggy_common is



  -- config addresses
  constant STATE_ADD: Integer := 0;--size 1
  constant STATE_SIZE: Integer := 1;

  constant TRIGGERS_ADD: Integer := STATE_ADD+STATE_SIZE;--1
  constant TRIGGER_CONF_SIZE: Integer := 9;
  constant NB_TRIGGERS: Integer := 4;

  constant APDU_FOLLOWER_CONFIG_ADD: Integer := TRIGGERS_ADD+TRIGGER_CONF_SIZE*NB_TRIGGERS;--1+9*4=37
  constant APDU_FOLLOWER_CONFIG_SIZE: Integer := 1;
  constant APDU_FOLLOWER_WAIT_COUNTER_ADD: Integer := APDU_FOLLOWER_CONFIG_ADD+APDU_FOLLOWER_CONFIG_SIZE;--38
  constant APDU_FOLLOWER_WAIT_COUNTER_SIZE: Integer := 4;
  constant APDU_FOLLOWER_HEADER_SELECT_ADD: Integer := APDU_FOLLOWER_WAIT_COUNTER_ADD+APDU_FOLLOWER_WAIT_COUNTER_SIZE;--42
  constant APDU_FOLLOWER_HEADER_SELECT_SIZE: Integer := 4;
  constant COUNTER_ADD: Integer := APDU_FOLLOWER_HEADER_SELECT_ADD+APDU_FOLLOWER_HEADER_SELECT_SIZE;--46
  constant COUNTER_SIZE: Integer := 4;

  constant SEQUENCE_SIZE: Integer := 8;
  constant UART_INTERCEPT_STEP_ADD: Integer := COUNTER_ADD+COUNTER_SIZE;--50
  constant UART_INTERCEPT_STEP_SIZE: Integer := 4;
  constant UART_INTERCEPT_CONFIG_ADD: Integer := UART_INTERCEPT_STEP_ADD+UART_INTERCEPT_STEP_SIZE;--54
  constant UART_INTERCEPT_CONFIG_SIZE: Integer := 1;
  constant UART_INTERCEPT_SEQ_SIZE_ADD: Integer := UART_INTERCEPT_CONFIG_ADD+UART_INTERCEPT_CONFIG_SIZE;--55
  constant UART_INTERCEPT_SEQ_SIZE_SIZE: Integer := 1;
  constant UART_INTERCEPT_SEQ_ADD: Integer := UART_INTERCEPT_SEQ_SIZE_ADD+UART_INTERCEPT_SEQ_SIZE_SIZE;--56
  constant UART_INTERCEPT_SEQ_SIZE: Integer := SEQUENCE_SIZE;



  -- size of config
  constant MAX_ADDRESS: Integer := UART_INTERCEPT_SEQ_ADD+UART_INTERCEPT_SEQ_SIZE;

  type APDUHeader is array (0 to 3) of byte;
  type APDUConf is
    record
      wait_counter:           u32; -- define a timeout
      header_select:          APDUHeader;
      header_select_en:       std_logic;
  end record;

  type TrigConf is
    record
      trig_wait:  unsigned(31 downto 0);--wait before trig in and rising edge
      trig_width: unsigned(31 downto 0);--trig up width
      idle_polarity:   std_logic;--logic level when idlec
      trig_source: std_logic_vector(2 downto 0);--000=> immediate, 001=>uart sequence, 010=>apdu follower, 011=>external trigger, 1XX=>trigXX
      repeat: std_logic;
  end record;

  type TrigConfs is array (0 to NB_TRIGGERS-1) of TrigConf;

  type UartSequence is array (0 to SEQUENCE_SIZE-1) of byte;
  type UartTrigConf is
    record
      sequence:       UartSequence;
      sequence_size:  u8;
  end record;

end triggy_common;
