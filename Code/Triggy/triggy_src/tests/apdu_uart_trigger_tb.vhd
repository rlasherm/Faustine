library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use work.common.all;
use work.triggy_common.all;

entity apdu_uart_trigger_tb is
end apdu_uart_trigger_tb;

architecture behavior of apdu_uart_trigger_tb is

  component apdu_uart_trigger is
    port(clk:           in std_logic;
         reset:         in std_logic;

         io:            in std_logic; --the intercepted uart wire

         apdu_conf:       in APDUConf;
         uart_trig_conf:  in UartTrigConf;
         uart_conf:       in UartConf;

         --uart trig
         uart_trig_o:     out std_logic;

         --apdu follower output
         counter_updated: out std_logic;
         counter_value:   out u32;
         debug:           out byte
         );
  end component;

  constant clk_period : time := 10 ns;
  constant bit_period : time := 8681 ns; -- 115200 bauds
  constant idle_polarity : std_logic := '1';

  signal clk : std_logic := '0';
  signal reset : std_logic := '0';


  signal io: std_logic;

  signal uart_trig, counter_up: std_logic;
  signal cvalue: u32;
  signal debug: byte;

  signal uart_conf: UartConf := (idle_polarity       => idle_polarity,
                baud_gen_step       => X"04B7F5A5",
                start_bits          => One,
                stop_bits           => One,
                parity              => None,
                data_bits           => Eight);

  signal apdu_conf: APDUConf := (
    wait_counter                => X"00001000",
    header_select               => (X"00", X"00", X"00", X"00"), --((others=> (others=>'0')))
    header_select_en            => '0'
  );

  signal uart_trig_conf: UartTrigConf := ( sequence      => (X"00", X"00", X"00", X"00", X"00", X"00", X"64", X"67"),
                      sequence_size => X"02");

  begin

    -- Clock process definitions: clock with 50% duty cycle is generated here.
    clk_process :process
    begin
        clk <= '0';
        wait for clk_period/2;
        clk <= '1';
        wait for clk_period/2;
    end process;



    dut: apdu_uart_trigger
    port map(clk            => clk,
         reset              => reset,
         io                 => io,

         apdu_conf          => apdu_conf,
         uart_trig_conf     => uart_trig_conf,
         uart_conf          => uart_conf,

         --uart trig
         uart_trig_o        => uart_trig,

         --apdu follower output
         counter_updated    => counter_up,
         counter_value      => cvalue,
         debug              => debug
         );

    -- Stimuli
    stim: process
    begin
      reset <= '0';
      io <= idle_polarity;
      wait for 5*clk_period;
      reset <= '1';
      wait for clk_period;
      reset <= '0';
      wait for 3*clk_period;
      wait for bit_period;

      --write

      --function write

      io <=  not idle_polarity;-- start
      wait for bit_period; -- 0x01
      io <= '1';-- bit 0
      wait for bit_period;
      io <= '0';-- bit 1
      wait for bit_period;
      io <= '0';-- bit 2
      wait for bit_period;
      io <= '0';-- bit 3
      wait for bit_period;
      io <= '0';-- bit 4
      wait for bit_period;
      io <= '0';-- bit 5
      wait for bit_period;
      io <= '0';-- bit 6
      wait for bit_period;
      io <= '0';-- bit 7
      wait for bit_period;
      io <= idle_polarity;-- stop
      wait for bit_period;

      -- write address

      io <=  not idle_polarity;-- start
      wait for bit_period; -- 0x00
      io <= '0';-- bit 0
      wait for bit_period;
      io <= '0';-- bit 1
      wait for bit_period;
      io <= '0';-- bit 2
      wait for bit_period;
      io <= '0';-- bit 3
      wait for bit_period;
      io <= '0';-- bit 4
      wait for bit_period;
      io <= '0';-- bit 5
      wait for bit_period;
      io <= '0';-- bit 6
      wait for bit_period;
      io <= '0';-- bit 7
      wait for bit_period;
      io <= idle_polarity;-- stop
      wait for bit_period;

      -- write value

      io <=  not idle_polarity;-- start
      wait for bit_period; -- 0x64
      io <= '0';-- bit 0
      wait for bit_period;
      io <= '0';-- bit 1
      wait for bit_period;
      io <= '1';-- bit 2
      wait for bit_period;
      io <= '0';-- bit 3
      wait for bit_period;
      io <= '0';-- bit 4
      wait for bit_period;
      io <= '1';-- bit 5
      wait for bit_period;
      io <= '1';-- bit 6
      wait for bit_period;
      io <= '0';-- bit 7
      wait for bit_period;
      io <= idle_polarity;-- stop
      wait for bit_period;

      --read

      --function read

      io <=  not idle_polarity;-- start
      wait for bit_period; -- 0x67
      io <= '1';-- bit 0
      wait for bit_period;
      io <= '1';-- bit 1
      wait for bit_period;
      io <= '1';-- bit 2
      wait for bit_period;
      io <= '0';-- bit 3
      wait for bit_period;
      io <= '0';-- bit 4
      wait for bit_period;
      io <= '1';-- bit 5
      wait for bit_period;
      io <= '1';-- bit 6
      wait for bit_period;
      io <= '0';-- bit 7
      wait for bit_period;
      io <= idle_polarity;-- stop
      wait for bit_period;

      -- read address

      io <=  not idle_polarity;-- start
      wait for bit_period; -- 0x00
      io <= '0';-- bit 0
      wait for bit_period;
      io <= '0';-- bit 1
      wait for bit_period;
      io <= '0';-- bit 2
      wait for bit_period;
      io <= '0';-- bit 3
      wait for bit_period;
      io <= '0';-- bit 4
      wait for bit_period;
      io <= '0';-- bit 5
      wait for bit_period;
      io <= '0';-- bit 6
      wait for bit_period;
      io <= '0';-- bit 7
      wait for bit_period;
      io <= idle_polarity;-- stop
      wait for 2*bit_period;
      wait for 20*bit_period;

    end process;

end behavior;
