library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use work.common.all;
use work.triggy_common.all;

entity triggy_top_tb is
end triggy_top_tb;

architecture behavior of triggy_top_tb is

  component triggy_top is
    port(clk_in:           in std_logic;
         reset:         in std_logic;

         -- PC Uart
         rx_from_pc:          in std_logic;
         tx_to_pc:            out std_logic;

         --intercept uart
         uart_intercept:  in std_logic;

        --trigs
        trig_ext:         in std_logic;
        triggers:         out std_logic_vector(NB_TRIGGERS-1 downto 0);

        --debug
        led:              out std_logic_vector(7 downto 0)
         );
  end component;

  constant clk_period : time := 10 ns;
  constant bit_period : time := 8681 ns; -- 115200 bauds
  constant idle_polarity : std_logic := '1';

  signal clk : std_logic := '0';
  signal reset : std_logic := '0';

  signal rx, tx: std_logic;
  signal led: std_logic_vector(7 downto 0);

  signal io: std_logic;

  signal triggers: std_logic_vector(NB_TRIGGERS-1 downto 0);
  signal trig_ext: std_logic;

  begin

    -- Clock process definitions: clock with 50% duty cycle is generated here.
   clk_process :process
   begin
        clk <= '0';
        wait for clk_period/2;
        clk <= '1';
        wait for clk_period/2;
   end process;

   io <= '0';
   trig_ext <= '0';

   dut: triggy_top
    port map(
      clk_in      => clk,
      reset       => reset,
      rx_from_pc  => rx,
      tx_to_pc    => tx,
      uart_intercept  => io,
      trig_ext        => trig_ext,
      triggers        => triggers,
      led         => led
    );

    -- Stimuli
    stim: process
     begin
       reset <= '0';
       rx <= idle_polarity;
       wait for clk_period;
       reset <= '1';
       wait for clk_period;
       reset <= '0';
       wait for 3*clk_period;
       wait for bit_period;

       --write

       --function write

       rx <=  not idle_polarity;-- start
       wait for bit_period; -- 0x01
       rx <= '1';-- bit 0
       wait for bit_period;
       rx <= '0';-- bit 1
       wait for bit_period;
       rx <= '0';-- bit 2
       wait for bit_period;
       rx <= '0';-- bit 3
       wait for bit_period;
       rx <= '0';-- bit 4
       wait for bit_period;
       rx <= '0';-- bit 5
       wait for bit_period;
       rx <= '0';-- bit 6
       wait for bit_period;
       rx <= '0';-- bit 7
       wait for bit_period;
       rx <= idle_polarity;-- stop
       wait for 2*bit_period;

       -- write address

       rx <=  not idle_polarity;-- start
       wait for bit_period; -- 0x00
       rx <= '0';-- bit 0
       wait for bit_period;
       rx <= '0';-- bit 1
       wait for bit_period;
       rx <= '0';-- bit 2
       wait for bit_period;
       rx <= '0';-- bit 3
       wait for bit_period;
       rx <= '0';-- bit 4
       wait for bit_period;
       rx <= '0';-- bit 5
       wait for bit_period;
       rx <= '0';-- bit 6
       wait for bit_period;
       rx <= '0';-- bit 7
       wait for bit_period;
       rx <= idle_polarity;-- stop
       wait for 2*bit_period;

       -- write value

       rx <=  not idle_polarity;-- start
       wait for bit_period; -- 0x55
       rx <= '1';-- bit 0
       wait for bit_period;
       rx <= '0';-- bit 1
       wait for bit_period;
       rx <= '1';-- bit 2
       wait for bit_period;
       rx <= '0';-- bit 3
       wait for bit_period;
       rx <= '1';-- bit 4
       wait for bit_period;
       rx <= '0';-- bit 5
       wait for bit_period;
       rx <= '1';-- bit 6
       wait for bit_period;
       rx <= '0';-- bit 7
       wait for bit_period;
       rx <= idle_polarity;-- stop
       wait for 2*bit_period;

       --read

       --function read

       rx <=  not idle_polarity;-- start
       wait for bit_period; -- 0x00
       rx <= '0';-- bit 0
       wait for bit_period;
       rx <= '0';-- bit 1
       wait for bit_period;
       rx <= '0';-- bit 2
       wait for bit_period;
       rx <= '0';-- bit 3
       wait for bit_period;
       rx <= '0';-- bit 4
       wait for bit_period;
       rx <= '0';-- bit 5
       wait for bit_period;
       rx <= '0';-- bit 6
       wait for bit_period;
       rx <= '0';-- bit 7
       wait for bit_period;
       rx <= idle_polarity;-- stop
       wait for 2*bit_period;

       -- read address

       rx <=  not idle_polarity;-- start
       wait for bit_period; -- 0x00
       rx <= '0';-- bit 0
       wait for bit_period;
       rx <= '0';-- bit 1
       wait for bit_period;
       rx <= '0';-- bit 2
       wait for bit_period;
       rx <= '0';-- bit 3
       wait for bit_period;
       rx <= '0';-- bit 4
       wait for bit_period;
       rx <= '0';-- bit 5
       wait for bit_period;
       rx <= '0';-- bit 6
       wait for bit_period;
       rx <= '0';-- bit 7
       wait for bit_period;
       rx <= idle_polarity;-- stop
       wait for 2*bit_period;
       wait for 20*bit_period;

    end process;

end behavior;
