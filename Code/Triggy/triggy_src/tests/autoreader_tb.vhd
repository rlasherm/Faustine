library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use work.common.all;

entity autoreader_tb is
end autoreader_tb;

architecture behavior of autoreader_tb is

  component fifo_autoreader is
    port(clk:             in std_logic;
         reset:           in std_logic;
         enable:          in std_logic;
         fifo_empty:      in std_logic;
         fifo_data:       in std_logic_vector(7 downto 0);
         fifo_read_order: out std_logic;

         data:            out std_logic_vector(7 downto 0);
         data_en:         out std_logic
         );
  end component;

  component fifo IS
    PORT (
      clk : IN STD_LOGIC;
      rst : IN STD_LOGIC;
      din : IN STD_LOGIC_VECTOR(7 DOWNTO 0);
      wr_en : IN STD_LOGIC;
      rd_en : IN STD_LOGIC;
      dout : OUT STD_LOGIC_VECTOR(7 DOWNTO 0);
      full : OUT STD_LOGIC;
      empty : OUT STD_LOGIC
    );
  end component;

  signal clk : std_logic := '0';
  signal reset : std_logic := '0';


  signal fifo_isempty, fifo_read_order, data_en : std_logic;

  signal fifo_write_order: std_logic;
  signal fifo_write_data, fifo_read_data, data_byte: std_logic_vector(7 downto 0);


  -- Clock period definitions
   constant clk_period : time := 10 ns;

  begin
    read_fifo: fifo_autoreader
      port map(
        clk               => clk,
        reset             => reset,
        enable            => '1',
        fifo_empty        => fifo_isempty,
        fifo_data         => fifo_read_data,
        fifo_read_order   => fifo_read_order,

        data              => data_byte,
        data_en           => data_en
      );

    dut_fifo: fifo
        port map(
        rst           => reset,
        clk           => clk,
        din           => fifo_write_data,
        wr_en         => fifo_write_order,
        rd_en         => fifo_read_order,
        dout          => fifo_read_data,
        full          => open,
        empty         => fifo_isempty
        );

    -- Clock process definitions: clock with 50% duty cycle is generated here.
   clk_process :process
   begin
        clk <= '0';
        wait for clk_period/2;
        clk <= '1';
        wait for clk_period/2;
   end process;


   -- Stimuli
   stim: process
    begin
      reset <= '0';
      fifo_write_data <= X"00";
      fifo_write_order <= '0';
      wait for 3.5*clk_period;
      reset <= '1';
      wait for 6*clk_period;
      reset <= '0';
      wait for 5*clk_period;
      fifo_write_data <= X"55";
      fifo_write_order <= '1';
      wait for clk_period;
      fifo_write_order <= '0';
      wait for clk_period;
      fifo_write_data <= X"01";
      fifo_write_order <= '1';
      wait for clk_period;
      fifo_write_data <= X"02";
      fifo_write_order <= '1';
      wait for clk_period;
      fifo_write_order <= '0';
      wait for 4*clk_period;

   end process;
end behavior;
