library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use work.common.all;
use work.triggy_common.all;

entity uart_trig_tb is
end uart_trig_tb;

architecture behavior of uart_trig_tb is

  component uart_trig is
    port(clk:           in std_logic;
         reset:         in std_logic;

         conf:          in UartTrigConf;

         data_en:       in std_logic;
         data:          in byte;

         trig_o:        out std_logic
         );
  end component;

  signal clk : std_logic := '0';
  signal reset : std_logic := '0';

  signal data_en, tout: std_logic;
  signal data: byte;
  signal tconf: UartTrigConf := (sequence => (X"00", X"00", X"00", X"00", X"01", X"01", X"01", X"01"),
                              sequence_size => X"02");

  -- Clock period definitions
   constant clk_period : time := 10 ns;

  begin

    dut: uart_trig
      port map(
           clk        => clk,
           reset      => reset,

           conf       => tconf,

           data_en    => data_en,
           data       => data,

           trig_o     => tout
      );


    -- Clock process definitions: clock with 50% duty cycle is generated here.
   clk_process :process
   begin
        clk <= '0';
        wait for clk_period/2;
        clk <= '1';
        wait for clk_period/2;
   end process;


   -- Stimuli
   stim: process
    begin
      reset <= '0';
      data_en <= '0';
      wait for 1.5*clk_period;
      reset <= '1';
      wait for clk_period;
      reset <= '0';
      wait for 4*clk_period;
      data_en <= '1';
      data <= X"FF";
      wait for clk_period;
      data <= X"01";
      wait for clk_period;
      data <= X"01";
      wait for clk_period;
      data <= X"01";
      wait for clk_period;
      data <= X"01";
      wait for clk_period;
      data <= X"02";
      wait for clk_period;
      data_en <= '0';
      wait for 4*clk_period;
      data <= X"03";
      data_en <= '1';
      wait for clk_period;
      data_en <= '0';
      wait for 10*clk_period;

   end process;
end behavior;
