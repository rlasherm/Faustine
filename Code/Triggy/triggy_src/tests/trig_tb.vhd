library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use work.common.all;
use work.triggy_common.all;

entity trig_tb is
end trig_tb;

architecture behavior of trig_tb is

  component trig_module is
    port(clk:             in std_logic;
         reset:           in std_logic;

         trig_in:         in std_logic;--start trig
         trig_conf:       in TrigConf;

         trig_out:        out std_logic
         );
  end component;

  signal clk : std_logic := '0';
  signal reset : std_logic := '0';

  signal tin, tout: std_logic;
  signal tconf: TrigConf := (trig_wait => X"00000004",
                              trig_width => X"00000004",
                              idle_polarity => '0',
                              repeat => '1',
                              trig_source => "000");

  -- Clock period definitions
   constant clk_period : time := 10 ns;

  begin

    dut: trig_module
      port map(clk      => clk,
           reset        => reset,

           trig_in      => tin,
           trig_conf    => tconf,

           trig_out     => tout
           );


    -- Clock process definitions: clock with 50% duty cycle is generated here.
   clk_process :process
   begin
        clk <= '0';
        wait for clk_period/2;
        clk <= '1';
        wait for clk_period/2;
   end process;


   -- Stimuli
   stim: process
    begin
      reset <= '0';
      tin <= '0';
      wait for 1.5*clk_period;
      reset <= '1';
      wait for clk_period;
      reset <= '0';
      wait for 4*clk_period;
      tin <= '1';
      wait for clk_period;
      tin <= '0';
      wait for 30*clk_period;

      tin <= '1';
      wait for 20*clk_period;
      tin <= '0';
      wait for 300*clk_period;

   end process;
end behavior;
