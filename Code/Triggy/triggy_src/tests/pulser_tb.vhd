library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use work.common.all;

entity pulser_tb is
end pulser_tb;

architecture behavior of pulser_tb is

  component pulser is
    port(clk_in:   in std_logic;
         clk_out:   in std_logic;
         reset:     in std_logic;
         pulse_in:  in std_logic;
         pulse_out: out std_logic
         );
  end component;

  signal clk_o : std_logic := '0';
  signal clk_i : std_logic := '0';
  signal reset : std_logic := '0';

  signal pin, pout: std_logic;

  -- Clock period definitions
   constant clko_period : time := 100 ns;
   constant clki_period : time := 9 ns;

  begin
    --dut instance
    dut: pulser port map (
      clk_in      => clk_i,
      clk_out     => clk_o,
      reset       => reset,
      pulse_in    => pin,
      pulse_out   => pout
    );

    -- Clock process definitions: clock with 50% duty cycle is generated here.
   clko_process :process
   begin
        clk_o <= '0';
        wait for clko_period/2;
        clk_o <= '1';
        wait for clko_period/2;
   end process;

   clki_process :process
   begin
        clk_i <= '0';
        wait for clki_period/2;
        clk_i <= '1';
        wait for clki_period/2;
   end process;

   -- Stimuli
   stim: process
    begin
      wait for 1.5*clki_period;
      pin <= '0';
      reset <= '1';
      wait for clki_period;
      reset <= '0';
      wait for 4*clki_period;
      pin <= '1';
      wait for clki_period;
      pin <= '0';
      wait for 100*clki_period;

   end process;
end behavior;
