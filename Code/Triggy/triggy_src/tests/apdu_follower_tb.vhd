library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use work.common.all;
use work.triggy_common.all;

entity apdu_follower_tb is
end apdu_follower_tb;

architecture behavior of apdu_follower_tb is

  component apdu_follower is
    port(clk:           in std_logic;
         reset:         in std_logic;
         reset_ext:     in std_logic;

         io_data:       in byte;
         io_rdy:        in std_logic;

         apdu_conf:     in APDUConf;

         --apdu follower output
         counter_updated: out std_logic;
         counter_value:   out u32;
         debug:           out byte
         );
  end component;

  signal clk : std_logic := '0';
  signal reset : std_logic := '0';
  signal reset_ext: std_logic := '0';

  signal data, debug: byte;
  signal data_en: std_logic;

  signal apdu_conf: APDUConf := (
    wait_counter                => X"00001000",
    header_select               => (X"00", X"00", X"00", X"00"), --((others=> (others=>'0')))
    header_select_en            => '0'
  );

  signal counter: u32;
  signal counter_update: std_logic;

  -- Clock period definitions
   constant clk_period : time := 10 ns;

  begin

    -- Clock process definitions: clock with 50% duty cycle is generated here.
   clk_process :process
   begin
        clk <= '0';
        wait for clk_period/2;
        clk <= '1';
        wait for clk_period/2;
   end process;

   dut: apdu_follower
    port map(clk      => clk,
        reset         => reset,
        reset_ext     => reset_ext,

        io_data       => data,
        io_rdy        => data_en,

        apdu_conf     => apdu_conf,

        --apdu follower output
        counter_updated     => counter_update,
        counter_value       => counter,
        debug               => debug
        );

    -- Stimuli
    stim: process
     begin
       reset <= '0';
       data <= X"00";
       data_en <= '0';
       wait for 1.5*clk_period;
       reset <= '1';
       wait for clk_period;
       reset <= '0';
       wait for 4*clk_period;
       data <= X"01"; --CLA
       data_en <= '1';
       wait for clk_period;
       data <= X"02"; --INS
       data_en <= '1';
       wait for clk_period;
       data <= X"03"; --P1
       data_en <= '1';
       wait for clk_period;
       data <= X"04"; --P2
       data_en <= '1';
       wait for clk_period;
       data <= X"00"; --P3
       data_en <= '1';
       wait for clk_period;
       data_en <= '0';

       wait for 15*clk_period;
       data <= X"90"; --SW1
       data_en <= '1';
       wait for clk_period;
       data <= X"00"; --SW2
       data_en <= '1';
       wait for clk_period;
       data_en <= '0';

      -- new try
       apdu_conf.header_select <= (X"11", X"12", X"13", X"14");
       apdu_conf.header_select_en <= '1';

       wait for 10*clk_period;
       data <= X"11"; --CLA
       data_en <= '1';
       wait for clk_period;
       data <= X"12"; --INS
       data_en <= '1';
       wait for clk_period;
       data <= X"13"; --P1
       data_en <= '1';
       wait for clk_period;
       data <= X"14"; --P2
       data_en <= '1';
       wait for clk_period;
       data <= X"01"; --P3
       data_en <= '1';

       wait for clk_period;
       data <= X"12"; --Ack (INS)
       data_en <= '1';

       wait for clk_period;
       data <= X"FF"; --CMD0
       data_en <= '1';
      --  wait for clk_period;
      --  data <= X"FE"; --CMD1
      --  data_en <= '1';
      --  wait for clk_period;
      --  data <= X"FD"; --CMD2
      --  data_en <= '1';

       wait for clk_period;
       data_en <= '0';

       wait for clk_period;
       data <= X"91"; --SW1
       data_en <= '1';
       wait for clk_period;
       data <= X"92"; --SW2
       data_en <= '1';
       wait for clk_period;
       data_en <= '0';

       -- new try
        apdu_conf.header_select <= (X"11", X"13", X"13", X"14");
        apdu_conf.header_select_en <= '0';

        wait for 10*clk_period;
        data <= X"11"; --CLA
        data_en <= '1';
        wait for clk_period;
        data <= X"12"; --INS
        data_en <= '1';
        wait for clk_period;
        data <= X"13"; --P1
        data_en <= '1';
        wait for clk_period;
        data <= X"14"; --P2
        data_en <= '1';
        wait for clk_period;
        data <= X"03"; --P3
        data_en <= '1';

        wait for clk_period;
        data_en <= '0';
        wait for 3*clk_period;

        wait for clk_period;
        data <= X"12"; --Ack (INS)
        data_en <= '1';

        wait for clk_period;
        data_en <= '0';
        wait for 15*clk_period;

        wait for clk_period;
        data <= X"FF"; --CMD0
        data_en <= '1';
        wait for clk_period;
        data <= X"FE"; --CMD1
        data_en <= '1';
        wait for clk_period;
        data <= X"FD"; --CMD2
        data_en <= '1';

        wait for clk_period;
        data_en <= '0';

        wait for clk_period;
        data <= X"91"; --SW1
        data_en <= '1';
        wait for clk_period;
        data <= X"92"; --SW2
        data_en <= '1';
        wait for clk_period;
        data_en <= '0';


    end process;

end behavior;
