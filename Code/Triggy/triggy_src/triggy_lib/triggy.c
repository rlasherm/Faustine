/**
* @Author: ronan
* @Date:   02-12-2016
* @Email:  ronan.lashermes@inria.fr
* @Last modified by:   ronan
* @Last modified time: 03-01-2017
*/


#include <fcntl.h>
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include "uart.h"
#include "triggy.h"

int blocking = 0;

int open_triggy(char * filename)
{
  int handle = open(filename, O_RDWR | O_NOCTTY );
	if(handle < 0)
	{
		printf("Unable to connect to Triggy.\n");
		exit(0);
	}
  else
  {
    set_interface_attribs (handle, B115200, 0);  // set speed to 115,200 bps, 8n1 (no parity)
  	set_blocking (handle, blocking);                // set blocking
    return handle;
  }
}

int close_triggy(int handle)
{
  return close(handle);
}

void print_error_message(char* result, int error_code)
{
  switch (error_code) {
    case RES_OK:
      sprintf(result, "OK");
      break;
    case RES_WR_FAIL:
      sprintf(result, "write failed");
      break;
    case RES_RD_FAIL:
      sprintf(result, "read failed");
      break;
    case RES_CHECK_FAIL:
      sprintf(result, "check failed");
      break;
    case OUT_OF_BOUNDS_FAIL:
      sprintf(result, "out of bounds");
      break;
    default:
      sprintf(result, "unknown error");
      break;
  }
}

void int_to_byte_array(unsigned int val, unsigned char* array)
{
  int i;
  for(i=0; i<4; i++)
  {
    array[i] = (unsigned char) ((val >> (i*8)) & 0xFF);
  }
}

//assume array has size 4
int byte_array_to_int(unsigned char* array)
{
  int i;
  int res= 0;
  for(i=0; i<4; i++)
  {
    res |= (array[i] << (8*i));
  }
  return res;
}

unsigned int ns_to_ticks(unsigned long ns)
{
  return (unsigned int)(ns/NANOSECONDS_PER_TICK);
}

void baud_rate_to_steps(int baudrate, unsigned char* array)
{
  //step = round( baud rate * oversampling=16 * 2^32 / clock frequency )
  long step = (((long)baudrate) << 36L) / 100000000L;
  int_to_byte_array((int)step, array);
}

//send a write command
int write_byte(int handle, unsigned char address, unsigned char value) {
  unsigned char cmd[3] = { WRITE_CMD, address, value};
  unsigned char checksum = WRITE_CMD ^ address ^ value;
  usleep(10);
  int wr_success = write(handle, cmd, 3);
  usleep(10);

  int result_code = RES_OK;//success
  if(wr_success)
  {
    //read checksum
    set_blocking(handle, 1);
    int rd_success = read(handle, cmd, 1);
    set_blocking(handle, blocking);
    //verify checksum
    if(rd_success)
    {
      if(cmd[0] != checksum)
      {
        result_code = RES_CHECK_FAIL; //checksum fail
      }
    }
    else
    {
      result_code = RES_RD_FAIL;//read fail
    }
  }
  else
  {
    result_code = RES_WR_FAIL;//write fail
  }
  return result_code;
}


//send a read command
int read_byte(int handle, unsigned char address, unsigned char* result) {
  unsigned char cmd[2] = { READ_CMD, address};
  unsigned char checksum = address;
  int wr_success = write(handle, cmd, 2);

  int result_code = RES_OK;//success
  if(wr_success)
  {
    //read checksum
    set_blocking(handle, 2);
    int rd_success = read(handle, cmd, 2);
    set_blocking(handle, blocking);
    //verify checksum
    if(rd_success)
    {
      if(cmd[1] != checksum)
      {
        // printf("0x%x 0x%x instead of 0x** 0x%x\n", cmd[0], cmd[1], checksum);
        result_code = RES_CHECK_FAIL; //checksum fail
      }
      else
      {
        // printf("0x%x 0x%x\n", cmd[0], cmd[1]);
        *result = cmd[0];
      }
    }
    else
    {
      result_code = RES_RD_FAIL;//read fail
    }
  }
  else
  {
    result_code = RES_WR_FAIL;//write fail
  }
  return result_code;
}

int test(int handle)
{
  unsigned char cmd[1] = {TEST_CMD};
  int wr_success = write(handle, cmd, 1);

  int result_code = RES_OK;
  if(wr_success)
  {
    //read checksum
    set_blocking(handle, 1);
    int rd_success = read(handle, cmd, 1);
    set_blocking(handle, blocking);
    //verify checksum
    if(rd_success)
    {
      if(cmd[0] != TEST_CMD)
      {
        result_code = RES_CHECK_FAIL; //checksum fail
      }
    }
    else
    {
      result_code = RES_RD_FAIL;//read fail
    }
  }
  else
  {
    result_code = RES_WR_FAIL;
  }
  return result_code;
}

int trig_immediate(int handle, int triggers)
{
  unsigned char to_send = (unsigned char)triggers;
  unsigned char cmd[2] = { IMM_CMD, to_send};
  unsigned char checksum = IMM_CMD ^ to_send;
  int wr_success = write(handle, cmd, 2);

  int result_code = RES_OK;//success
  if(wr_success)
  {
    //read checksum
    set_blocking(handle, 1);
    int rd_success = read(handle, cmd, 1);
    set_blocking(handle, blocking);
    //verify checksum
    if(rd_success)
    {
      if(cmd[0] != checksum)
      {
        // printf("Checkvalue: %d instead of %d ", cmd[0], checksum);
        result_code = RES_CHECK_FAIL; //checksum fail
      }
    }
    else
    {
      result_code = RES_RD_FAIL;//read fail
    }
  }
  else
  {
    result_code = RES_WR_FAIL;//write fail
  }
  return result_code;
}

int set_trig_config(int handle, int trig_index, unsigned long wait_ns, unsigned long width_ns, int polarity, int repeat,  int source)
{
  if(trig_index < 0 || trig_index >= NB_TRIGGERS)
  {
    return OUT_OF_BOUNDS_FAIL;
  }

  int trig_base_address = TRIGGERS_CONFIG_ADD+trig_index*TRIGGERS_CONFIG_SIZE;
  unsigned char to_send[4];
  int wr_success, i;

  //write wait value
  int_to_byte_array(ns_to_ticks(wait_ns), to_send);//convert val to byte array
  for(i = 0;i<4;i++)
  {
    wr_success = write_byte(handle, trig_base_address+i, to_send[i]);
    if(wr_success != RES_OK)
    {
      return wr_success;
    }
  }

  //write width value
  trig_base_address += 4;
  int_to_byte_array(ns_to_ticks(width_ns), to_send);//convert val to byte array
  for(i = 0;i<4;i++)
  {
    wr_success = write_byte(handle, trig_base_address+i, to_send[i]);
    if(wr_success != RES_OK)
    {
      return wr_success;
    }
  }

  //write config
  trig_base_address += 4;
  unsigned char config = (unsigned char)(  ((repeat & 1) << 4) | ((source & 7) << 1) | (polarity & 1));
  // printf("UART config = %d (repeat %d, source %d, polarity %d)\n", config, repeat, source, polarity);
  wr_success = write_byte(handle, trig_base_address, config);
  if(wr_success != RES_OK)
  {
    return wr_success;
  }


  return RES_OK;
}

int set_intercept_uart_config(int handle, int baud_rate, int parity, int start_bits, int stop_bits, int polarity)
{
  int wr_success, i;
  unsigned char step[4];

  //send uart steps (defines baud rate)
  int base_address = UART_CONF_ADD;
  baud_rate_to_steps(baud_rate, step);
  for(i = 0;i<4;i++)
  {
    wr_success = write_byte(handle, base_address+i, step[i]);
    if(wr_success != RES_OK)
    {
      return wr_success;
    }
  }
  base_address += 4;

  //send config byte
  unsigned char config = (unsigned char)(  ((stop_bits & 1) << 4) | ((start_bits & 1) << 3) | ((parity & 3) << 1) | (polarity & 1) );
  wr_success = write_byte(handle, base_address, config);
  if(wr_success != RES_OK)
  {
    return wr_success;
  }

  return RES_OK;
}

int set_intercept_uart_sequence(int handle, unsigned char *sequence, int seq_size)
{
  if(seq_size > 8)
  {
    return OUT_OF_BOUNDS_FAIL;
  }

  int wr_success, i;
  int base_address = UART_SEQ_ADD;

  //send sequence size
  wr_success = write_byte(handle, base_address, (unsigned char)(seq_size & 0xFF));
  if(wr_success != RES_OK)
  {
    return wr_success;
  }


  //send sequence
  base_address += 1;
  if(seq_size > 0)
  {
    for(i=8-seq_size;i<8; i++)
    {
      wr_success = write_byte(handle, base_address+i, sequence[i + seq_size - 8]);
      if(wr_success != RES_OK)
      {
        return wr_success;
      }
    }
  }

  return RES_OK;
}

int set_apdu_follower_config(int handle, unsigned long timeout_ns, unsigned char header_select_en)
{
  int wr_success, i;
  unsigned char to_send[4];
  int base_address = APDU_CONFIG;

  //send header_select_en
  wr_success = write_byte(handle, base_address, (unsigned char)(header_select_en & 0x01));
  // wr_success = write_byte(handle, base_address, (unsigned char)(header_select_en));
  if(wr_success != RES_OK)
  {
    return wr_success;
  }

  //send timeout
  base_address = APDU_TIMEOUT;
  int_to_byte_array(ns_to_ticks(timeout_ns), to_send);//convert val to byte array
  for(i=0; i < 4; i++)
  {
    wr_success = write_byte(handle, base_address+i, to_send[i]);
    if(wr_success != RES_OK)
    {
      return wr_success;
    }
  }

  return RES_OK;
}

int set_apdu_follower_header_select(int handle, unsigned char *header, int header_size)
{
  if(header_size != 4)
  {
    return OUT_OF_BOUNDS_FAIL;
  }

  int wr_success, i;
  int base_address = APDU_HEADER_SELECT;
  for(i=0; i < header_size; i++)
  {
    wr_success = write_byte(handle, base_address+i, header[i]);
    if(wr_success != RES_OK)
    {
      return wr_success;
    }
  }

  return RES_OK;
}

int get_apdu_duration_counter(int handle, unsigned long* result)
{
  int rd_success, i;
  unsigned char read_val = 0;
  unsigned char counter_array[4];
  *result = 0;

  int base_address = APDU_COUNTER_ADD;
  for(i=0; i<4; i++)
  {
    rd_success = read_byte(handle, (unsigned char)(base_address+i), &read_val);
    counter_array[i] = read_val;
    if(rd_success != RES_OK)
    {
      return rd_success;
    }
  }

  unsigned long res = (unsigned long)byte_array_to_int(counter_array);
  *result = res*NANOSECONDS_PER_TICK;

  return RES_OK;
}
