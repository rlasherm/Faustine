/**
* @Author: ronan
* @Date:   02-12-2016
* @Email:  ronan.lashermes@inria.fr
* @Last modified by:   ronan
* @Last modified time: 03-01-2017
*/



#include "uart.h"
#include <string.h>
#include <stdbool.h>
#include <time.h>
#include <stdio.h>
#include <unistd.h>

double toUART = 3.0;//timeout

int set_interface_attribs (int fd, int speed, int parity)
{
        struct termios tty;
        memset (&tty, 0, sizeof tty);
        if (tcgetattr (fd, &tty) != 0)
        {
                //error_message ("error %d from tcgetattr", errno);
                return -1;
        }

        cfsetospeed (&tty, speed);
        cfsetispeed (&tty, speed);

        tty.c_cflag = (tty.c_cflag & ~CSIZE) | CS8;     // 8-bit chars
        // disable IGNBRK for mismatched speed tests; otherwise receive break
        // as \000 chars
        tty.c_iflag &= ~IGNBRK;         // disable break processing
        tty.c_lflag = 0;                // no signaling chars, no echo,
                                        // no canonical processing
        tty.c_oflag = 0;                // no remapping, no delays
        tty.c_cc[VMIN]  = 0;            // read doesn't block
        tty.c_cc[VTIME] = 5;            // 0.5 seconds read timeout

        tty.c_iflag &= ~(IXON | IXOFF | IXANY); // shut off xon/xoff ctrl

        tty.c_cflag |= (CLOCAL | CREAD);// ignore modem controls,
                                        // enable reading
        tty.c_cflag &= ~(PARENB | PARODD);      // shut off parity
        tty.c_cflag |= parity;
        tty.c_cflag &= ~CSTOPB;
        tty.c_cflag &= ~CRTSCTS;

        if (tcsetattr (fd, TCSANOW, &tty) != 0)
        {
                //error_message ("error %d from tcsetattr", errno);
                return -1;
        }

        return 0;
}

void set_blocking (int fd, int min)
{
        struct termios tty;
        memset (&tty, 0, sizeof tty);
        if (tcgetattr (fd, &tty) != 0)
        {
                //error_message ("error %d from tggetattr", errno);
                return;
        }

        tty.c_cc[VMIN]  = min;
        // tty.c_cc[VTIME] = 5;            // 0.5 seconds read timeout
        tty.c_cc[VTIME] = 20;            // 0.5 seconds read timeout

        if (tcsetattr (fd, TCSANOW, &tty) != 0)
        {
          //error_message ("error %d setting term attributes", errno);
        }

}

int readLineUART(int fd, char* buf, int max_size)//read until end of line
{
  int i = 0;
  bool end = false;
  int tot = 0;
  bzero(buf, max_size);
  clock_t start = clock();

  while(!end && i < max_size-1)
  {
    int r = read(fd, buf+i, 1);
    tot += r;
    if(r > 0)
    {

      if(buf[i] == '\n')
      {
        end = true;
      }
      i+=r;
    }
    clock_t now = clock();
    double elapsed = (double)(now-start)*10000/CLOCKS_PER_SEC;
    usleep(100);

    if(elapsed > toUART)//TIMEOUT
      end = true;
  }

  buf[i] = '\0';
  return tot;
}

int readBlock(int fd, unsigned char* buf, int size)
{
  int i = 0;
  while(i < size)
  {
    i += read(fd, buf+i, size-i);
  }

  return i;
}
