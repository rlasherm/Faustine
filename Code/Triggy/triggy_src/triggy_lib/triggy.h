/**
* @Author: ronan
* @Date:   02-12-2016
* @Email:  ronan.lashermes@inria.fr
* @Last modified by:   ronan
* @Last modified time: 03-01-2017
*/

#ifndef TRIGGY_H
#define TRIGGY_H

#define RES_OK 0
#define RES_WR_FAIL 1
#define RES_RD_FAIL 2
#define RES_CHECK_FAIL 3
#define OUT_OF_BOUNDS_FAIL 4

//trigger source
#define IMMEDIATE_SELECT 0
#define UART_SELECT 1
#define APDU_SELECT 2
#define EXT_SELECT 3
#define TRIG0_SELECT 4
#define TRIG1_SELECT 5
#define TRIG2_SELECT 6
#define TRIG3_SELECT 7

//Trigger flag
#define TRIG0_FLAG 1
#define TRIG1_FLAG 2
#define TRIG2_FLAG 4
#define TRIG3_FLAG 8

//polarity
#define IDLE_HIGH 1
#define IDLE_LOW 0

//repeat
#define SINGLE 0
#define REPEAT 1

//uart
#define STOPBITS_1 0
#define STOPBITS_2 1
#define DATABITS_7 1
#define DATABITS_8 0
#define STARTBITS_1 0
#define STARTBITS_2 1
#define PARITY_NONE 0
#define PARITY_EVEN 2
#define PARITY_ODD 1

//APDU
#define HEADER_SELECT_ON 1
#define HEADER_SELECT_OFF 0

//command functions
#define READ_CMD 0
#define WRITE_CMD 1
#define IMM_CMD 2
#define TEST_CMD 0xFF

#define NANOSECONDS_PER_TICK 10 //100MHz clock

//addresses
#define NB_TRIGGERS 4
#define TRIGGERS_CONFIG_ADD 1
#define TRIGGERS_CONFIG_SIZE 9
#define UART_CONF_ADD 50
#define UART_SEQ_ADD 55

#define APDU_TIMEOUT 38
#define APDU_CONFIG 37
#define APDU_HEADER_SELECT 42
#define APDU_COUNTER_ADD 46

//general
int open_triggy(char * filename);//return handle
int close_triggy(int handle);
int reset_config(int handle);
int test(int handle);//test connection. 0=success, other=fail

void print_error_message(char* result, int error_code);
int read_byte(int handle, unsigned char address, unsigned char* result);
int write_byte(int handle, unsigned char address, unsigned char value);

unsigned int ns_to_ticks(unsigned long ns);//convert nanoseconds to ticks

//triggers
int trig_immediate(int handle, int triggers);//launch an immediate trigger
int set_trig_config(int handle, int trig_index, unsigned long wait_ns, unsigned long width_ns, int polarity, int repeat, int source);//configure a trigger (index from 0 to 3) with wait and widt durations

//uart
int set_intercept_uart_config(int handle, int baud_rate, int parity, int start_bits, int stop_bits, int polarity);
int set_intercept_uart_sequence(int handle, unsigned char *sequence, int seq_size); //size <= 8

//apdu
int set_apdu_follower_config(int handle, unsigned long timeout_ns, unsigned char header_select_en);
int set_apdu_follower_header_select(int handle, unsigned char *header, int header_size);
int get_apdu_duration_counter(int handle, unsigned long* result);//counter is 10ns step

#endif
