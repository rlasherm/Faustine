/**
* @Author: ronan
* @Date:   12-12-2016
* @Email:  ronan.lashermes@inria.fr
* @Last modified by:   ronan
* @Last modified time: 09-01-2017
*/



#include "javacard.h"
//Basé sur le code de Ludovic Rousseau
//http://ludovicrousseau.blogspot.fr/2010/04/pcsc-sample-in-c.html

SCARDCONTEXT hContext;
LPTSTR mszReaders;
SCARDHANDLE hCard;
DWORD dwReaders, dwActiveProtocol, dwRecvLength;

SCARD_IO_REQUEST pioSendPci;
int check_verbose = 0; //mettre à 0 pour enlever l'affichage des résulats des fonctions, 1 pour l'activer
int verbose = 0;

//Affiche le résultat d'une fonction
void check(const char* f, LONG rv)
{
  if(!check_verbose)
  {
    return;
  }

  if (SCARD_S_SUCCESS != rv)
  {
    printf("%s: %s\n",f, pcsc_stringify_error(rv));
  }
  else
  {
    printf("%s: Ok\n", f);
  }
}

//Initialise la connection avec la javacard (si ok, ne pas oublier d'appeler close à la fin)
void javacard_init()
{
  LONG rv = SCardEstablishContext(SCARD_SCOPE_SYSTEM, NULL, NULL, &hContext);
  check("SCardEstablishContext", rv);

  dwReaders = SCARD_AUTOALLOCATE;
  rv = SCardListReaders(hContext, NULL, (LPTSTR)&mszReaders, &dwReaders);
  check("SCardListReaders", rv);

  if(verbose)
    printf("Reader name: %s\n", mszReaders);

  rv = SCardConnect(hContext, mszReaders, SCARD_SHARE_SHARED, SCARD_PROTOCOL_T0 | SCARD_PROTOCOL_T1, &hCard, &dwActiveProtocol);
  check("SCardConnect", rv);

  switch(dwActiveProtocol)
  {
    case SCARD_PROTOCOL_T0:
      printf("T0\n");
      pioSendPci = *SCARD_PCI_T0;
      break;

    case SCARD_PROTOCOL_T1:
      printf("T1\n");
      pioSendPci = *SCARD_PCI_T1;
      break;
  }
}

//envoie la commande [cmd] de taille [cmd_size] et ecrit la réponse dans [rcv] de taille max [rcv_size] (argument).
//La valeur de [rcv_size] est mis à jour avec la taille réelle de la réponse.
void javacard_cmd(BYTE* cmd, int cmd_size, BYTE* rcv, DWORD *rcv_size)
{
  LONG rv = SCardTransmit(hCard, &pioSendPci, cmd, cmd_size, NULL, rcv, rcv_size);
  check("SCardTransmit", rv);
}

//Affiche les valeurs d'un buffer
void javacard_printbuf(BYTE* buf, int  buf_size)
{
  int i;
  for(i=0; i<buf_size; i++)
    printf("%02X ", buf[i]);
}

//avec saut de ligne
void javacard_printbufn(BYTE* buf, int  buf_size)
{
  int i;
  for(i=0; i<buf_size; i++)
    printf("%02X ", buf[i]);
  printf("\n");
}

//Ferme la connection avec la javacard
void javacard_close()
{
  LONG rv = SCardDisconnect(hCard, SCARD_LEAVE_CARD);
  check("SCardDisconnect", rv);

  rv = SCardFreeMemory(hContext, mszReaders);
  check("SCardFreeMemory", rv);

  rv = SCardReleaseContext(hContext);
  check("SCardReleaseContext", rv);
}

void javacard_selectPINApp()
{
  BYTE rcv[256];
	DWORD rcv_size = 256;

	BYTE cmd[] = {0x00, 0xA4, 0x04, 0x00, 0x0B, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08, 0x09, 0X00, 0x00};
	int cmd_size = sizeof(cmd);

  javacard_cmd(cmd, cmd_size, rcv, &rcv_size);
  if(verbose)
  {
    printf("Select PIN app: ");
    javacard_printbufn(rcv, rcv_size);
  }
}

void javacard_setKey(BYTE b)
{
  BYTE rcv[256];
	DWORD rcv_size = 256;

	BYTE cmd[] = {0x80, 0x12, 0x00, 0x00, 0x01, b};
	int cmd_size = sizeof(cmd);

  javacard_cmd(cmd, cmd_size, rcv, &rcv_size);
  if(verbose)
  {
    printf("Set key %02X: ",b);
    javacard_printbufn(rcv, rcv_size);
  }
}

int javacard_testCandidate(BYTE b)
{
  BYTE rcv[256];
	DWORD rcv_size = 256;

	BYTE cmd[] = {0x80, 0x13, 0x00, 0x00, 0x01, b};
	int cmd_size = sizeof(cmd);

  javacard_cmd(cmd, cmd_size, rcv, &rcv_size);
  if(verbose)
  {
    printf("Test candidate %02X: ",b);
    javacard_printbuf(rcv, rcv_size);
  }

  int success = 0;
  if(rcv_size == 2)
  {
    if(rcv[0] == 0x90 && rcv[1] == 0x00)
    {
      success = 1;
    }
    else {
      printf(" -> FAIL %x %x\n", rcv[0], rcv[1]);
    }
  }
  else {
    unsigned int i = 0;
    printf(" -> FAIL (%ld!=2) ", rcv_size);
    for(i=0;i<rcv_size;i++)
    {
      printf("%x ", rcv[i]);
    }
    printf("\n");
  }

  if(verbose)
  {
    if(success)
    {
      printf(" -> SUCCESS\n");
    }
    else
    {
      printf(" -> FAIL\n");
    }
  }

  return success;
}

void javacard_resetTryCounter()
{
  BYTE rcv[256];
	DWORD rcv_size = 256;

	BYTE cmd[] = {0x80, 0x14, 0x00, 0x00};
	int cmd_size = sizeof(cmd);

  javacard_cmd(cmd, cmd_size, rcv, &rcv_size);
  if(verbose)
  {
    printf("Reset try counter: ");
    javacard_printbufn(rcv, rcv_size);
  }
}
