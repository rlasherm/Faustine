/**
* @Author: ronan
* @Date:   07-12-2016
* @Email:  ronan.lashermes@inria.fr
* @Last modified by:   ronan
* @Last modified time: 09-01-2017
*/

#include <winscard.h>
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include "triggy.h"
#include "javacard.h"

int main(void)
{
  char temp[256];
  printf("Starting test...\n");

  javacard_init();

  // int triggy = open_triggy("/dev/ttyUSB0");

  // int test_result = test(triggy);
  // print_error_message(temp, test_result);
  // printf("Triggy test result: %s\n", temp);
  //
  // int write_result = write_byte(triggy, 0, 0x55);
  // print_error_message(temp, write_result);
  // printf("Write@0 result: %s\n", temp);
  //
  // unsigned char byte_read = 0;
  // int read_result = read_byte(triggy, 0, &byte_read);
  // print_error_message(temp, read_result);
  // printf("Read@0 result: %s, value is %d\n", temp, byte_read);
  //
  // // int config_trig0_result = set_trig_config(triggy, 0, 1000, 1000,  IDLE_LOW, SINGLE, IMMEDIATE_SELECT);
  // // int config_trig0_result = set_trig_config(triggy, 0, 1000, 100000,  IDLE_LOW, SINGLE, UART_SELECT);
  // int config_trig0_result = set_trig_config(triggy, 0, 1000, 100000,  IDLE_LOW, SINGLE, APDU_SELECT);
  // print_error_message(temp, config_trig0_result);
  // printf("Trig0 configured: %s\n", temp);
  //
  // int trig_result = trig_immediate(triggy, TRIG0_FLAG);
  // print_error_message(temp, trig_result);
  // printf("Trig immediate result: %s\n", temp);
  //
  // unsigned long counter = 0;
  // int apdu_counter_result = get_apdu_duration_counter(triggy, &counter);
  // print_error_message(temp, apdu_counter_result);
  // printf("APDU counter result: %s, value is %lu\n", temp, counter);
  //
  // //prepare uart trig on '' value
  // // int uart_config_result = set_intercept_uart_config(triggy, 115200, PARITY_NONE, STARTBITS_1, STOPBITS_1, IDLE_HIGH);
  // int uart_config_result = set_intercept_uart_config(triggy, 10700, PARITY_EVEN, STARTBITS_1, STOPBITS_1, IDLE_HIGH);
  // print_error_message(temp, uart_config_result);
  // printf("Intercept UART result: %s\n", temp);
  //
  // // unsigned char sequence[7] = {'t', 'e', 's', 't', ' ', 'O', 'K'};
  // // unsigned char sequence[2] = {'O', 'K'};
  // unsigned char sequence[4] = {0x80, 0x13, 0x00, 0x00};
  //
  // int apdu_config_result = set_apdu_follower_header_select(triggy, sequence, 4);
  // print_error_message(temp, apdu_config_result);
  // printf("APDU header select result: %s\n", temp);
  //
  // apdu_config_result = set_apdu_follower_config(triggy, 500000000, HEADER_SELECT_ON);
  // print_error_message(temp, apdu_config_result);
  // printf("APDU config result: %s\n", temp);

  // uart_config_result = set_intercept_uart_sequence(triggy, sequence, 4);
  // print_error_message(temp, uart_config_result);
  // printf("Intercept UART sequence result: %s\n", temp);

	javacard_selectPINApp();

  usleep(1000000);

	javacard_setKey(0x00);
	javacard_resetTryCounter();
  javacard_testCandidate(0x00);

  int loop = 1000000;
  int cnt = 0;
  printf("Starting...\n");

  while((loop--) > 0)
  {
    usleep(20000);
    // javacard_resetTryCounter();
    int test = javacard_testCandidate(0x00);
    cnt++;
    //printf(".");
    //if(cnt % 100 == 0)
    	// printf("%d",cnt);
     printf("\r%d",cnt);

    // int apdu_counter_result = get_apdu_duration_counter(triggy, &counter);
    // print_error_message(temp, apdu_counter_result);
    // printf("\r(%d) APDU counter result: %s, value is %lu", cnt, temp, counter);
    fflush(stdout);
    // usleep(5000);

    if(test != 1) {
      printf("\nFAIL!\n");
      break;
    }

    // if (apdu_counter_result != RES_OK)
    // {
    //   break;
    // }
  }

  printf("\n");
  javacard_close();

  // close_triggy(triggy);

  printf("The end.\n");
  return 0;
}
