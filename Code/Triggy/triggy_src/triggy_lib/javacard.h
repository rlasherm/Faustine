#ifndef _JAVACARD_H
#define _JAVACARD_H

#include <winscard.h>
#include <stdlib.h>
#include <stdio.h>


void check(const char* f, LONG rv);

void javacard_init();

void javacard_close();

void javacard_cmd(BYTE* cmd, int cmd_size, BYTE* rcv, DWORD *rcv_size);

void javacard_printbuf(BYTE* buf, int  buf_size);
void javacard_printbufn(BYTE* buf, int  buf_size);

void javacard_selectPINApp();

void javacard_setKey(BYTE b);

int javacard_testCandidate(BYTE b);

void javacard_resetTryCounter();

#endif
