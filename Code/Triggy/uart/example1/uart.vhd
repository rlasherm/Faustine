--/*******************************************************************
-- *
-- *    DESCRIPTION: UART top level module implements full duplex UART function.
-- *    AUTHOR: Jim Jian
-- *    HISTORY: 1/15/97
-- *    QuickLogic's Application Notes and QuickNotes
-- *    (Digital UART Design Using Hardware Description Language)
-- *    http://www.quicklogic.com/support/anqn/
-- *
-- *******************************************************************/
LIBRARY ieee;
USE ieee.std_logic_1164.ALL;

ENTITY uart IS
PORT (mclkx16   : IN std_logic;
      read      : IN std_logic;
      write     : IN std_logic;
      reset     : IN std_logic;
      data      : INOUT std_logic_vector(7 downto 0);
      -- receiver input signal, error, and status flags
      rx        : IN std_logic;
      rxrdy     : OUT std_logic;
      parityerr : OUT std_logic;
      framingerr: OUT std_logic;
      overrun   : OUT std_logic;
      -- transmitter output signal and status flag
      tx        : OUT std_logic;
      txrdy     : OUT std_logic);
END uart;


ARCHITECTURE top OF uart IS
   COMPONENT txmit IS
   PORT (mclkx16, write, reset : IN std_logic;
         data : IN std_logic_vector(7 downto 0);
         tx, txrdy : OUT std_logic);
   END COMPONENT;

   COMPONENT rxcver IS
   PORT (mclkx16, read, rx, reset : IN std_logic;
         rxrdy, parityerr, framingerr, overrun : OUT std_logic;
         data : OUT std_logic_vector(7 downto 0));
   END COMPONENT;

   SIGNAL rxdata : std_logic_vector(7 downto 0);
BEGIN

   transmitter: txmit PORT MAP (
       mclkx16 => mclkx16,
       write => write,
       reset => reset,
       data => data,
       tx => tx,
       txrdy => txrdy);

    receiver: rxcver PORT MAP (
       mclkx16 => mclkx16,
       read => read,
       rx => rx,
       reset => reset,
       rxrdy => rxrdy,
       parityerr => parityerr,
       framingerr => framingerr,
       overrun => overrun,
       data => rxdata);

   data <= rxdata WHEN (read = '0') ELSE "ZZZZZZZZ";
END top;
