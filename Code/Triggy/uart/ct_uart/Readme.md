<!--
@Author: ronan
@Date:   07-11-2016
@Email:  ronan.lashermes@inria.fr
@Last modified by:   ronan
@Last modified time: 07-11-2016
-->



# CT UART

This UART component intends to be highly customizable, to be used both as a standard UART and as a smartcard link.

## Baud generator

From a global clock signal, generate a square signal (called baud clock) with frequency = oversampling*baud rate. Oversampling is taken =16 here.
We must have 16 rising edges for each baud period.

Compute step as:
```
step = round( baud rate * oversampling * 2^32 / clock frequency )
```

## Uart tx

In charge of transmitting data to distant entity.

## Uart rx

In charge of receiving data from distant entity.
