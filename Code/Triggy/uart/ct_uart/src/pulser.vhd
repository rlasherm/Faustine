library IEEE;
use IEEE.std_logic_1164.all;

-- generate a pulse of one clk_out period on a rising edge on pulse_in
entity pulser is
  port(clk_in:    in std_logic;
       clk_out:   in std_logic;
       reset:     in std_logic;
       pulse_in:  in std_logic;
       pulse_out: out std_logic
       );
end pulser;

architecture behavior of pulser is
  signal armed: std_logic :='0';
  signal executed: std_logic;
  signal pulse_o: std_logic;

  begin

    pulse_out <= pulse_o;

    -- armed <= '1' when pulse_in = '1' else
    --          '0' when executed = '1' and pulse_in = '0' and pulse_o = '0' else
    --          armed;

    process(clk_in, reset)
    begin
      if reset = '1' then
        armed <= '0';
      elsif rising_edge(clk_in) then
        if pulse_in = '1' and executed = '0' then
          armed <= '1';
        elsif pulse_in = '0' and executed = '1' then
          armed <= '0';
        end if;
      end if;
    end process;


    process(clk_out, reset)
    begin
      if reset = '1' then
        executed <= '0';
      elsif rising_edge(clk_out) then
        if pulse_o = '1' and armed = '1' then -- pulse_o last only 1 clk_out period
          executed <= '1';
        elsif armed = '0' then
          executed <= '0';
        end if;
      end if;
    end process;

    process(clk_out, reset)
    begin
      if reset = '1' then
        pulse_o <= '0';
      elsif rising_edge(clk_out) then
        if armed = '1' and executed = '0' and pulse_o = '0' then
          pulse_o <= '1';
        else
          pulse_o <= '0';
        end if;
      end if;
    end process;


end behavior;
