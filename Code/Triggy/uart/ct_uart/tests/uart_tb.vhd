library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use work.common.all;

entity uart_tb is
end uart_tb;

architecture behavior of uart_tb is
  component uart is
    port(clk:  in std_logic;
         reset:     in std_logic;
         uart_conf: in UartConf;

         rx:        in std_logic;--rx wire
         tx:        out std_logic;--tx wire

         tx_data:   in std_logic_vector(7 downto 0);--data byte to write
         rx_data:   out std_logic_vector(7 downto 0);--data byte read

         rx_rdy:    out std_logic;--a byte is ready to be read
         tx_rdy:    out std_logic;--a byte can be written
         tx_start:  in std_logic--write a byte
         );
  end component;

  signal clk : std_logic := '0';
  signal reset : std_logic := '0';

  signal uart_conf: UartConf := (idle_polarity => '1',
                                  baud_gen_step => X"2F2F9874",
                                  start_bits => One,
                                  stop_bits => One,
                                  parity => Even,
                                  data_bits => Eight);

  signal tx: std_logic;
  signal rx: std_logic;

  signal tx_data: std_logic_vector(7 downto 0);
  signal rx_data: std_logic_vector(7 downto 0);

  signal tx_rdy: std_logic;
  signal tx_start: std_logic;

  signal rx_rdy: std_logic;


   -- Clock period definitions
   constant clk_period : time := 100 ns;
   constant bit_period : time := 8681 ns; -- 115200 bauds
  begin
    --dut instance
    dut: uart port map(
      clk           => clk,
      reset         => reset,
      uart_conf     => uart_conf,

      rx            => rx,
      tx            => tx,

      tx_data       => tx_data,
      rx_data       => rx_data,

      rx_rdy        => rx_rdy,
      tx_rdy        => tx_rdy,
      tx_start      => tx_start
    );
    -- Clock process definitions: clock with 50% duty cycle is generated here.
   clk_process :process
   begin
        clk <= '0';
        wait for clk_period/2;
        clk <= '1';
        wait for clk_period/2;
   end process;

   -- Stimuli
   stim: process
    begin
      reset <= '0';
      rx <= uart_conf.idle_polarity;
      wait for clk_period;
      reset <= '1';
      wait for clk_period;
      reset <= '0';
      wait for 3*clk_period;
      tx_data <= X"55";
      tx_start <= '1';
      wait for bit_period;
      tx_start <= '0';
      wait for 12*bit_period;

      tx_data <= X"dd";
      uart_conf.parity <= Even;
      tx_start <= '1';
      wait for bit_period;
      tx_start <= '0';
      wait for 13*bit_period;

      rx <=  not uart_conf.idle_polarity;-- start
      wait for bit_period; -- 0x69
      rx <= '1';-- bit 0
      wait for bit_period;
      rx <= '0';-- bit 1
      wait for bit_period;
      rx <= '0';-- bit 2
      wait for bit_period;
      rx <= '1';-- bit 3
      wait for bit_period;
      rx <= '0';-- bit 4
      wait for bit_period;
      rx <= '1';-- bit 5
      wait for bit_period;
      rx <= '1';-- bit 6
      wait for bit_period;
      rx <= '0';-- bit 7
      wait for bit_period;
      rx <= '0';-- parity
      wait for bit_period;
      rx <= uart_conf.idle_polarity;-- stop
      assert rx_data = X"69" severity error;
      wait for 2*bit_period;


   end process;
end behavior;
