library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use work.common.all;

entity baud_gen_tb is
end baud_gen_tb;

architecture behavior of baud_gen_tb is
  component baud_gen is
    port(main_clk:  in std_logic;
         reset:     in std_logic;
         baud_clk:  out std_logic;--generated signal
         --tot for divider is 2^32
         uart_conf:  in UartConf);--step for divider
  end component;

  signal clk : std_logic := '0';
  signal baud_clk: std_logic;
  signal reset : std_logic := '0';

  signal uart_conf: UartConf := (idle_polarity => '0',
                                  baud_gen_step => X"2F2F9874",
                                  start_bits => One,
                                  stop_bits => One,
                                  parity => None);

  -- Clock period definitions
   constant clk_period : time := 100 ns;
  begin
    --dut instance
    dut: baud_gen port map (
      main_clk    => clk,
      reset       => reset,
      baud_clk    => baud_clk,
      uart_conf    => uart_conf
    );

    -- Clock process definitions: clock with 50% duty cycle is generated here.
   clk_process :process
   begin
        clk <= '0';
        wait for clk_period/2;
        clk <= '1';
        wait for clk_period/2; 
   end process;

   -- Stimuli
   stim: process
    begin
      wait for clk_period;
      reset <= '1';
      wait for clk_period;
      reset <= '0';
      wait for 100*clk_period;
   end process;
end behavior;
