library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use work.common.all;

entity uart_tx_tb is
end uart_tx_tb;

architecture behavior of uart_tx_tb is
  component baud_gen is
    port(main_clk:  in std_logic;
         reset:     in std_logic;
         baud_clk:  out std_logic;--generated signal
         --tot for divider is 2^32
         uart_conf:  in UartConf);--step for divider
  end component;

  component uart_tx is
    port(baud_clk:  in std_logic;
         reset:     in std_logic;
         uart_conf: in UartConf;

         tx:        out std_logic;--rx wire
         tx_rdy:    out std_logic;-- 1 when we can send a new byte

         tx_data:   in std_logic_vector(7 downto 0);--data byte read
         tx_start:  in std_logic
         );
  end component;

  signal clk : std_logic := '0';
  signal baud_clk: std_logic;
  signal reset : std_logic := '0';

  signal uart_conf: UartConf := (idle_polarity => '1',
                                  baud_gen_step => X"2F2F9874",
                                  start_bits => One,
                                  stop_bits => One,
                                  parity => None,
                                  data_bits => Eight);

  signal tx: std_logic;
  signal tx_data: std_logic_vector(7 downto 0);
  signal tx_rdy: std_logic;
  signal tx_start: std_logic;

   -- Clock period definitions
   constant clk_period : time := 100 ns;
   constant bit_period : time := 8681 ns; -- 115200 bauds
   constant baud_period: time := 543 ns;
  begin
    --dut instance
    gen: baud_gen port map (
      main_clk    => clk,
      reset       => reset,
      baud_clk    => baud_clk,
      uart_conf   => uart_conf
    );

    dut: uart_tx port map(
      baud_clk      => baud_clk,
      reset         => reset,
      uart_conf     => uart_conf,
      tx            => tx,
      tx_data       => tx_data,
      tx_rdy        => tx_rdy,
      tx_start      => tx_start
    );

    -- Clock process definitions: clock with 50% duty cycle is generated here.
   clk_process :process
   begin
        clk <= '0';
        wait for clk_period/2;
        clk <= '1';
        wait for clk_period/2;
   end process;

   -- Stimuli
   stim: process
    begin
      reset <= '0';
      tx_start <= '0';
      tx_data <= X"00";
      wait for clk_period;
      reset <= '1';
      wait for clk_period;
      reset <= '0';
      wait for 3*clk_period;
      tx_data <= X"55";
      tx_start <= '1';
      wait for baud_period;
      tx_start <= '0';
      wait for 12*bit_period;

      tx_data <= X"dd";
      uart_conf.parity <= Even;
      tx_start <= '1';
      wait for baud_period;
      tx_start <= '0';
      wait for 13*bit_period;

   end process;
end behavior;
