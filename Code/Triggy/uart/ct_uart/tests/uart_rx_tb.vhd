library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use work.common.all;

entity uart_rx_tb is
end uart_rx_tb;

architecture behavior of uart_rx_tb is
  component baud_gen is
    port(main_clk:  in std_logic;
         reset:     in std_logic;
         baud_clk:  out std_logic;--generated signal
         --tot for divider is 2^32
         uart_conf:  in UartConf);--step for divider
  end component;

  component uart_rx is
    port(baud_clk:  in std_logic;
         reset:     in std_logic;
         uart_conf: in UartConf;
         rx:        in std_logic;--rx wire
         rx_data:   out std_logic_vector(7 downto 0);--data byte read
         rx_rdy:    out std_logic;
         error_flag:     out std_logic
         );
  end component;

  signal clk : std_logic := '0';
  signal baud_clk: std_logic;
  signal reset : std_logic := '0';

  signal uart_conf: UartConf := (idle_polarity => '1',
                                  baud_gen_step => X"2F2F9874",
                                  start_bits => One,
                                  stop_bits => One,
                                  parity => None,
                                  data_bits => Eight);

  signal rx: std_logic;
  signal rx_data: std_logic_vector(7 downto 0);
  signal rx_rdy: std_logic;
  signal error_flag: std_logic;

   -- Clock period definitions
   constant clk_period : time := 100 ns;
   constant bit_period : time := 8681 ns; -- 115200 bauds
  begin
    --dut instance
    gen: baud_gen port map (
      main_clk    => clk,
      reset       => reset,
      baud_clk    => baud_clk,
      uart_conf   => uart_conf
    );

    dut: uart_rx port map(
      baud_clk      => baud_clk,
      reset         => reset,
      uart_conf     => uart_conf,
      rx            => rx,
      rx_data       => rx_data,
      rx_rdy        => rx_rdy,
      error_flag         => error_flag
    );

    -- Clock process definitions: clock with 50% duty cycle is generated here.
   clk_process :process
   begin
        clk <= '0';
        wait for clk_period/2;
        clk <= '1';
        wait for clk_period/2;
   end process;

   -- Stimuli
   stim: process
    begin
      reset <= '0';
      rx <= uart_conf.idle_polarity;
      wait for clk_period;
      reset <= '1';
      wait for clk_period;
      reset <= '0';
      wait for 3*bit_period;
      uart_conf.parity <= None;
      rx <=  not uart_conf.idle_polarity;-- start
      wait for bit_period;-- 0xAB
      rx <= '1';-- bit 0
      wait for bit_period;
      rx <= '1';-- bit 1
      wait for bit_period;
      rx <= '0';-- bit 2
      wait for bit_period;
      rx <= '1';-- bit 3
      wait for bit_period;
      rx <= '0';-- bit 4
      wait for bit_period;
      rx <= '1';-- bit 5
      wait for bit_period;
      rx <= '0';-- bit 6
      wait for bit_period;
      rx <= '1';-- bit 7
      wait for bit_period;
      rx <= uart_conf.idle_polarity;-- stop
      assert rx_data = X"AB" severity error;
      assert error_flag = '0' severity error;
      wait for bit_period;

      uart_conf.parity <= Even;
      rx <=  not uart_conf.idle_polarity;-- start
      wait for bit_period; -- 0x69
      rx <= '1';-- bit 0
      wait for bit_period;
      rx <= '0';-- bit 1
      wait for bit_period;
      rx <= '0';-- bit 2
      wait for bit_period;
      rx <= '1';-- bit 3
      wait for bit_period;
      rx <= '0';-- bit 4
      wait for bit_period;
      rx <= '1';-- bit 5
      wait for bit_period;
      rx <= '1';-- bit 6
      wait for bit_period;
      rx <= '0';-- bit 7
      wait for bit_period;
      rx <= '0';-- parity
      wait for bit_period;
      rx <= uart_conf.idle_polarity;-- stop
      assert rx_data = X"69" severity error;
      assert error_flag = '0' severity error;
      wait for 2*bit_period;

      rx <=  not uart_conf.idle_polarity;-- start
      wait for bit_period; -- 0x69 with parity error_flag
      rx <= '1';-- bit 0
      wait for bit_period;
      rx <= '0';-- bit 1
      wait for bit_period;
      rx <= '0';-- bit 2
      wait for bit_period;
      rx <= '1';-- bit 3
      wait for bit_period;
      rx <= '0';-- bit 4
      wait for bit_period;
      rx <= '1';-- bit 5
      wait for bit_period;
      rx <= '1';-- bit 6
      wait for bit_period;
      rx <= '0';-- bit 7
      wait for bit_period;
      rx <= '1';-- parity error_flag
      wait for bit_period;
      rx <= uart_conf.idle_polarity;-- stop
      assert rx_data = X"69" severity error;
      assert error_flag = '1' severity error;
      wait for 2*bit_period;

      -- start 2, stop 2, parity odd
      uart_conf.parity <= Odd;
      uart_conf.start_bits <= Two;
      uart_conf.stop_bits <= Two;
      rx <=  not uart_conf.idle_polarity;-- start
      wait for 2*bit_period; -- 0xDD
      rx <= '1';-- bit 0
      wait for bit_period;
      rx <= '0';-- bit 1
      wait for bit_period;
      rx <= '1';-- bit 2
      wait for bit_period;
      rx <= '1';-- bit 3
      wait for bit_period;
      rx <= '1';-- bit 4
      wait for bit_period;
      rx <= '0';-- bit 5
      wait for bit_period;
      rx <= '1';-- bit 6
      wait for bit_period;
      rx <= '1';-- bit 7
      wait for bit_period;
      rx <= '1';-- parity
      wait for bit_period;
      rx <= uart_conf.idle_polarity;-- stop
      assert rx_data = X"DD" severity error;
      assert error_flag = '0' severity error;
      wait for 4*bit_period;


   end process;
end behavior;
