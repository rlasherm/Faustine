library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use work.triggy_common.all;
use work.common.all;

entity configuration_manager is
  port(clk:       in std_logic;
       reset:     in std_logic;
       
       memout: out std_logic_vector(MAX_ADDRESS*8-1 downto 0);

       --fifo from Uart PC rx (cross clock domains)
       din:         in byte;
       din_rdy:     in std_logic;
       din_read:    out std_logic;-- send read order

       --fifo to Uart PC tx (cross clock domains)
       dout:        out byte;
       dout_write:  out std_logic;-- send write order

      --from apdu follower
      counter_updated:   in std_logic;
      counter_value:     in u32;
      apdu_conf:         out APDUConf;


      -- triggers
      trig_confs:       out TrigConfs;

      -- uart intercept
      uart_conf:        out UartConf;
      uart_trig_conf:   out UartTrigConf

      -- global

      --debug_o:                    out byte
       );
end configuration_manager;

architecture behavior of configuration_manager is

  component fifo_autoreader is
    port(clk:             in std_logic;
         reset:           in std_logic;
         enable:          in std_logic;
         fifo_empty:      in std_logic;
         fifo_data:       in std_logic_vector(7 downto 0);
         fifo_read_order: out std_logic;

         data:            out std_logic_vector(7 downto 0);
         data_en:         out std_logic
         );
  end component;

  -- protocel is [address(15 downto 0), payload_len(7 downto 0), data(8*payload_len-1 downto 0)]
  type COMMAND_FSM_STATE is (FUN, ADD, READ_DATA, WRITE_DATA, WAIT1, TRIG_IMM, SEND_CHECKSUM); -- FUN is skipped

  signal data_byte: byte;
  signal data_en:   std_logic;

  signal dout_write_order: std_logic;

  signal fifo_empty: std_logic;

  signal command_state: COMMAND_FSM_STATE;
  signal mem: std_logic_vector(MAX_ADDRESS*8-1 downto 0);

  -- signal address: unsigned(7 downto 0);
  signal address: integer range 0 to 255;
  signal crw: std_logic; -- 0 read 1 write

  signal autoread_enable: std_logic;

  signal action_reg, dout_buf, checksum: byte;
  signal action: std_logic;

  signal uart_trig_conf_seq : UartSequence;
  signal apdu_header: APDUHeader;

  begin

    fifo_empty <= not(din_rdy);
    dout <= dout_buf;
    dout_write <= dout_write_order;
    autoread_enable <= '1';
    memout<=mem;

    -- Memory mapping

    --triggers confs
    gen_trig_confs: for i in 0 to NB_TRIGGERS-1 generate
      trig_confs(i) <= (trig_wait     => unsigned(mem((TRIGGERS_ADD+i*TRIGGER_CONF_SIZE)*8 + 31 downto (TRIGGERS_ADD+i*TRIGGER_CONF_SIZE)*8)),
                        trig_width    => unsigned(mem((TRIGGERS_ADD+i*TRIGGER_CONF_SIZE)*8 + 63 downto (TRIGGERS_ADD+i*TRIGGER_CONF_SIZE)*8 + 32)),
                        idle_polarity => mem((TRIGGERS_ADD+i*TRIGGER_CONF_SIZE)*8 + 64),
                        trig_source   => mem((TRIGGERS_ADD+i*TRIGGER_CONF_SIZE)*8 + 67 downto (TRIGGERS_ADD+i*TRIGGER_CONF_SIZE)*8 + 65),
                        repeat        => mem((TRIGGERS_ADD+i*TRIGGER_CONF_SIZE)*8 + 68));
    end generate;

    -- global
    -- RFU

    -- uart intercept
    uart_conf <= (idle_polarity       => mem(UART_INTERCEPT_CONFIG_ADD*8),
                  baud_gen_step       => unsigned(mem((UART_INTERCEPT_STEP_ADD+UART_INTERCEPT_STEP_SIZE)*8-1 downto UART_INTERCEPT_STEP_ADD*8)),
                  start_bits          => parse_startbits(mem(UART_INTERCEPT_CONFIG_ADD*8 + 3)),
                  stop_bits           => parse_stopbits(mem(UART_INTERCEPT_CONFIG_ADD*8 + 4)),
                  parity              => parse_parity(mem(UART_INTERCEPT_CONFIG_ADD*8 + 2 downto UART_INTERCEPT_CONFIG_ADD*8 + 1)),
                  data_bits           => parse_databits(mem(UART_INTERCEPT_CONFIG_ADD*8 + 5)));

    uart_conf_seq: for i in 0 to SEQUENCE_SIZE-1 generate
      uart_trig_conf_seq(i) <= mem((UART_INTERCEPT_SEQ_ADD+i)*8 + 7 downto (UART_INTERCEPT_SEQ_ADD+i)*8);
    end generate;

    uart_trig_conf <= ( sequence      => uart_trig_conf_seq,
                        sequence_size => unsigned(mem(UART_INTERCEPT_SEQ_SIZE_ADD*8 + 7 downto UART_INTERCEPT_SEQ_SIZE_ADD*8)));

    -- apdu follower conf
    apdu_head: for i in 0 to APDU_FOLLOWER_HEADER_SELECT_SIZE-1 generate
      apdu_header(i) <= mem((APDU_FOLLOWER_HEADER_SELECT_ADD+i)*8 + 7 downto (APDU_FOLLOWER_HEADER_SELECT_ADD+i)*8);
    end generate;

    apdu_conf <= (wait_counter    => unsigned(mem((APDU_FOLLOWER_WAIT_COUNTER_ADD+APDU_FOLLOWER_WAIT_COUNTER_SIZE)*8-1 downto APDU_FOLLOWER_WAIT_COUNTER_ADD*8)),
                  header_select         => apdu_header,
                  header_select_en      => mem(APDU_FOLLOWER_CONFIG_ADD*8));

    -- mem operations
    read_fifo: fifo_autoreader
      port map(
        clk               => clk,
        reset             => reset,
        enable            => autoread_enable,
        fifo_empty        => fifo_empty,
        fifo_data         => din,
        fifo_read_order   => din_read,

        data              => data_byte,
        data_en           => data_en
      );


    update_mem: process(clk, reset)
    begin
      if reset = '1' then
        mem <= (others => '0');
      elsif rising_edge(clk) then
        if command_state = READ_DATA and data_en = '1' then
          mem(address*8+7 downto address*8) <= data_byte;
        end if;
        if counter_updated = '1' then
          mem((COUNTER_ADD+COUNTER_SIZE)*8-1 downto COUNTER_ADD*8) <= std_logic_vector(counter_value);
        end if;
      end if;
    end process;

    update_checksum: process(clk, reset)
    begin
      if reset = '1' then
        checksum <= X"00";
      elsif rising_edge(clk) then
        if command_state = SEND_CHECKSUM then
          checksum <= X"00";
        elsif data_en = '1' then
          checksum <= checksum xor data_byte;
        end if;
      end if;
    end process;

    write_back: process(clk, reset)--write data to host
    begin
      if reset = '1' then
        dout_buf <= X"00";
        dout_write_order <= '0';
      elsif rising_edge(clk) then
        if command_state = SEND_CHECKSUM then
          dout_buf <= checksum;
          dout_write_order <= '1';
        elsif command_state = WRITE_DATA then
          dout_buf <= mem(address*8+7 downto address*8);
          dout_write_order <= '1';
        else
          dout_write_order <= '0';
        end if;
      end if;
    end process;

    add_set: process(clk, reset)--write data to host
    begin
      if reset = '1' then
        address <= 0;
      elsif rising_edge(clk) then
        if command_state = ADD and data_en = '1' then
          address <= to_integer(unsigned(data_byte));
        end if;
      end if;
    end process;



--    trig_imm_proc: process(clk, reset)
--    begin
--      if reset = '1' then
--        trig_immediate <= (others => '0');
--      elsif rising_edge(clk) then
--        if command_state = TRIG_IMM and data_en = '1' then
--          trig_immediate <= data_byte(NB_TRIGGERS-1 downto 0);
--        else
--          trig_immediate <= (others => '0');
--        end if;
--      end if;
--    end process;

    --debug_o <= X"00";

    fsm_command: process(clk, reset)
      begin
        if reset = '1' then
          command_state <= FUN;
          crw <= '0';
        elsif rising_edge(clk) then
          case command_state is
            when FUN =>
              if data_en = '1' then -- read function
                if data_byte = X"00" then
                  crw <= '0';
                  command_state <= ADD;
                elsif data_byte = X"01" then
                  crw <= '1';
                  command_state <= ADD;
                elsif data_byte = X"02" then
                  crw <= '1';
                  command_state <= TRIG_IMM;
                else -- test
                  crw <= '0';
                  command_state <= SEND_CHECKSUM;
                end if;
              else
                command_state <= FUN;
              end if;

            when TRIG_IMM =>
              if data_en = '1' then
                command_state <= SEND_CHECKSUM;
              else
                command_state <= TRIG_IMM;
              end if;

            when ADD =>
              if data_en = '1' then
                if crw = '1' then --write mode expect data value
                  command_state <= READ_DATA;
                else
                  command_state <= WRITE_DATA;
                end if;
              else
                command_state <= ADD;
              end if;

            when READ_DATA => -- read data from host and write to mem
              if data_en = '1' then
                command_state <= SEND_CHECKSUM;
              else
                command_state <= READ_DATA;
              end if;

            when WRITE_DATA => -- write data to host
              command_state <= WAIT1;

            when WAIT1 =>
              command_state <= SEND_CHECKSUM;


            when SEND_CHECKSUM =>
              command_state <= FUN;

          end case;
        end if;
    end process;

end behavior;
