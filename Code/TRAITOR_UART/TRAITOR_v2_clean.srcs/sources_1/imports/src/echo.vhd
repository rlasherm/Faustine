library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use work.triggy_common.all;
use work.common.all;

entity echo is
  port(clk_in:            in std_logic;
       reset:               in std_logic;

       -- PC Uart
       rx_from_pc:          in std_logic;
       tx_to_pc:            out std_logic;

       --debug
       led:              out std_logic_vector(7 downto 0);
       ja:               out std_logic_vector(3 downto 0)
       );
end echo;

architecture behavior of echo is

  component clk_gen is
    port(
      clk_in1: in std_logic;
      clk_out1: out std_logic
    );
  end component;

  component uart is
    port(clk:  in std_logic;
         reset:     in std_logic;
         uart_conf: in UartConf;

         rx:        in std_logic;--rx wire
         tx:        out std_logic;--tx wire

         tx_data:   in std_logic_vector(7 downto 0);--data byte to write
         rx_data:   out std_logic_vector(7 downto 0);--data byte read

         rx_rdy:    out std_logic;--a byte is ready to be read
         tx_rdy:    out std_logic;--a byte can be written
         tx_start:  in std_logic;--write a byte

         error_flag: out std_logic;

         baud_clk_o: out std_logic
         );
  end component;


  signal pc_data: std_logic_vector(7 downto 0);
  signal pc_rx_rdy, pulse_rx_rdy, pc_tx_rdy, start: std_logic;

  signal uart_conf: UartConf := (idle_polarity => '1',
                                  baud_gen_step => X"04B7F5A5",-- To adapt with clk period
                                  start_bits => One,
                                  stop_bits => One,
                                  parity => None,
                                  data_bits => Eight);

  signal clk: std_logic;

  signal baud_clk: std_logic;
  signal error_flag: std_logic;

  signal tx_buf: std_logic;
  signal data_buf: std_logic_vector(7 downto 0);

  begin

    led <= data_buf;
    --data_buf <= pc_data when pc_rx_rdy = '1' else data_buf;
    data_buf <= pc_data;

    ja(0) <= rx_from_pc;
    ja(1) <= pc_rx_rdy;
    ja(2) <= error_flag;
    ja(3) <= tx_buf;

    clk_gen_inst: clk_gen
      port map(
        clk_in1   => clk_in,
        clk_out1  => clk
      );


    tx_to_pc <= tx_buf;

    pc_uart: uart
      port map(
        clk           => clk,
        reset         => reset,

        uart_conf     => uart_conf,

        rx            => rx_from_pc,
        tx            => tx_buf,

        tx_data       => data_buf,
        rx_data       => pc_data,

        rx_rdy        => pc_rx_rdy,
        tx_rdy        => pc_tx_rdy,
        tx_start      => pc_rx_rdy,
        error_flag    => error_flag,
        baud_clk_o    => baud_clk
      );


end behavior;
