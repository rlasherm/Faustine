library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use work.triggy_common.all;
use work.common.all;

entity trig_module is
  port(clk:             in std_logic;
       reset:           in std_logic;

       trig_in:         in std_logic;--start trig
       trig_conf:       in TrigConf;

       trig_out:        out std_logic
       );
end trig_module;

architecture behavior of trig_module is

  signal trig_in_buf: std_logic;

  signal waiting: std_logic;
  signal trig: std_logic;

  signal armed: std_logic;

  signal counter_wait: unsigned(31 downto 0);
  signal counter_trig: unsigned(31 downto 0);

  begin

    trig_in_buf <= trig_in when armed = '0' and trig_conf.repeat = '0'
              else trig_in when trig_conf.repeat = '1'
              else '0';

    armed_update: process(clk, reset)
      begin
        if reset = '1' then
          armed <= '0';
        elsif rising_edge(clk) then
          if waiting = '0' and trig = '0' and trig_in_buf = '1' then
            armed <= '1';
          elsif trig_in = '0' then
            armed <= '0';
          end if;
        end if;
    end process;

    wait_out: process(clk, reset)
      begin
        if reset = '1' then
          waiting <= '0';
        elsif rising_edge(clk) then
          if waiting = '0' and trig = '0' and trig_in_buf = '1' then
            waiting <= '1';
          elsif waiting = '1' and counter_wait = trig_conf.trig_wait then
            waiting <= '0';
          end if;
        end if;
    end process;

    count_wait: process(clk, reset)
      begin
        if reset = '1' then
          counter_wait <= (others => '0');
        elsif rising_edge(clk) then
          if waiting = '1' then
            counter_wait <= counter_wait + 1;
          else
            counter_wait <= (others => '0');
          end if;
        end if;
    end process;

    trig_out <= not trig_conf.idle_polarity when trig = '1' else trig_conf.idle_polarity;

    tr_out: process(clk, reset)
      begin
        if reset = '1' then
          trig <= '0';
        elsif rising_edge(clk) then
          if waiting = '1' and counter_wait = trig_conf.trig_wait then
            trig <= '1';
          elsif trig = '1' and counter_trig = trig_conf.trig_width then
            trig <= '0';
          end if;
        end if;
    end process;

    count_trig: process(clk, reset)
      begin
        if reset = '1' then
          counter_trig <= (others => '0');
        elsif rising_edge(clk) then
          if trig = '1' then
            counter_trig <= counter_trig + 1;
          else
            counter_trig <= (others => '0');
          end if;
        end if;
    end process;

end behavior;
