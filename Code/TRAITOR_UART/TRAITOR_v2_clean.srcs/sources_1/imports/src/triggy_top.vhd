library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use work.triggy_common.all;
use work.common.all;
use work.all;

entity triggy_top is
  port(clk_in:          in std_logic;
       reset:           in std_logic;
       trig_delays_out:       out TrigDelays;
       trig_width_out:  out std_logic_vector(31 downto 0);
       -- PC Uart
       rx_from_pc:      in std_logic;
       tx_to_pc:        out std_logic;

       --intercept uart
       uart_intercept:  in std_logic;

       --trigs
      trig_ext:         in std_logic
       );
end triggy_top;

architecture behavior of triggy_top is

  component clk_gen is
    port(
      clk_in1: in std_logic;
      clk_out1: out std_logic
    );
  end component;

  component fifo IS
    PORT (
      clk : IN STD_LOGIC;
      rst : IN STD_LOGIC;
      din : IN STD_LOGIC_VECTOR(7 DOWNTO 0);
      wr_en : IN STD_LOGIC;
      wr_rst_busy : OUT STD_LOGIC;
      rd_en : IN STD_LOGIC;
      rd_rst_busy : OUT STD_LOGIC;
      dout : OUT STD_LOGIC_VECTOR(7 DOWNTO 0);
      full : OUT STD_LOGIC;
      empty : OUT STD_LOGIC
    );
  end component;

  component uart is
    port(clk:  in std_logic;
         reset:     in std_logic;
         uart_conf: in UartConf;

         rx:        in std_logic;--rx wire
         tx:        out std_logic;--tx wire

         tx_data:   in std_logic_vector(7 downto 0);--data byte to write
         rx_data:   out std_logic_vector(7 downto 0);--data byte read

         rx_rdy:    out std_logic;--a byte is ready to be read
         tx_rdy:    out std_logic;--a byte can be written
         tx_start:  in std_logic;--write a byte

         error_flag: out std_logic;

         baud_clk_o: out std_logic;
         tick_o:    out std_logic
         );
  end component;

  component configuration_manager is
    port(clk:  in std_logic;
         reset:     in std_logic;
         memout: out std_logic_vector(MAX_ADDRESS*8-1 downto 0);

         --fifo from Uart PC rx (cross clock domains)
         din:         in byte;
         din_rdy:     in std_logic;
         din_read:    out std_logic;-- send read order

         --fifo to Uart PC tx (cross clock domains)
         dout:        out byte;
         dout_write:  out std_logic;-- send write order

        --from apdu follower
        counter_updated:   in std_logic;
        counter_value:     in u32;
        apdu_conf:         out APDUConf;


        -- triggers
        trig_confs:       out TrigConfs;
        -- uart intercept
        uart_conf:        out UartConf;
        uart_trig_conf:   out UartTrigConf
         );
  end component;

  component apdu_uart_trigger is
    port(clk:           in std_logic;
         reset:         in std_logic;

         io:            in std_logic; --the intercepted uart wire

         apdu_conf:       in APDUConf;
         uart_trig_conf:  in UartTrigConf;
         uart_conf:       in UartConf;

         --uart trig
         uart_trig_o:     out std_logic;

         --apdu follower output
         counter_updated: out std_logic;
         counter_value:   out u32;
         debug:           out byte
         );
  end component;

  component pulser is
    port(clk_in:       in std_logic;
         clk_out:       in std_logic;
         reset:     in std_logic;
         pulse_in:  in std_logic;
         pulse_out: out std_logic
         );
  end component;

  component fifo_autoreader is
    port(clk:             in std_logic;
         reset:           in std_logic;
         enable:          in std_logic;
         fifo_empty:      in std_logic;
         fifo_data:       in std_logic_vector(7 downto 0);
         fifo_read_order: out std_logic;

         data:            out std_logic_vector(7 downto 0);
         data_en:         out std_logic
         );
  end component;

  component trig_module is
    port(clk:             in std_logic;
         reset:           in std_logic;

         trig_in:         in std_logic;--start trig
         trig_conf:       in TrigConf;

         trig_out:        out std_logic
         );
  end component;

  signal clk: std_logic;

  --PC uart
  signal pc_tx_data, pc_rx_data: std_logic_vector(7 downto 0);
  signal pc_error_flag: std_logic;
  signal tx_to_pc_buf: std_logic;
  signal wr_rst_busy_01: std_logic;
  signal wr_rst_busy_02: std_logic;
  signal rd_rst_busy_01: std_logic;
  signal rd_rst_busy_02: std_logic;
  signal uart_conf_s: UartConf;
  signal delay: u32;
  signal start_delay: std_logic;

  -- fifo rx
  signal fifo_rx_dout, fifo_rx_din: std_logic_vector(7 downto 0);
  signal fifo_rx_available, fifo_rx_empty, fifo_rx_read_order: std_logic;

  -- fifo tx
  signal fifo_tx_dout, fifo_tx_din: std_logic_vector(7 downto 0);
  signal fifo_tx_empty, fifo_tx_write_order, fifo_tx_read, fifo_tx_enable, fifo_tx_enlock: std_logic;

  signal pc_rx_rdy, pulse_rx_rdy, pc_tx_rdy, pc_tx_start: std_logic;
  signal baud_clk: std_logic;

  -- triggers
  signal trig_starts: std_logic_vector(NB_TRIGGERS-1 downto 0);
  signal triggers_buf: std_logic_vector(NB_TRIGGERS-1 downto 0);
  signal trig_confs: TrigConfs;

  -- uart intercept
  signal uart_intercept_conf: UartConf;
  signal uart_intercept_trig_conf: UartTrigConf;
  signal uart_intercept_trig: std_logic;

  --apdu follower
  signal counter_updated: std_logic;
  signal counter_value: u32;
  signal apdu_conf: APDUConf;



  --debug
  signal trig_counter: u8;

  begin
    uart_conf_s<= (idle_polarity => '1',
                                    baud_gen_step => X"04B7F5A5",-- To adapt with clk period -> 115200 bauds
                                    start_bits => One,
                                    stop_bits => One,
                                    parity => None,
                                    data_bits => Eight);
                                    
                                    
trig_width_out <= std_logic_vector(trig_confs(0).trig_width(31 downto 0));
trig_delayout: for i in 0 to NB_TRIGGERS-1 generate
            trig_delays_out(i) <=  trig_confs(i).trig_wait(31 downto 0);
            end generate;

    debug_proc: process(clk, reset)
    begin
      if reset = '1' then
        trig_counter <= X"00";
      elsif rising_edge(clk) then
        if trig_starts(0) = '1' or trig_starts(1) = '1' or trig_starts(2) = '1' or trig_starts(3) = '1' then
          trig_counter <= trig_counter + 1;
        end if;
      end if;
    end process;


    -- clok generation
    clk_gen_inst: clk_gen
      port map(
        clk_in1   => clk_in,
        clk_out1  => clk
      );

    -- signal to write to fifo form uart
    pc_uart_pulser: pulser
      port map(
        clk_in      => baud_clk,
        clk_out     => clk,
        reset       => reset,
        pulse_in    => pc_rx_rdy,
        pulse_out   => pulse_rx_rdy
      );

    -- UART between Triggy and host PC
    tx_to_pc <= tx_to_pc_buf;
    pc_uart: uart
      port map(
        clk           => clk,
        reset         => reset,

        uart_conf     => uart_conf_s,

        rx            => rx_from_pc,
        tx            => tx_to_pc_buf,

        tx_data       => pc_tx_data,
        rx_data       => pc_rx_data,

        rx_rdy        => pc_rx_rdy,
        tx_rdy        => pc_tx_rdy,
        tx_start      => pc_tx_start,
        error_flag    => pc_error_flag,

        baud_clk_o    => baud_clk,
--        tick_o        => debug(0)
         tick_o        => open
      );


      -- debug <= fifo_rx_dout;

      -- BUFFER between PC Uart and config_manager: fifo + autoreaders

    pc_uart_rx_fifo: fifo
        port map(
        rst           => reset,
        clk           => clk,
        din           => pc_rx_data,
        wr_en         => pulse_rx_rdy,
        rd_en         => fifo_rx_read_order,
        dout          => fifo_rx_dout,
        rd_rst_busy   => rd_rst_busy_01,
        wr_rst_busy   => wr_rst_busy_01,
        full          => open,
        empty         => fifo_rx_empty
        );

      fifo_rx_available <= not(fifo_rx_empty);

      pc_uart_tx_fifo: fifo
        port map(
        rst           => reset,
        clk           => clk,
        din           => fifo_tx_din,
        wr_en         => fifo_tx_write_order,
        rd_en         => fifo_tx_read,
        dout          => fifo_tx_dout,
        rd_rst_busy   => rd_rst_busy_02,
        wr_rst_busy   => wr_rst_busy_02,
        full          => open,
        empty         => fifo_tx_empty
        );


      process(clk, reset)
      begin
        if reset = '1' then
          fifo_tx_enable <= '1';
          fifo_tx_enlock <= '0';
          delay <= X"00000000";
          start_delay <= '0';
        elsif rising_edge(clk) then
          if fifo_tx_read = '1' then
            fifo_tx_enable <= '0';
            fifo_tx_enlock <= '1';
            start_delay <= '0';
          elsif pc_tx_rdy = '0' then
            fifo_tx_enlock <= '0';
            start_delay <= '1';
            delay <= X"00000100";
          elsif fifo_tx_enlock = '0' and pc_tx_rdy = '1' then
            if start_delay = '1' then
              if delay = X"00000000" then
                fifo_tx_enable <= '1';
                start_delay <= '0';
              else
                delay <= delay - 1;
              end if;
            end if;
          end if;
        end if;
      end process;

      fifo_to_tx: fifo_autoreader
        port map(
          clk               => clk,
          reset             => reset,
          enable            => fifo_tx_enable,-- pc_tx_rdy,
          fifo_empty        => fifo_tx_empty,
          fifo_data         => fifo_tx_dout,
          fifo_read_order   => fifo_tx_read,

          data              => pc_tx_data,
          data_en           => pc_tx_start
        );

      -- where the Triggy configuration is stored

      config_manager: configuration_manager
        port map(
          clk      => clk,
          reset         => reset,
          din           => fifo_rx_dout,
          din_rdy       => fifo_rx_available,
          din_read      => fifo_rx_read_order,

          --fifo to Uart PC tx (cross clock domains)
          dout          => fifo_tx_din,
          dout_write    => fifo_tx_write_order,-- send write order

          --from apdu follower
          counter_updated => counter_updated,
          counter_value   => counter_value,
          apdu_conf       => apdu_conf,

          -- triggers
          trig_confs      => trig_confs,

          -- uart intercept
          uart_conf       => uart_intercept_conf,
          --uart_conf       => uart_conf,
          uart_trig_conf  => uart_intercept_trig_conf

          );

          -- uart and apdu triggers

        uart_apdu_trig: apdu_uart_trigger
          port map(clk            => clk,
               reset              => reset,
               io                 => uart_intercept,

               apdu_conf          => apdu_conf,
               uart_trig_conf     => uart_intercept_trig_conf,
               uart_conf          => uart_intercept_conf,

               --uart trig
               uart_trig_o        => uart_intercept_trig,

               --apdu follower output
               counter_updated    => counter_updated,
               counter_value      => counter_value,
              --  debug              => debug
               debug              => open
               );

          -- trigger modules


          trig_router: for i in 0 to NB_TRIGGERS-1 generate

            trig_starts(i) <=   --trig_starts(i)               when trig_confs(i).trig_source = "000" else
                                uart_intercept_trig               when trig_confs(i).trig_source = "001" else
                                counter_updated                   when trig_confs(i).trig_source = "010" else
                                trig_ext                          when trig_confs(i).trig_source = "011" else
                                triggers_buf(0)                   when trig_confs(i).trig_source = "100" and i/=0 else
                                triggers_buf(1)                   when trig_confs(i).trig_source = "101" and i/=1 else
                                triggers_buf(2)                   when trig_confs(i).trig_source = "110" and i/=2 else
                                triggers_buf(3)                   when trig_confs(i).trig_source = "111" and i/=3 else
                                trig_ext;


          end generate;

--          triggers <= triggers_buf;

          trig_gen: for i in 0 to NB_TRIGGERS-1 generate
            trig_ent: trig_module
              port map(
                  clk         => clk,
                  reset       => reset,

                  trig_in     => trig_starts(i),
                  trig_conf   => trig_confs(i),

                  trig_out    => triggers_buf(i)
              );
          end generate;

end behavior;
