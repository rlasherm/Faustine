----------------------------------------------------------------------------------
-- Company: INRIA
-- Engineer: Ludovic Claudepierre
-- 
-- Create Date: 31.10.2018 16:33:09
-- Design Name: 
-- Module Name: PLL_clk - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;
-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;
use work.triggy_common.all;
use work.common.all;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
library UNISIM;
use UNISIM.VComponents.all;

entity mmcm_reset is
    Port (  -- INPUT--
            clk_100 : in STD_LOGIC;
--            btn_phplus : in STD_LOGIC;
--            btn_phmoins : in STD_LOGIC;
--            btn_Dplus : in STD_LOGIC;
--            btn_Dmoins : in STD_LOGIC;
--            SelectSwitch : in STD_LOGIC;
            Dswitch : in STD_LOGIC_VECTOR(1 downto 0);
--            Fswitch : in STD_LOGIC_VECTOR(1 downto 0);
            stm_trig: in STD_LOGIC;
--            mem_glitch : std_logic_vector(MAX_ADDRESS*8-1 downto 0);
            trig_delay_in: in TrigDelays;
            trig_width_in : in  STD_LOGIC_VECTOR(31 downto 0);
            
            -- OUPUT--
--            phmoins_out  : out STD_LOGIC;
--            phplus_out  : out STD_LOGIC;
            led     : out STD_LOGIC_VECTOR (15 downto 0);
            Fault_trig_out : out STD_LOGIC;
--            stm_trig_out : out STD_LOGIC;
            clk_init : out STD_LOGIC;
            clk_dphase : out STD_LOGIC;
            clk_XOR_out : out STD_LOGIC;
--            clk_nXOR_out : out STD_LOGIC;
            clk_fault_out : out STD_LOGIC;
            clk_fault_out2 : out STD_LOGIC);
end mmcm_reset;


architecture Behavioral of mmcm_reset is
--    signal btn_meta     : std_logic := '0';
--    signal btn          : std_logic := '0';
--    signal speed_select : std_logic := '0';
--    signal counter      : std_logic_vector(26 downto 0) := (others => '0');
--    signal debounce     : std_logic_vector(15 downto 0) := (others => '0');
    signal clk_0 : std_logic := '0';
    signal clk_1 : std_logic := '0';
    signal clk_0nb : std_logic := '0';
    signal clk_1nb : std_logic := '0';
    signal clk_fb       : std_logic := '0';
    
    type t_state is (state_wait, state_enable, state_plus, state_moins, state_end);
    signal state   : t_state := state_wait;
    
    
    
    -----------------------------------------------------------------------------
    --- This is the CLKOUT0 ClkReg1 address - the only register to be played with 
    -----------------------------------------------------------------------------
    signal daddr : std_logic_vector(6 downto 0) := "0001000";
    signal do    : std_logic_vector(15 downto 0) := (others => '0');
    signal drdy  : std_logic := '0';
    signal den   : std_logic := '0';
    signal di    : std_logic_vector(15 downto 0) := (others => '0');
    signal dwe   : std_logic := '0';
    signal rst   : std_logic := '0';
    
    signal s_PSCLK  : std_logic := '0';
    signal s_PSEN   : std_logic := '0';
    signal s_PSINCDEC  : std_logic := '0';
    signal s_PSDONE  : std_logic := '0';
    signal s_PSDONE_buf  : std_logic := '0';
    ------------------------------------------------------------------------------
    ----                   Autres signaux Glitch Amplitude                    ----
    ------------------------------------------------------------------------------
    signal phplus : std_logic := '0';
    signal phmoins : std_logic := '0';
    signal flagphplus : std_logic := '0';
    signal flagphmoins : std_logic := '0';
    signal flagPSDONE : std_logic := '0';
    signal btn_phplus_b : std_logic := '0';
    signal btn_phmoins_b : std_logic := '0';
    signal btn_phplus_b2 : std_logic := '0';
    signal btn_phmoins_b2 : std_logic := '0';
    signal s_led : std_logic_vector(15 downto 0) := "0000000000000000";
    signal AGlitch : std_logic_vector(31 downto 0) ;--"0000000011011110";
    signal AGlitch_loc : std_logic_vector(31 downto 0);
    signal diffGlitch : std_logic_vector(31 downto 0);--"0000000011011110";
    
    ------------------------------------------------------------------------------
    ----                   Autres signaux Glitch Delay                    ----
    ------------------------------------------------------------------------------
    signal Dplus : std_logic := '0';
    signal Dmoins : std_logic := '0';
    signal btn_Dplus_b : std_logic := '0';
    signal btn_Dmoins_b : std_logic := '0';
    signal btn_Dplus_b2 : std_logic := '0';
    signal btn_Dmoins_b2 : std_logic := '0';
    
    signal Delay1 : std_logic_vector(31 downto 0) ; --"0000000000100000";--
    signal Delay2 : std_logic_vector(31 downto 0); --"0000000001101100";--
    signal Delay3 : std_logic_vector(31 downto 0) ;
    signal Delay4 : std_logic_vector(31 downto 0) ;
    signal Dcount_led : std_logic_vector(15 downto 0) := "0000000000000000";
--    signal Delay12 : std_logic_vector(15 downto 0);
--    signal Delay123 : std_logic_vector(15 downto 0);
--    signal Dcount : std_logic_vector(15 downto 0) := "0000000000000000";
--    signal Nfault : std_logic_vector(1 downto 0);
--    signal kfault : std_logic_vector(1 downto 0):="00";
    constant Nfault: Integer := NB_TRIGGERS; 
    shared variable Dcount : integer :=0;
    shared variable kfault: Integer := 0;
    shared variable delayfault: Integer := 0;
    
    signal stm_trig_b : std_logic := '0';
    signal stm_trig_b2 : std_logic := '0';
    signal stm_evt : std_logic := '0';
    signal Fault_trig : std_logic := '0';
    signal Fault_flag : std_logic := '0';
    signal clk_fault : std_logic := '0';
    signal clk_notfault : std_logic := '0';
    signal clk_XOR : std_logic := '0';
    signal clk_notXOR : std_logic := '0';
    signal clk_fault_XOR : std_logic := '0';
    
begin
-- // -- Output configuration signals -- // --
led<=Dcount_led(15 downto 0) when (Dswitch = "00") else Delay1(15 downto 0) when (Dswitch = "01") else Delay2(15 downto 0) when (Dswitch = "10") else Delay3(15 downto 0) when (Dswitch = "11");
clk_fault_out <= clk_fault;
clk_fault_out2 <= clk_notfault;

clk_XOR_out <= clk_notXOR;
--clk_nXOR_out <= clk_notXOR;

clk_XOR<= (clk_0 xor clk_1) and clk_0;
clk_notXOR<= not(clk_0 xor clk_1) and not(clk_1);

clk_init<=clk_0;
clk_dphase<=clk_1;

Fault_trig_out<=Fault_trig;

Dcount_led<=std_logic_vector(to_unsigned(Dcount, Dcount_led'length));
-- // ---------------------------------- // --
clk_fault <= clk_XOR when (Fault_trig='1') else clk_0;
clk_notfault <= clk_notXOR when (Fault_trig='1') else clk_0;

s_PSCLK<=clk_0;
--NFault<="10";
--//-------chargement des Delay de enregitrés en mémoire----------//
Delay1<=std_logic_vector(trig_delay_in(0));
Delay2<=std_logic_vector(trig_delay_in(1));
Delay3<=std_logic_vector(trig_delay_in(2));
Delay4<=std_logic_vector(trig_delay_in(3));
--//--------------------------------------------------------------//
-- rising edge buttons
--phplus<=(btn_phplus_b xor btn_phplus_b2) and btn_phplus_b ;
--phmoins<=(btn_phmoins_b xor btn_phmoins_b2) and btn_phmoins_b ;
Dplus<=(btn_Dplus_b xor btn_Dplus_b2) and btn_Dplus_b ;
Dmoins<=(btn_Dmoins_b xor btn_Dmoins_b2) and btn_Dmoins_b ;

stm_evt<=(stm_trig_b xor stm_trig_b2) and stm_trig_b ;
--//------------- Creation des sorties clk_out (horloge fautée) et du trigger mis en forme ------------//--

   -------------------------------------------------------
   -- Generate a 24MHz clock from the 100MHz 
   -- system clock 
   ------------------------------------------------------- 
   MMCME2_ADV_inst : MMCME2_ADV
      generic map (
         BANDWIDTH => "OPTIMIZED",      -- Jitter programming (OPTIMIZED, HIGH, LOW)
         CLKFBOUT_MULT_F => 10.0,        -- Multiply value for all CLKOUT (2.000-64.000).
         CLKFBOUT_PHASE => 0.0,         -- Phase offset in degrees of CLKFB (-360.000-360.000).
         -- CLKIN_PERIOD: Input clock period in ns to ps resolution (i.e. 33.333 is 30 MHz).
         CLKIN1_PERIOD => 10.0,
         CLKIN2_PERIOD => 0.0,
         -- CLKOUT0_DIVIDE - CLKOUT6_DIVIDE: Divide amount for CLKOUT (1-128)
         --CLKOUT0_DIVIDE => 100,
         CLKOUT1_DIVIDE => 125,
         CLKOUT2_DIVIDE => 125,
         CLKOUT3_DIVIDE => 1,
         CLKOUT4_DIVIDE => 1,
         CLKOUT5_DIVIDE => 1,
         CLKOUT6_DIVIDE => 1,
         CLKOUT0_DIVIDE_F => 125.000,       -- Divide amount for CLKOUT0 (1.000-128.000).
         -- CLKOUT0_DUTY_CYCLE - CLKOUT6_DUTY_CYCLE: Duty cycle for CLKOUT outputs (0.01-0.99).
         CLKOUT0_DUTY_CYCLE => 0.5,
         CLKOUT1_DUTY_CYCLE => 0.5,
         CLKOUT2_DUTY_CYCLE => 0.5,
         CLKOUT3_DUTY_CYCLE => 0.5,
         CLKOUT4_DUTY_CYCLE => 0.5,
         CLKOUT5_DUTY_CYCLE => 0.5,
         CLKOUT6_DUTY_CYCLE => 0.5,
         -- CLKOUT0_PHASE - CLKOUT6_PHASE: Phase offset for CLKOUT outputs (-360.000-360.000).
         CLKOUT0_PHASE => 0.0,
         CLKOUT1_PHASE => 0.0,
         CLKOUT2_PHASE => 0.0,
         CLKOUT3_PHASE => 0.0,
         CLKOUT4_PHASE => 0.0,
         CLKOUT5_PHASE => 0.0,
         CLKOUT6_PHASE => 0.0,
         CLKOUT4_CASCADE => FALSE,      -- Cascade CLKOUT4 counter with CLKOUT6 (FALSE, TRUE)
         COMPENSATION => "ZHOLD",       -- ZHOLD, BUF_IN, EXTERNAL, INTERNAL
         DIVCLK_DIVIDE => 1,            -- Master division value (1-106)
         -- REF_JITTER: Reference input jitter in UI (0.000-0.999).
         REF_JITTER1 => 0.0,
         REF_JITTER2 => 0.0,
         STARTUP_WAIT => FALSE,         -- Delays DONE until MMCM is locked (FALSE, TRUE)
         -- Spread Spectrum: Spread Spectrum Attributes
         SS_EN => "FALSE",              -- Enables spread spectrum (FALSE, TRUE)
         SS_MODE => "CENTER_HIGH",      -- CENTER_HIGH, CENTER_LOW, DOWN_HIGH, DOWN_LOW
         SS_MOD_PERIOD => 10000,        -- Spread spectrum modulation period (ns) (VALUES)
         -- USE_FINE_PS: Fine phase shift enable (TRUE/FALSE)
         CLKFBOUT_USE_FINE_PS => FALSE,
         CLKOUT0_USE_FINE_PS => FALSE,
         CLKOUT1_USE_FINE_PS => TRUE,
         CLKOUT2_USE_FINE_PS => FALSE,
         CLKOUT3_USE_FINE_PS => FALSE,
         CLKOUT4_USE_FINE_PS => FALSE,
         CLKOUT5_USE_FINE_PS => FALSE,
         CLKOUT6_USE_FINE_PS => FALSE 
      )
      port map (
         -- Clock Outputs: 1-bit (each) output: User configurable clock outputs
         CLKOUT0  => clk_0nb,
         CLKOUT0B => open,
         CLKOUT1  => clk_1nb,
         CLKOUT1B => open,
         CLKOUT2  => open,
         CLKOUT2B => open,
         CLKOUT3  => open,
         CLKOUT3B => open,
         CLKOUT4  => open,
         CLKOUT5  => open,
         CLKOUT6  => open,
         -- Dynamic Phase Shift Ports: 1-bit (each) output: Ports used for dynamic phase shifting of the outputs
         PSDONE => s_PSDONE,
         -- Feedback Clocks: 1-bit (each) output: Clock feedback ports
         CLKFBOUT => clk_fb,
         CLKFBOUTB => open,
         -- Status Ports: 1-bit (each) output: MMCM status ports
         CLKFBSTOPPED => open,
         CLKINSTOPPED => open,
         LOCKED       => open,
         -- Clock Inputs: 1-bit (each) input: Clock inputs
         CLKIN1   => clk_100,
         CLKIN2   => '0', 
         -- Control Ports: 1-bit (each) input: MMCM control ports
         CLKINSEL => '1',
         PWRDWN   => '0',             -- 1-bit input: Power-down
         RST      => rst,                   -- 1-bit input: Reset
   
         -- DRP Ports: 16-bit (each) output: Dynamic reconfiguration ports
         DCLK  => clk_0,                 -- 1-bit input: DRP clock
         DO    => DO,                     -- 16-bit output: DRP data
         DRDY  => DRDY,                 -- 1-bit output: DRP ready
         -- DRP Ports: 7-bit (each) input: Dynamic reconfiguration ports
         DADDR => DADDR,               -- 7-bit input: DRP address
         DEN   => DEN,                   -- 1-bit input: DRP enable
         DI    => DI,                     -- 16-bit input: DRP data
         DWE   => DWE,                   -- 1-bit input: DRP write enable
         
    
         -- Dynamic Phase Shift Ports: 1-bit (each) input: Ports used for dynamic phase shifting of the outputs
         PSCLK    => s_PSCLK,
         PSEN     => s_PSEN,
         PSINCDEC => s_PSINCDEC,
         -- Feedback Clocks: 1-bit (each) input: Clock feedback ports
         CLKFBIN => clk_fb
      );
      
--triggers confs
--gen_delay: for i in 0 to NB_TRIGGERS-1 generate
--    v_delay(i) <= unsigned(mem_glitch((TRIGGERS_ADD+i*TRIGGER_CONF_SIZE)*8 + 31 downto (TRIGGERS_ADD+i*TRIGGER_CONF_SIZE)*8));
--    v__width(i) <=unsigned(mem_glitch((TRIGGERS_ADD+i*TRIGGER_CONF_SIZE)*8 + 63 downto (TRIGGERS_ADD+i*TRIGGER_CONF_SIZE)*8 + 32));
--end generate;

      
      
phase_change_fsm: process(s_PSCLK)
begin
if rising_edge(s_PSCLK) then
    di <= (others => '0');
    dwe <= '0';
    den <= '0';
    
    AGlitch<=trig_width_in;
    
    if s_PSDONE='1' then
        flagPSDONE<='1';
    end if;
    
    case state is
        when state_wait =>
            
            if AGlitch>AGlitch_loc then
                diffGlitch<=AGlitch-AGlitch_loc;
                flagphplus<='1';
                flagphmoins<='0';
                state <= state_plus;
            elsif AGlitch<AGlitch_loc  then
                diffGlitch<=AGlitch_loc-AGlitch;
                flagphmoins<='1';
                flagphplus<='0';
                state <= state_moins;
            else
                state <= state_wait;
            end if;
        when state_plus =>
            s_PSINCDEC <= '1';
            state <= state_enable;
        when state_moins =>
            s_PSINCDEC <= '0';
            state <= state_enable;
        when state_enable =>
            s_PSEN<='1';
            state <= state_end;
        when state_end =>
            if flagPSDONE = '1' then
                if diffGlitch>0 then
                    diffGlitch<=diffglitch-'1';
                    if flagphplus='1' then
                        AGlitch_loc<=AGlitch_loc+'1';
                    elsif flagphmoins='1' then
                        AGlitch_loc<=AGlitch_loc-'1';
                    end if;
                end if;
                flagphplus<='0';
                flagphmoins<='0';
                state <= state_wait;
                flagPSDONE<='0';
            end if;
            s_PSEN <='0';
    end case;
end if;
end process;

--btnPhase_event: process(clk_0)
--begin
--    if rising_edge(clk_0) then
--            btn_phplus_b2<=btn_phplus_b;
--            btn_phplus_b<=btn_phplus;
            
--            btn_phmoins_b2<=btn_phmoins_b;
--            btn_phmoins_b<=btn_phmoins;
--    end if;
--end process;
        

--/------------------ Paramétrisation du DelayN --------------------------/--

Trigger_event: process(clk_0)
begin
    if rising_edge(clk_0) then
            stm_trig_b2<=stm_trig_b;
            stm_trig_b<=stm_trig;
    end if;
end process;  

Fault_trigger_signal: process(clk_0)   
begin
    if rising_edge(clk_0) and Nfault>0 then
        if Fault_flag='1' or stm_evt='1' then
            if kfault<Nfault then
                delayfault:=to_integer(trig_delay_in(kfault));
                
                if delayfault = 0 then
                    kfault:=kfault+1; -- si le kème delay est nul, on passe à la faute suivante (balayage de toute la mémoire)
                    Fault_trig<='0';
                    Fault_flag<='1';
                elsif Dcount = delayfault then
                    Fault_trig<='1';
                    Dcount:=Dcount+1;
                    Fault_flag<='1';
                    kfault:=kfault+1;
                else 
                    Fault_trig<='0';
                    Fault_flag<='1';
                    Dcount:=(Dcount + 1);
                end if;
            elsif kfault>=Nfault then
                Dcount:=0;
                Fault_trig<='0';
                Fault_flag<='0';
                kfault:=0;
            end if;
        end if;
    end if;
end process;
--/----------------------------------------------------------------------/--
--led_buffer: process(clk_0)
--begin
--    if rising_edge(clk_0) then
--            --led<=s_led;
--            case Dswitch is 
--                when "00" => s_led<=AGlitch;
--                when "01" => s_led <= Delay1;
--                when "10" => s_led <= Delay2;
--                when "11" => s_led <= Delay3;
--            end case;
--    end if;
--end process;  

buf_clk0: BUFG 
    port map (
        i => clk_0nb,
        o => clk_0
    );

buf_clk1: BUFG 
    port map (
        i => clk_1nb,
        o => clk_1
    );
end Behavioral;
