----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 18.03.2019 13:57:46
-- Design Name: 
-- Module Name: triggy_glitch - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------

library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use work.triggy_common.all;
use work.common.all;


entity triggy_glitch is
  Port (
        --//-------------- triggy_top -------------------//--
       clk_in:          in std_logic;
       reset:           in std_logic;

       -- PC Uart
       rx_from_pc:      in std_logic;
       tx_to_pc:        out std_logic;

       --intercept uart
       uart_intercept:  in std_logic;

       --trigs
      trig_ext:         in std_logic;
      
        --//-------------- CLK_glitch -------------------//--
        -- INPUT--
        Dswitch : in STD_LOGIC_VECTOR(1 downto 0);
--        Fswitch : in STD_LOGIC_VECTOR(1 downto 0);
        stm_trig: in STD_LOGIC;
        -- OUPUT--
        led     : out STD_LOGIC_VECTOR (15 downto 0);
        Fault_trig_out : out STD_LOGIC;
--            stm_trig_out : out STD_LOGIC;
        clk_init : out STD_LOGIC;
        clk_dphase : out STD_LOGIC;
        clk_XOR_out : out STD_LOGIC;
        clk_fault_out : out STD_LOGIC;
        clk_fault_out2 : out STD_LOGIC
        
         );
end triggy_glitch;

architecture Behavioral of triggy_glitch is

component triggy_top IS
    PORT (
      clk_in:          in std_logic;
      reset:           in std_logic;
    
      -- PC Uart
      rx_from_pc:      in std_logic;
      tx_to_pc:        out std_logic;
    
      --intercept uart
      uart_intercept:  in std_logic;
    
      --trigs
      trig_ext:         in std_logic;
--      triggers:         out std_logic_vector(NB_TRIGGERS-1 downto 0);
     
      trig_delays_out:       out TrigDelays;
      trig_width_out:       out std_logic_vector(31 downto 0)
    );
  end component;
  
component mmcm_reset IS
  PORT (
   clk_100 : in STD_LOGIC;
   Dswitch : in STD_LOGIC_VECTOR(1 downto 0);
   stm_trig: in STD_LOGIC;
   
    trig_delay_in: in TrigDelays;
    trig_width_in : in  STD_LOGIC_VECTOR(31 downto 0);
   -- OUPUT--
   led     : out STD_LOGIC_VECTOR (15 downto 0);
   Fault_trig_out : out STD_LOGIC;
--            stm_trig_out : out STD_LOGIC;
   clk_init : out STD_LOGIC;
   clk_dphase : out STD_LOGIC;
   clk_XOR_out : out STD_LOGIC;
   clk_fault_out : out STD_LOGIC;
   clk_fault_out2 : out STD_LOGIC
  );
end component;

    signal memoire : std_logic_vector(MAX_ADDRESS*8-1 downto 0);
    signal led_s : std_logic_vector(7 downto 0);
    signal trig_delays: TrigDelays;
    signal trig_width: std_logic_vector(31 downto 0);
begin

trig_uart: triggy_top 
    port map (
      clk_in => clk_in,
      reset => reset,
    
      -- PC Uart
      rx_from_pc=> rx_from_pc,
      tx_to_pc => tx_to_pc,
    
      --intercept uart
      uart_intercept => uart_intercept,
    
      --trigs
      trig_ext => trig_ext,
     trig_delays_out => trig_delays,
        trig_width_out => trig_width
    );


glitch_uart: mmcm_reset
    port map (
      clk_100 => clk_in,
       
       Dswitch => Dswitch,
       stm_trig => stm_trig,
        trig_delay_in => trig_delays,
        trig_width_in => trig_width,
       -- OUPUT--
       led  => led,
       Fault_trig_out => Fault_trig_out,
       clk_init => clk_init,
       clk_dphase => clk_dphase,
       clk_XOR_out => clk_XOR_out,
       clk_fault_out => clk_fault_out,
       clk_fault_out2 =>clk_fault_out2
    );
end Behavioral;
