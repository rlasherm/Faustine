# STM32 IO

Uses a NixShell workflow. [Download and install the Nix](curl -L https://nixos.org/nix/install | sh) package manager.



## Build

Launch the shell with
```
nix-shell
```
Then in the new shell, build the app with
```
make
```

Both commands can be automated by a single call to the build script:
```
./build.sh
```

## Programming the chip

### Host PC setup (to do once)

We must allow openocd to access the stlink debugger (to do once before the first programming).

```
sudo useradd -G plugdev $(whoami)
```

and add the udev rules

```
cp 49-stlinkv2.rules /etc/udev/rules.d
```

then restart the computer.

### Programming

Call the script, that uses openocd:
```
./Program/program.sh
```



## Various

Communication with the host computer is done with UART. We use the FTDIFriend module from ADAFruit to have UART2USB conversion. 
