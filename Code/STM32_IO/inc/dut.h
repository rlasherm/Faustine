/*
 * File: dut.h
 * Project: inc
 * Created Date: Tuesday October 26th 2021
 * Author: Ronan (ronan.lashermes@inria.fr)
 * -----
 * Last Modified: Tuesday, 26th October 2021 2:34:21 pm
 * Modified By: Ronan (ronan.lashermes@inria.fr>)
 * -----
 * Copyright (c) 2021 INRIA
 */

#ifndef DUT
#define DUT

#include <stdint.h>

extern uint32_t add1(uint32_t x); 

#endif