/*
 * File: dut.s
 * Project: src
 * Created Date: Tuesday October 26th 2021
 * Author: Ronan (ronan.lashermes@inria.fr)
 * -----
 * Last Modified: Tuesday, 26th October 2021 2:39:48 pm
 * Modified By: Ronan (ronan.lashermes@inria.fr>)
 * -----
 * Copyright (c) 2021 INRIA
 */

.syntax unified
.cpu cortex-m3
.fpu softvfp
.thumb

.global add1
add1:
    adds r0, r0, #1
    bx lr
    