################################################################################
# Toolchains
################################################################################
ROOT					?= $(CURDIR)/..
TOOLCHAIN_ROOT 			?= $(ROOT)/toolchains

TOOLCHAIN_URL			?= https://developer.arm.com/-/media/Files/downloads/gnu-rm/7-2017q4/gcc-arm-none-eabi-7-2017-q4-major-linux.tar.bz2?revision=375265d4-e9b5-41c8-bf23-56cbe927e156?product=GNU%20Arm%20Embedded%20Toolchain,64-bit,,Linux,7-2017-q4-major
TOOLCHAIN_DESIGNATION 	?= arm-elf

# AARCH32_PATH 			?= $(TOOLCHAIN_ROOT)/aarch32
# AARCH32_CROSS_COMPILE 		?= $(AARCH32_PATH)/bin/arm-linux-gnueabihf-
# AARCH32_GCC_VERSION 		?= gcc-linaro-$(TOOLCHAIN_DOWNLOAD_VERSION)-x86_64_arm-linux-gnueabihf
# SRC_AARCH32_GCC 		?= http://releases.linaro.org/components/toolchain/binaries/$(TOOLCHAIN_VERSION)/arm-linux-gnueabihf/${AARCH32_GCC_VERSION}.tar.xz

TOOLCHAIN_PATH 			?= $(TOOLCHAIN_ROOT)

GCC_VERSION = arm-none-eabi-gcc-7

toolchain:
	# mkdir -p $(AARCH32_PATH)
	# curl -L $(SRC_AARCH32_GCC) -o $(TOOLCHAIN_ROOT)/$(AARCH32_GCC_VERSION).tar.xz
	# tar xf $(TOOLCHAIN_ROOT)/$(AARCH32_GCC_VERSION).tar.xz -C $(AARCH32_PATH) --strip-components=1

	mkdir -p $(TOOLCHAIN_PATH)
	curl -L $(TOOLCHAIN_URL) -o $(TOOLCHAIN_ROOT)/$(GCC_VERSION).tar.xz
	tar xf $(TOOLCHAIN_ROOT)/$(GCC_VERSION).tar.xz -C $(TOOLCHAIN_PATH) --strip-components=1

	# mkdir -p $(LEGACY_AARCH64_PATH)
	# curl -L $(LEGACY_SRC_AARCH64_GCC) -o $(TOOLCHAIN_ROOT)/$(LEGACY_AARCH64_GCC_VERSION).tar.xz
	# tar xf $(TOOLCHAIN_ROOT)/$(LEGACY_AARCH64_GCC_VERSION).tar.xz -C $(LEGACY_AARCH64_PATH) --strip-components=1

toolchain-clean:
	rm -r $(TOOLCHAIN_ROOT)