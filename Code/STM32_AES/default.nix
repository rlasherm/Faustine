/*
 * File: default.nix
 * Project: STM32_IO
 * Created Date: Tuesday October 26th 2021
 * Author: Ronan (ronan.lashermes@inria.fr)
 * -----
 * Last Modified: Tuesday, 26th October 2021 3:02:28 pm
 * Modified By: Ronan (ronan.lashermes@inria.fr>)
 * -----
 * Copyright (c) 2021 INRIA
 */
with import <nixpkgs> {};

let
  pkgs = import (builtins.fetchGit {
         # Descriptive name to make the store path easier to identify                
         name = "my-old-revision";                                                 
         url = "https://github.com/nixos/nixpkgs-channels/";                       
         ref = "refs/heads/nixpkgs-unstable";                     
         rev = "c83e13315caadef275a5d074243c67503c558b3b";                                           
     }) {};  
in 

 # Make a new "derivation" that represents our shell
stdenv.mkDerivation {
  name = "app_maker";

  # The packages in the `buildInputs` list will be added to the PATH in our shell
  buildInputs = with pkgs; [
    gcc-arm-embedded
    openocd
    libusb1
    libftdi1
    which
  ];
}