#!/usr/bin/env nix-shell
#!nix-shell ../default.nix -i bash
#nix-shell must not be pure to access usb devices

# the particular way to find the binary is there to allow sudo use under nix-shell
# but works natively too

SCRIPTPATH="$( cd "$(dirname "$0")" >/dev/null 2>&1 ; pwd -P )"

if [ -z $1 ]; then
$(which openocd) -f $SCRIPTPATH/stm32vldiscovery.cfg -c "program ../aes.elf verify reset exit"
else
$(which openocd) -f $SCRIPTPATH/stm32vldiscovery.cfg -c "program $1 verify reset exit"
fi
