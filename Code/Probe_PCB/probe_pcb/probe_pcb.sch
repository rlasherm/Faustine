EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Connector:Conn_Coaxial J1
U 1 1 62541A1E
P 3200 1350
F 0 "J1" H 3300 1325 50  0000 L CNN
F 1 "SMB" H 3300 1234 50  0000 L CNN
F 2 "" H 3200 1350 50  0001 C CNN
F 3 " ~" H 3200 1350 50  0001 C CNN
	1    3200 1350
	1    0    0    -1  
$EndComp
$Comp
L Device:L_Core_Ferrite L1
U 1 1 625420D8
P 3050 2350
F 0 "L1" V 2869 2350 50  0000 C CNN
F 1 "EM probe" V 2960 2350 50  0000 C CNN
F 2 "" H 3050 2350 50  0001 C CNN
F 3 "~" H 3050 2350 50  0001 C CNN
	1    3050 2350
	0    1    1    0   
$EndComp
$Comp
L Device:D_Zener D1
U 1 1 6254633D
P 3050 2000
F 0 "D1" H 3050 2216 50  0000 C CNN
F 1 "1.5KE600A" H 3050 2125 50  0000 C CNN
F 2 "" H 3050 2000 50  0001 C CNN
F 3 "~" H 3050 2000 50  0001 C CNN
	1    3050 2000
	1    0    0    -1  
$EndComp
Wire Wire Line
	3200 2000 3200 2350
Connection ~ 3200 2000
Wire Wire Line
	2900 2350 2900 2000
Wire Wire Line
	2900 2000 2900 1350
Wire Wire Line
	2900 1350 3000 1350
Connection ~ 2900 2000
Wire Wire Line
	3200 1550 3200 2000
$EndSCHEMATC
