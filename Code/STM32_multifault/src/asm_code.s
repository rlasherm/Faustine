.cpu cortex-m3
.thumb

.section .text
.globl asm_ldr
.globl asm_reg
.globl asm_apsr
.globl asm_add
.globl asm_misc

#.type asm_code,@function


asm_ldr:
	mov R3, R0
	nop
	nop
	nop
	nop
	nop
	nop
	nop
	nop
	ldr R2,[R0,#4]
	nop
	nop
	nop
	nop
	nop
	nop
	nop
	nop
	mov R4, PC
	add R4, R4, #5
	cmp R2, R1
	bne testBP
	mov R0,R2
	bx lr

testBP:
	nop
	nop
	nop
	nop
	nop
	nop
	nop
	nop
	mov R3,R2
	nop
	nop
	nop
	nop
	nop
	nop
	nop
	nop
	bx R4

asm_reg:
	str R1,[R0]
	bx lr

asm_apsr:
	mrs r7, apsr
	mov r0,r7
	bx lr

asm_add:
	mov R4,#0
	mov R4,R0
	nop
	nop
	nop
	nop
	add R4, #1
	add R4, #2
	add R4, #3
	add R4, #4
	add R4, #5
	add R4, #6
	add R4, #7
	add R4, #8
	add R4, #9
	nop
	mov R0, R4
	bx lr

	asm_misc:
		mov R4,R0
		mov R3,#0
		add R4, #1
		add R4, #2
		mov R2, R4
		mov R4, R3
		add R4, #5
		add R4, #6
		mov R0, R2
		mov R1, R4
		bx lr
