\documentclass[10pt,a4paper]{article}
\usepackage[utf8]{inputenc}
\usepackage[english]{babel}
\usepackage{amsmath}
\usepackage{amsfonts}
\usepackage{amssymb}
\usepackage{hyperref}

\author{Ronan Lashermes}
\title{Process for a fault injection on a new target}
\begin{document}
\maketitle

\section{Introduction}

This document aims to describe the process to perform a EM fault injection on a new target with Secure-IC Analyzer (ANZ) software.

\section{Overview}

The goal is to add a target (an application embedded in a device) for fault injection evaluation.
The ANZ differentiates the application (called \textit{Target} in the ANZ) and the device (called \textit{TestPlatform} in the ANZ).
Communications between the target and the host computer should be done with either UART or Ethernet.

Including a new target comes down to the following steps:
\begin{itemize}
\item Design and properly document your application \textbf{and its communication protocol}.
\item Plan and design your triggering system.
\item Plan and design your resetting system.
\item Create an ANZ \textit{TestPlatform} plugin (describes how to communicate with the device).
\item Create an ANZ \textit{Target} plugin (describes the communication protocol).
\item Test your system without faults.
\item Perform the fault injection.
\item Extract useful data for your attack.
\end{itemize}

With this document, you should find an example project targeting an AES implementation on a STM32 $\mu C$. This example includes:
\begin{itemize}
\item The application on the STM32,
\item the \textit{TestPlatform} plugin (STM32VLDISCO),
\item the \textit{Target} plugin (AESP1)
\end{itemize}

The ANZ plugins must be coded in java.

\section{Design the communication protocol}

You should plan ahead your communication protocol following the following layout:
\begin{itemize}
\item Init command: called once at the beginning of the experiment. Send data only.
\item Close command: called once at the end of the experiment. Send data only.
\item SetKey command: called once (not sure, to check) after init, this is a configuration command. Send data only.
\item ProcessData command: called at each execution. Send data only.
\item GetResult command: called at each execution after process data. Send and receive data.
\end{itemize}
 All these commands work with byte arrays.
 
 A consequence of this layout is that your application must support two sets of commands, one corresponding to ProcessData take inputs and compute a result. Another corresponding to GetResult should be able to return and shape the result data.

\section{Triggering system}

A critical feature of a fault injection is \textbf{when} to inject a fault. 3 solutions are mainly used:
\begin{itemize}
\item GPIO triggering: the cheating way, add specific code for the trigger in the application. This is not representative of a real application but is the most efficient (and simple) way to obtain a good trigger signal.
\item Communication triggering: intercept communications between the host and the target. At the correct instant (depending on the comm protocol), issue a trigger. Relatively easy to do with a little time, do not require to change the targeted application.
\item Digital and analog signal triggering: use and combine any available digital or analog signal (probing or side-channels). This is difficult to do efficiently: a trade-off must be made between trigger speed and computation required to trigger. Still achievable with time and dedicated hardware.
\end{itemize}

In our example we use GPIO triggering but communication triggering is achievable easily with our oscilloscope by intercepting UART commands. Still this solution would trig at the start of the AES instead of around a specific SubBytes operation. 

\section{Resetting system}

A fault attack will crash your device, which will be unable to manage and answer ``normal" commands.
A resetting system must be put in place at a higher level than the application.

In our example a JTAG-equivalent connexion is maintained through openOCD. It is this connexion which is used to reset the device.

Another possibility is to use another device (for example an $\mu C$) to perform hardware resets when required.

\section{\textit{TestPlatform} plugin}

The \textit{TestPlatform} plugin is used to describe the targeted device (and not the application protocol).
This plugin should describe how to connect to the device, send and receive commands, and may shape commands (from the application layer) before sending them to the device.

Your plugin class must implements the \textit{AdvancedTestPlatformInterface} interface.
You do not handle the communication connexion directly. Instead, you describe the connexion parameters inside a HashMap$<$String, Object$>$ object read with the getProperties() method.
The connexion type (UART or Ethernet) and the endpoint are determined at runtime by the user but all others parameters must be defined here. Example:

\begin{verbatim}
prop.put(AdvancedTestPlatformInterface.PROPERTIES_KEY_BAUDRATE, 115200);
prop.put(AdvancedTestPlatformInterface.PROPERTIES_KEY_DATABITS, COMDataBits.EIGHT);
prop.put(AdvancedTestPlatformInterface.PROPERTIES_KEY_PARITY, COMParity.NONE);
prop.put(AdvancedTestPlatformInterface.PROPERTIES_KEY_STOPBITS, COMStopBits.ONE);
\end{verbatim}

This parameters must be defined after the constructor ends. They will be used to provide a \textbf{COMPortHandle} object in the initialize method. This object has to be used for all communications, as a mutex. Example:

\begin{verbatim}
handle.lock();
handle.getOutputStream().write(...); //send data to device
handle.getInputStream().read(...); //read data from device
handle.unlock();
\end{verbatim}

An instance of the target plugin (TargetInterface) should be stored in the TestPlatform instance after being provided by the attachTarget method.

Then the TestPlatform is responsible for sending commands. Example:

\begin{verbatim}
@Override
public void processData(byte[] bytes) {
    send_commands(this.target.getProcessDataCommand(bytes));
}
\end{verbatim}

The process is transmit the command payload to the Target plugin and retrieve the shaped command.
Send this shaped command to the device. The \textit{send\_command} method is defined by us, cf the STM32VLDISCO plugin for an example.

\section{\textit{Target} plugin}

The \textit{Target} plugin is responsible for the communication protocol with your application.
For the various commands, it receives the payload and must return a list of shaped commands. 
The properties must define the different sizes of the commands, cf. our example project.
Example:

\begin{verbatim}
@Override
public ArrayList<byte[]> getProcessDataCommand(byte[] bytes) {
    byte[] cmd = new byte[17];
    cmd[0] = 'p';
    for(int i = 0; i < 16 && i < bytes.length; i++) {
        cmd[i+1] = bytes[i];
    }
    ArrayList<byte[]> res =  new ArrayList<byte[]>();
    res.add(cmd);
    
    byte[] cmd2 = new byte[1];
    cmd2[0] = 'g';
    res.add(cmd2);
    return  res;
    }
\end{verbatim}
Our protocol \textit{P1} defines the following commands: `pxxxxxxxxxxxxxxxx' where the `x' vector is the plaintext as a byte array (length 16)., `g' is a command the launch encryption.

The \textit{getProcessDataCommand} method has the plaintext as an argument and generates a list of two commands: the first is `p' + plaintext, the second is `g'. 

Additionally the \textit{getExpectedResult} should return the correct result expected. Useful in particular for fault attack. This method should not communicate with the device. For example in our case, we use the java AES to compute the correct ciphertext.

\section{Testing}

Once the plugins have been developed, it is time to test them. You can debug them with \textit{System.out.println} which data can be seen in ANZ console window.
Add the target and the test platform plugins in the correct folders for the ANZ. Then launch the ANZ, select them and choose Test TestPlatform when your new TestPlatform is selected.

\section{Fault injection}

If everything works, you may start your fault injection if you know what you are doing.

\section{Result extraction}

Results may be saved by the ANZ in a TRA file (sqlite database). You may extract desired information from this database with an custom utility program. Or by creating a FaultExploitation plugin (not the subject of this document).

\section{Conclusion}

You should now understand the process to add a new target for a fault injection evaluation.
For any comment or correction for this document, please contact me at \href{mailto:ronan.lashermes@inria.fr}{ronan.lashermes@inria.fr}.

\end{document}