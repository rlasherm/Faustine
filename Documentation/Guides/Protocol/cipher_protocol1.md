# Cipher Protocol 1 (CP1)

This is the simplest protocol to launch AES encryption over UART.

Each command starts with a one byte command, ASCII encoding of a letter:

- 't': Test connection, return a String if the command is received.
- 'p' + plaintext[16]: Send plaintext.
- 'k' + key[16]: Send key.
- 'g' Go, execute AES encryption
- 'c' Get ciphertext, expect 16 bytes corresponding to the ciphertext.
- 'f' + plaintext[16]: Fast mode, send plaintext, execute AES and give the ciphertext back. Expect 16 bytes corresponding to the ciphertext.

At startup, a welcome message is sent by the board to the host, notifying that the boot phase has terminated.