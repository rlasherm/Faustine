<!--
@Author: ronan
@Date:   28-11-2016
@Email:  ronan.lashermes@inria.fr
@Last modified by:   ronan
@Last modified time: 03-01-2017
-->



# Triggy documentation

**Triggy** is an IP used with the Arty board to add advanced triggering capabilities to the LHS platforms.

It features:
- Trigger shaper: from another trig source, generate a pulse after a specified delay that last a specified width.
- Uart decoding: trig source on an Uart sequence
- APDU follower: use the Uart decoding module to fill an APDU FSM. Trig on a specified command.
- APDU timer: precisely times the delay between an APDU command and its response.
- Dynamic configuration: all the parameters of the previous modules are configured from the host PC without modifying the IP.

## User documentation

This User documentation describes how to use the **Triggy** IP with the Arty board from Digilent. Triggy is configured through the triggy C library.

### UART configuration

- baudrate = 115200
- databits = 8
- stopbits = 1
- parity = "none"
- flowcontrol = "none"

### PIN connections

The table describing the board PIN functions is below:

|Board PIN|Description|
|---|---|
|BTN0|IP reset (current config is wiped out)|
|JC0|Trig ext (external trigger input)|
|JC3|Uart intercept io (uart to trig on, for APDU follower too)|
|JB0-3|Triggers 0 to 3 (output)|

## Dev documentation

There is good documentation on the Arty board on the digilent website.

### Synthesis and IP upload

Look for Digilent Arty's [tutorial](https://reference.digilentinc.com/learn/programmable-logic/tutorials/arty-programming-guide/start).

### Configuration memory layout

Counter values are in ticks. 1 tick = 10ns.

| Start address | Bytes count  | Description |
|---|---|---|
|0|1| (X X X X X X X reset_fifo) |
|1|4|Trig 0 wait counter (LSB@add, MSB@add+4)|
|5|4|Trig 0 width counter (LSB@add, MSB@add+4)|
|9|1|Trig 0 config (X X repeat trig_source:3 idle_polarity), sources are: 000=> immediate, 001=>uart sequence, 010=>apdu follower, 011=>external trigger, 1XX=>trigXX|
|10|4|Trig 1 wait counter|
|14|4|Trig 1 width counter|
|18|1|Trig 1 config|
|19|4|Trig 2 wait counter|
|23|4|Trig 2 width counter |
|27|1|Trig 2 config|
|28|4|Trig 3 wait counter|
|32|4|Trig 3 width counter|
|36|1|Trig 3 config|
|37|1|APDU follower configuration (X X X X X X X header_select_en)|
|38|4|APDU follower Wait counter (timeout)|
|42|4|APDU follower header select|
|46|4|Counter value from APDU follower (LSB@add, MSB@add+8)|
|50|4|Intercept UART step counter (step = round( baud rate * 16 * 2^32 / 100 000 000))|
|54|1|Intercept UART config (X X data_bits(0=>8, 1=>7) stop_bits(0=>1, 1=>2) start_bits(0=>1, 1=>2) parity:2(00=>none, 01=>Odd, 10=>Even) idle_polarity)|
|55|1|Uart sequence length|
|56|8|Uart sequence value to trig on|


### Immediate mode (function = 0x02)


The immediate mode allows manual triggers (to launch a hardware reset for example).
The argument byte select the triggers to trig: from 0 to 3 with the bit position.
Ex: sending 0x02 0X05 will select the immediate function and launch triggers 0 and 2 (0x05 = 0b00000101). The trigger parameters follow the corresponding configuration.

<!-- ### IP reset (function = 0x55) -->

<!-- ### APDU follower state machine reset (function = 0x03) -->


### Configuration protocol

The configuration is done with an UART from the host PC.
The simple protocol is as follows: host <-> triggy
- -> Function on 1 byte (u8): 0x00 for config read, 0x01 for config write, 0x02 for immediate, ..., 0xFF for test (other values are reserved).
- -> Address on 1 byte (u8).
- -> Data on 1 byte (u8) **if function is write**.
- <- Data on 1 byte (u8) **if function is read**.
- <- Acknowledge checksum (XOR of all command bytes from host to triggy) on 1 byte.

The test protocol is:
- -> 0xFF
- <- 0xFF (checksum)
