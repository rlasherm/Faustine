----------------------------------------------------------------------------------
-- Company: INRIA
-- Engineer: Ludovic Claudepierre
-- 
-- Create Date: 31.10.2018 16:33:09
-- Design Name: 
-- Module Name: PLL_clk - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;
-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
library UNISIM;
use UNISIM.VComponents.all;

entity clocking is
    Port ( --led: out std_logic_vector(2 downto 0);
        ledelay: out std_logic_vector(7 downto 0);
        stm_trig : in STD_LOGIC;    
        btn_plus : in STD_LOGIC;    
        btn_moins : in STD_LOGIC;    
        btn_plus2 : in STD_LOGIC;    
        btn_moins2 : in STD_LOGIC;    
        clk100MHz : in STD_LOGIC;
        clkfault_out : out STD_LOGIC;
        clkfault_out2 : out STD_LOGIC;
--        clkfaultlow_out : out STD_LOGIC;
        --clk_out : out STD_LOGIC;
        clk_out_XOR : out STD_LOGIC;
        clk_out_XOR_low : out STD_LOGIC;
        switch1 : in STD_LOGIC;
        switch2 : in STD_LOGIC;
        switch3 : in STD_LOGIC;
        switch4 : in STD_LOGIC;
        trig_out : out STD_LOGIC;
        stm_out : out STD_LOGIC
    );
           
end clocking;


architecture Behavioral of clocking is
    --signal phasemax: std_logic_vector(7 downto 0) := "00000011";
    signal delay: std_logic_vector(7 downto 0) := "01000100";-- "00001010"; --
    signal delayplus : STD_LOGIC;
    signal delaymoins : STD_LOGIC;
    signal delayk: std_logic_vector(10 downto 0) ;
    signal delayN: std_logic_vector(10 downto 0) ;
    signal delay_int: std_logic_vector(7 downto 0) := "01011110";--"00000010"; --
    signal delayplus2 : STD_LOGIC;
    signal delaymoins2 : STD_LOGIC;
    signal kfault: std_logic_vector(2 downto 0) ; -- Compteur de faute
    signal Nfault: std_logic_vector(2 downto 0) ; -- Nombre total de fautes voulues
    signal count: std_logic_vector(7 downto 0);
    signal count2: std_logic_vector(7 downto 0);
    signal btn_plus_buffered     : STD_LOGIC;
    signal btn_plus_buffered2     : STD_LOGIC;
    signal btn_moins_buffered     : STD_LOGIC;
    signal btn_moins_buffered2     : STD_LOGIC;
    signal btn_plus2_buffered     : STD_LOGIC;
    signal btn_plus2_buffered2     : STD_LOGIC;
    signal btn_moins2_buffered     : STD_LOGIC;
    signal btn_moins2_buffered2     : STD_LOGIC;
    signal stm_trig_buffered     : STD_LOGIC;
    signal stm_trig_buffered2     : STD_LOGIC;
    signal front_stm : STD_LOGIC;
    signal trig_btn : STD_LOGIC;
    signal trig_btn2 : STD_LOGIC;
    signal btn_buffered     : STD_LOGIC;
    signal btn_buffered2     : STD_LOGIC;
    signal btn2_buffered     : STD_LOGIC;
    signal btn2_buffered2     : STD_LOGIC;
    signal clk100MHz_buffered     : STD_LOGIC; --std_logic := '0';
    signal clkfb                  : STD_LOGIC; --std_logic := '0';
    signal front : STD_LOGIC;
    signal front_low : STD_LOGIC;
    signal front2 : STD_LOGIC;
    signal front2_low : STD_LOGIC;
    signal front_tot :STD_LOGIC;
    signal front_low_tot :STD_LOGIC;
    signal clk24MHzPhase1 : STD_LOGIC;
    signal clk24MHzPhase2 : STD_LOGIC;
    signal clk24MHzPhase3 : STD_LOGIC;
    signal clk24MHzPhase4 : STD_LOGIC;
    signal clk24MHzPhase5 : STD_LOGIC;
    signal clk24MHzPhase1_unbuffered : STD_LOGIC;
    signal clk24MHzPhase2_unbuffered : STD_LOGIC;
    signal clk24MHzPhase3_unbuffered : STD_LOGIC;
    signal clk24MHzPhase4_unbuffered : STD_LOGIC;
    signal clk24MHzPhase5_unbuffered : STD_LOGIC;
    
    signal clk_Phase0 : STD_LOGIC;
    signal clk24MHz   : STD_LOGIC;
    signal clk24MHz_unbuffered   : STD_LOGIC;
    signal clk24MHz90 : STD_LOGIC;
    signal clk24MHzXOR : std_logic;
    signal clk24MHzXOR_low : std_logic;
    
begin

--//------------- Creation des sorties clk_out (horloge fautée) et du trigger mis en forme ------------//--

--clkfault_out<= clk24MHzXOR when (front='1') else clk24MHzXOR when (front2='1') else clk24MHz;
--clkfaultlow_out<= clk24MHzXOR_low when (front='1') else clk24MHzXOR_low when (front2='1') else clk24MHz;
clkfault_out<= clk24MHzXOR when (front_tot='1') else clk24MHz;
clkfault_out2<= clk24MHzXOR when (front_tot='1') else clk24MHz;
--clkfaultlow_out<= clk24MHzXOR_low when (front_low_tot='1') else clk24MHz;
Nfault(0)<=switch1;
Nfault(1)<=switch2;
Nfault(2)<=switch3;
--/-------- signaux intermediaires -----------/--
clk24MHzXOR<= (clk24MHz xor clk24MHz90) and clk24MHz;
clk24MHzXOR_low<= not(clk24MHz xor clk24MHz90) and not(clk24MHz);
clk_out_XOR<=clk24MHzXOR;
clk_out_XOR_low<=clk24MHzXOR_low;
--clk_out<=clk24MHz;
trig_out<=front;-- io4
front_tot<=front or front2;
front_low_tot <= front_low or front2_low;


stm_out<=front_stm; -- io5
delayN<=delay+(Nfault-1)*delay_int;
delayk<=delay+kfault*delay_int;
--clk_out1<=clk24MHzPhase1;
--clk_out2<=clk24MHzPhase2;
--clk_out3<=clk24MHzPhase3;
--clk_out4<=clk24MHzPhase4;
--clk_out5<=clk24MHzPhase5;
clk24MHz90<=clk24MHzPhase5;--clk_Phase0;
--led <= idphase;
ledelay <= delay when switch4='0' else delay_int when switch4='1';
--/--------------------------------------------------/--

--/ ---------------------Buffer du trig provenant de la STM32------------------- /--
process(stm_trig, clk24MHz)        
begin   
    if clk24MHz'event and   clk24MHz='1' then
            stm_trig_buffered <= stm_trig ;
    end if;
end process;

process(stm_trig_buffered, clk24MHz)        
begin   
    if clk24MHz'event and   clk24MHz='1' then
            stm_trig_buffered2 <= stm_trig_buffered ;
    end if;
end process;

process(stm_trig_buffered2, stm_trig_buffered, btn_buffered)        
begin     
        if  (stm_trig_buffered2='0' and stm_trig_buffered='1') then --or  (btn_buffered2='0' and btn='1') then
            front_stm<='1';
        else
            front_stm<='0';
        end if;
end process;
--/---------------------------------------------------------------------------------/--

--//------------------------Buffer bouton Delay + ----------------------------//--
process(btn_plus, clk24MHz)        
begin   
    if clk24MHz'event and   clk24MHz='1' then
            btn_plus_buffered <= btn_plus ;
    end if;
end process;

process(btn_plus_buffered, clk24MHz)        
begin   
    if clk24MHz'event and   clk24MHz='1' then
            btn_plus_buffered2 <= btn_plus_buffered ;
    end if;
end process;

process(btn_plus_buffered2, btn_plus_buffered)        
begin     
        if  (btn_plus_buffered2='0' and btn_plus_buffered='1') then --or  (btn_buffered2='0' and btn='1') then
            delayplus<='1';
        else
            delayplus<='0';
        end if;
end process;
--//----------------------------------------------------------------------------//--
--//------------------------Buffer bouton Delay - ----------------------------//--
process(btn_moins, clk24MHz)        
begin   
    if clk24MHz'event and   clk24MHz='1' then
            btn_moins_buffered <= btn_moins ;
    end if;
end process;

process(btn_moins_buffered, clk24MHz)        
begin   
    if clk24MHz'event and   clk24MHz='1' then
            btn_moins_buffered2 <= btn_moins_buffered ;
    end if;
end process;

process(btn_moins_buffered2, btn_moins_buffered)        
begin     
        if  (btn_moins_buffered2='0' and btn_moins_buffered='1') then --or  (btn_buffered2='0' and btn='1') then
            delaymoins<='1';
        else
            delaymoins<='0';
        end if;
end process;
--//----------------------------------------------------------------------------//--
--/-/----------------------------- Delay +/-  -------------------------------/-/--
process(clk24MHz,delayplus, delaymoins)
begin
    if clk24MHz'event and   clk24MHz='1' then
        if delayplus='1' then
            delay<=delay+1;
        elsif delaymoins='1' then
            if delay <="00000001" then
                delay<=delay;
            else
                delay<=delay-1;
            end if;
        end if;
    end if;
end process;
--/-/-----------------------------------------------------------------------/-/--


--/------------------ Paramétrisation du Delay --------------------------/--
process(clk24MHz, front_stm)    
begin   
    if Nfault > 0 then
        if front_stm='1' then
            count<="00000000";
            trig_btn<='0';
        elsif clk24MHz'event and clk24MHz='1' then
            if count=delay then
                count<=count;
                trig_btn<='1';
            else
                count<=(count + 1);
                trig_btn<='0';
            end if;
        end if;
    end if;
end process;
--/----------------------------------------------------------------------/--

--///------------------- Calibrage du signal de trigger une fois retardé -----------------------///--

process(trig_btn, clk24MHz)        
begin   
    if clk24MHz'event and   clk24MHz='1' then
            btn_buffered <= trig_btn ;
    end if;
end process;

process(btn_buffered, clk24MHz)        
begin   
    if clk24MHz'event and   clk24MHz='1' then
            btn_buffered2 <= btn_buffered ;
    end if;
end process;

process(trig_btn, btn_buffered)        
begin     
        if  (btn_buffered2='0' and btn_buffered='1') then --or  (btn_buffered2='0' and btn='1') then
            front<='1';
        else
            front<='0';
        end if;
end process;

process(trig_btn, btn_buffered)        
begin     
        if  (btn_buffered2='1' and btn_buffered='0') then --or  (btn_buffered2='0' and btn='1') then
            front_low<='1';
        else
            front_low<='0';
        end if;
end process;
--///------------------------------------------------------------------------------------------///--

--//------------------------Buffer bouton Inter + ----------------------------//--
process(btn_plus2, clk24MHz)        
begin   
    if clk24MHz'event and   clk24MHz='1' then
            btn_plus2_buffered <= btn_plus2 ;
    end if;
end process;

process(btn_plus2_buffered, clk24MHz)        
begin   
    if clk24MHz'event and   clk24MHz='1' then
            btn_plus2_buffered2 <= btn_plus2_buffered ;
    end if;
end process;

process(btn_plus_buffered2, btn_plus2_buffered)        
begin     
        if  (btn_plus2_buffered2='0' and btn_plus2_buffered='1') then --or  (btn_buffered2='0' and btn='1') then
            delayplus2<='1';
        else
            delayplus2<='0';
        end if;
end process;
--//----------------------------------------------------------------------------//--
--//------------------------Buffer bouton Inter - ----------------------------//--
process(btn_moins2, clk24MHz)        
begin   
    if clk24MHz'event and   clk24MHz='1' then
            btn_moins2_buffered <= btn_moins2 ;
    end if;
end process;

process(btn_moins2_buffered, clk24MHz)        
begin   
    if clk24MHz'event and   clk24MHz='1' then
            btn_moins2_buffered2 <= btn_moins2_buffered ;
    end if;
end process;

process(btn_moins2_buffered2, btn_moins2_buffered)        
begin     
        if  (btn_moins2_buffered2='0' and btn_moins2_buffered='1') then --or  (btn_buffered2='0' and btn='1') then
            delaymoins2<='1';
        else
            delaymoins2<='0';
        end if;
end process;
--//----------------------------------------------------------------------------//--
--/-/----------------------------- Delay_inter +/-  -------------------------------/-/--
process(clk24MHz,delayplus2, delaymoins2)
begin
    if clk24MHz'event and   clk24MHz='1' then
        if delayplus2='1' then
            delay_int<=delay_int+1;
        elsif delaymoins2='1' then
            if delay_int <="00000001" then
                delay_int<=delay_int;
            else
                delay_int<=delay_int-1;
            end if;
        end if;
    end if;
end process;
--/-/-----------------------------------------------------------------------/-/--
--/------------------ Paramétrisation du DelayN --------------------------/--
process(clk24MHz, front_stm)    
begin
    if Nfault>1 then   
        if front_stm='1' then
            count2<="00000000";
            trig_btn2<='0';
            kfault<="001";
        elsif clk24MHz'event and clk24MHz='1' then
            if count2=delayN then
                count2<=count2;
                trig_btn2<='1';
            elsif count2=delayk then
                count2<=count2+1;
                kfault<=kfault+1;
                trig_btn2<='1';
            else
                count2<=(count2 + 1);
                trig_btn2<='0';
            end if;
        end if;
    end if;
end process;
--/----------------------------------------------------------------------/--
----///------------------- Calibrage du signal de trigger2 une fois retardé -----------------------///--

process(trig_btn2, clk24MHz)        
begin   
    if clk24MHz'event and   clk24MHz='1' then
            btn2_buffered <= trig_btn2 ;
    end if;
end process;

process(btn2_buffered, clk24MHz)        
begin   
    if clk24MHz'event and   clk24MHz='1' then
            btn2_buffered2 <= btn2_buffered ;
    end if;
end process;

process(trig_btn2, btn2_buffered)        
begin     
        if  (btn2_buffered2='0' and btn2_buffered='1') then
            front2<='1';
        else
            front2<='0';
        end if;
end process;

process(trig_btn, btn2_buffered)        
begin     
        if  (btn2_buffered2='1' and btn2_buffered='0') then
            front2_low<='1';
        else
            front2_low<='0';
        end if;
end process;
--///------------------------------------------------------------------------------------------///--

--/ Buffer entre CLK et PLL /--    
bufg_100: BUFG 
    port map (
        i => clk100MHz,
        o => clk100MHz_buffered
    );
   -------------------------------------------------------
   -- Generate a 24MHz clock from the 100MHz 
   -- system clock 
   ------------------------------------------------------- 
pll_clocking : PLLE2_BASE
   generic map (
      BANDWIDTH          => "OPTIMIZED",
      CLKFBOUT_MULT      => 10,
      CLKFBOUT_PHASE     => 0.0,
      CLKIN1_PERIOD      => 10.0,

      -- CLKOUT0_DIVIDE - CLKOUT5_DIVIDE: Divide amount for each CLKOUT (1-128)
      CLKOUT0_DIVIDE     => 125,  CLKOUT1_DIVIDE     => 125, CLKOUT2_DIVIDE      => 125, 
      CLKOUT3_DIVIDE     => 125,  CLKOUT4_DIVIDE     => 125, CLKOUT5_DIVIDE      => 125,

      -- CLKOUT0_DUTY_CYCLE - CLKOUT5_DUTY_CYCLE: Duty cycle for each CLKOUT (0.001-0.999).
      CLKOUT0_DUTY_CYCLE => 0.5, CLKOUT1_DUTY_CYCLE => 0.5, CLKOUT2_DUTY_CYCLE => 0.5,
      CLKOUT3_DUTY_CYCLE => 0.5, CLKOUT4_DUTY_CYCLE => 0.5, CLKOUT5_DUTY_CYCLE => 0.5,

      -- CLKOUT0_PHASE - CLKOUT5_PHASE: Phase offset for each CLKOUT (-360.000-360.000).
      CLKOUT0_PHASE      =>    0.0, CLKOUT1_PHASE      => -348.1 , CLKOUT2_PHASE      => -352.4,
      CLKOUT3_PHASE      => -351.7, CLKOUT4_PHASE      => -350.6, CLKOUT5_PHASE      => -349.2,

      DIVCLK_DIVIDE      => 1,
      REF_JITTER1        => 0.0,
      STARTUP_WAIT       => "FALSE"
   )
   port map (
      CLKIN1   => CLK100MHz_buffered,
      CLKOUT0  => CLK24MHz_unbuffered,   CLKOUT1 => clk24MHzPhase1_unbuffered,  CLKOUT2 => clk24MHzPhase2_unbuffered,  
      CLKOUT3  => clk24MHzPhase3_unbuffered, CLKOUT4 => clk24MHzPhase4_unbuffered,  CLKOUT5 => clk24MHzPhase5_unbuffered,
      LOCKED   => open,
      PWRDWN   => '0', 
      RST      => '0',
      CLKFBOUT => clkfb,
      CLKFBIN  => clkfb
   );

--/ Buffer sortie de PLL /--
buf_24Mhz: BUFG 
    port map (
        i => clk24MHz_unbuffered,
        o => clk24MHz
    );

buf_24MhzPhase1: BUFG 
    port map (
        i => clk24MHzPhase1_unbuffered,
        o => clk24MHzPhase1
    );
    
buf_24MhzPhase2: BUFG 
    port map (
        i => clk24MHzPhase2_unbuffered,
        o => clk24MHzPhase2
    );
    
buf_24MhzPhase3: BUFG 
    port map (
        i => clk24MHzPhase3_unbuffered,
        o => clk24MHzPhase3
    );
    
    
buf_24MhzPhase4: BUFG 
    port map (
        i => clk24MHzPhase4_unbuffered,
        o => clk24MHzPhase4
    );
    
buf_24MhzPhase5: BUFG 
    port map (
        i => clk24MHzPhase5_unbuffered,
        o => clk24MHzPhase5
    );

end Behavioral;
