----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 17.01.2019 11:23:25
-- Design Name: 
-- Module Name: LED_afficheur - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
USE IEEE.STD_LOGIC_UNSIGNED.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
library UNISIM;
use UNISIM.VComponents.all;

entity LED_afficheur is
  Port (
    LCD_EN : out STD_LOGIC;
    LCD_RS : out STD_LOGIC;
    LCD_RW : out STD_LOGIC;
    LCD_DATA0 : out STD_LOGIC;
    LCD_DATA1: out STD_LOGIC;
    LCD_DATA2 : out STD_LOGIC;
    LCD_DATA3: out STD_LOGIC;
    LCD_DATA4: out STD_LOGIC;
    LCD_DATA5: out STD_LOGIC;
    LCD_DATA6: out STD_LOGIC;
    LCD_DATA7: out STD_LOGIC;
    swstartagain : in STD_LOGIC;
    s_button : in std_logic;
    clk : in std_logic
--    btn_plus : in STD_LOGIC;    
--    btn_moins : in STD_LOGIC;    
--    btn_plus2 : in STD_LOGIC;    
--    btn_moins2 : in STD_LOGIC;  
--    switch1 : in STD_LOGIC;
--    switch2 : in STD_LOGIC;
--    switch3 : in STD_LOGIC;
--    switch4 : in STD_LOGIC
       ); 
end LED_afficheur;

architecture Behavioral of LED_afficheur is

    Type state is (etat_rien, etat_efface, etat_efface2, etat_retour,etat_retour2, etat_conf, etat_conf2, etat_allume, etat_allume2, etat_mode, etat_mode2, etat_ecrit);
    --signal s_button1 : STD_LOGIC;
    signal s_button2 : STD_LOGIC;
    signal s_button_evt : STD_LOGIC;
    signal s_etat_now :state :=etat_rien;
    signal s_etat_prochain:state :=etat_rien;
    signal clk_slow_counter : STD_LOGIC_VECTOR(19 DOWNTO 0);
    signal clk_slow : STD_LOGIC;
    

    
begin
s_button_evt<=s_button2 nor s_button;

process (clk)
begin
if clk'event and clk = '1' then
    clk_slow_counter <= clk_slow_counter + 1;
    if clk_slow_counter > 524287 then
        clk_slow <= '0';
    else
        clk_slow <= '1';
    end if;
end if;
end process;

buf_button: BUFG 
    port map (
        i => s_button,
        o => s_button2
    );
    
--process (clk_slow)
--begin
--    if clk'event and clk = '1' then
--    s_button2 <= s_button1;
--    s_button1 <= s_button;
--    end if;
--end process;

process (clk_slow)
begin
if clk_slow'event and clk_slow = '1' then
s_etat_now <= s_etat_prochain;
end if;
end process;


process(s_etat_now, s_button_evt, swstartagain)
begin
        case s_etat_now is
        WHEN etat_rien => 
            IF s_button_evt = '1' THEN 
                s_etat_prochain <= etat_conf;
            ELSIF swstartagain = '1' THEN
                s_etat_prochain <= etat_efface;
            ELSE
                s_etat_prochain <= etat_rien;
            END IF;
            LCD_EN <= '0';
            LCD_RS <='0';
            LCD_RW <= '0';
            LCD_DATA0 <= '0';
            LCD_DATA1 <= '0';
            LCD_DATA2 <= '0';
            LCD_DATA3<= '0';
            LCD_DATA4<= '0';
            LCD_DATA5<= '0';
            LCD_DATA6<= '0';
            LCD_DATA7<= '0';
            
        WHEN etat_efface =>
            LCD_EN <= '1';
            LCD_RS <='0';
            LCD_RW <= '0';
            LCD_DATA0 <= '1';
            LCD_DATA1 <= '0';
            LCD_DATA2 <= '0';
            LCD_DATA3 <= '0';
            LCD_DATA4 <= '0';
            LCD_DATA5 <= '0';
            LCD_DATA6 <= '0';
            LCD_DATA7 <= '0';
            s_etat_prochain <= etat_efface2;
            
        WHEN etat_efface2 =>
            LCD_EN <= '0';
            LCD_RS <='0';
            LCD_RW <= '0';
            LCD_DATA0 <= '0';
            LCD_DATA1 <= '0';
            LCD_DATA2 <= '0';
            LCD_DATA3 <= '0';
            LCD_DATA4 <= '0';
            LCD_DATA5 <= '0';
            LCD_DATA6 <= '0';
            LCD_DATA7 <= '0';
            s_etat_prochain <= etat_retour;
            
        WHEN etat_retour =>
            LCD_EN <= '1';
            LCD_RS <='0';
            LCD_RW <= '0';
            LCD_DATA0 <= '0';
            LCD_DATA1 <= '1';
            LCD_DATA2 <= '0';
            LCD_DATA3 <= '0';
            LCD_DATA4 <= '0';
            LCD_DATA5 <= '0';
            LCD_DATA6 <= '0';
            LCD_DATA7 <= '0';
            s_etat_prochain <= etat_retour2;
            
        WHEN etat_retour2 =>
            LCD_EN <= '0';
            LCD_RS <='0';
            LCD_RW <= '0';
            LCD_DATA0 <= '0';
            LCD_DATA1 <= '0';
            LCD_DATA2 <= '0';
            LCD_DATA3 <= '0';
            LCD_DATA4 <= '0';
            LCD_DATA5 <= '0';
            LCD_DATA6 <= '0';
            LCD_DATA7 <= '0';
            s_etat_prochain <= etat_rien;
            
        WHEN etat_conf =>
            LCD_EN <= '1';
            LCD_RS <='0';
            LCD_RW <= '0';
            LCD_DATA0 <= '0';
            LCD_DATA1 <= '0';
            LCD_DATA2 <= '0';
            LCD_DATA3 <= '1';
            LCD_DATA4 <= '1';
            LCD_DATA5 <= '1';
            LCD_DATA6 <= '0';
            LCD_DATA7 <= '0';
            s_etat_prochain <= etat_conf2;
            
        WHEN etat_conf2 =>
            LCD_EN <= '0';
            LCD_RS <='0';
            LCD_RW <= '0';
            LCD_DATA0 <= '0';
            LCD_DATA1 <= '0';
            LCD_DATA2 <= '0';
            LCD_DATA3 <= '0';
            LCD_DATA4 <= '1';
            LCD_DATA5 <= '1';
            LCD_DATA6 <= '0';
            LCD_DATA7 <= '0';
            s_etat_prochain <= etat_allume; 
               
        WHEN etat_allume =>
            LCD_EN <= '1';
            LCD_RS <='0';
            LCD_RW <= '0';
            LCD_DATA0 <= '0';
            LCD_DATA1 <= '1';
            LCD_DATA2 <= '1';
            LCD_DATA3 <= '1';
            LCD_DATA4 <= '0';
            LCD_DATA5 <= '0';
            LCD_DATA6 <= '0';
            LCD_DATA7 <= '0';
            s_etat_prochain <= etat_allume2;
            
         WHEN etat_allume2 =>
            LCD_EN <= '0';
            LCD_RS <='0';
            LCD_RW <= '0';
            LCD_DATA0 <= '0';
            LCD_DATA1 <= '1';
            LCD_DATA2 <= '1';
            LCD_DATA3 <= '1';
            LCD_DATA4 <= '0';
            LCD_DATA5 <= '0';
            LCD_DATA6 <= '0';
            LCD_DATA7 <= '0';
            s_etat_prochain <= etat_mode;   
            
        WHEN etat_mode =>
            LCD_EN <= '1';
            LCD_RS <='0';
            LCD_RW <= '0';
            LCD_DATA0 <= '0';
            LCD_DATA1 <= '1';
            LCD_DATA2 <= '1';
            LCD_DATA3 <= '0';
            LCD_DATA4 <= '0';
            LCD_DATA5 <= '0';
            LCD_DATA6 <= '0';
            LCD_DATA7 <= '0';
            s_etat_prochain <= etat_mode2;
            
        WHEN etat_mode2 =>
            LCD_EN <= '0';
            LCD_RS <='0';
            LCD_RW <= '0';
            LCD_DATA0 <= '0';
            LCD_DATA1 <= '1';
            LCD_DATA2 <= '1';
            LCD_DATA3 <= '0';
            LCD_DATA4 <= '0';
            LCD_DATA5 <= '0';
            LCD_DATA6 <= '0';
            LCD_DATA7 <= '0';
            s_etat_prochain <= etat_ecrit;
            
        WHEN etat_ecrit =>
            LCD_EN <= '1';
            LCD_RS <='1';
            LCD_RW <= '0';
            LCD_DATA0 <= '1';
            LCD_DATA1 <= '0';
            LCD_DATA2 <= '0';
            LCD_DATA3 <= '0';
            LCD_DATA4 <= '0';
            LCD_DATA5 <= '0';
            LCD_DATA6 <= '1';
            LCD_DATA7 <= '0';
            s_etat_prochain <= etat_rien;
    end case;
end process;


end Behavioral;
