.cpu cortex-m3
.thumb

.section .text
.globl asm_reg
.globl asm_apsr
.globl asm_add

#.type asm_code,@function


asm_reg:
	str R1,[R0]
	bx lr

asm_apsr:
	mrs r7, apsr
	mov r0,r7
	bx lr

asm_add:
	mov R4,#0
	mov R4,R0
	nop
	nop
	nop
	nop
	add R4, #1
	add R4, #2
	add R4, #3
	add R4, #4
	add R4, #5
	add R4, #6
	add R4, #7
	add R4, #8
	add R4, #9
	nop
	mov R0, R4
	bx lr
