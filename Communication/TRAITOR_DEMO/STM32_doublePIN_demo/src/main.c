/**
* @Author: helene, ronan
* @Date:   09-08-2016
* @Email:  helene.lebouder@inria.fr, ronan.lashermes@inria.fr
* @Last modified by:   ronan
* @Last modified time: 19-09-2016
* @License: GPL
*/

/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include <stdbool.h>
#include <string.h>

/** @addtogroup STM32F1xx_HAL_Examples
  * @{
  */

/** @addtogroup Templates
  * @{
  */

/* Private typedef -----------------------------------------------------------*/
/* Private define ------------------------------------------------------------*/
/* Private macro -------------------------------------------------------------*/
/* Private variables ---------------------------------------------------------*/
static GPIO_InitTypeDef  GPIO_InitStruct;
UART_HandleTypeDef UartHandle;
const char *welcome = "Shall we play a game?\n";
uint8_t command[1];//single byte commands
uint8_t PIN[4];//single byte commands
uint8_t ref[4];//single byte commands
char text[256];
int result=0;
int test;
int rg=18;
int id;
int tableau[30];
/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);
static void Error_Handler(void);
void OutputMCO();
void asm_reg(int, int);
bool check_result(uint8_t *);
int asm_apsr();
int asm_add(int);

/* Private functions ---------------------------------------------------------*/
//#ifdef __GNUC__
/* With GCC/RAISONANCE, small printf (option LD Linker->Libraries->Small printf
   set to 'Yes') calls __io_putchar() */
//#define PUTCHAR_PROTOTYPE int __io_putchar(int ch)
//#else
#define PUTCHAR_PROTOTYPE int fputc(int ch, FILE *f)
//#endif
/**
  * @brief  Main program
  * @param  None
  * @retval None
  */
int main(void)
{
  int verbose = 0;
  /* STM32F1xx HAL library initialization:
       - Configure the Flash prefetch
       - Systick timer is configured by default as source of time base, but user
         can eventually implement his proper time base source (a general purpose
         timer for example or other time source), keeping in mind that Time base
         duration should be kept 1ms since PPP_TIMEOUT_VALUEs are defined and
         handled in milliseconds basis.
       - Set NVIC Group Priority to 4
       - Low Level Initialization
     */
  HAL_Init();
  /* Configure the system clock to 24 MHz */
  SystemClock_Config();

  /* Ouput MCO signal on PIN PA8*/
  OutputMCO();

  /* -1- Enable each GPIO Clock (to be able to program the configuration registers) */

    LED3_GPIO_CLK_ENABLE();
    LED4_GPIO_CLK_ENABLE();

    GPIO_InitStruct.Mode  = GPIO_MODE_OUTPUT_PP;
    GPIO_InitStruct.Pull  = GPIO_PULLUP;
    GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_HIGH;


  /* -2- Configure IOs in output push-pull mode to drive external LEDs */
    HAL_GPIO_TogglePin(LED3_GPIO_PORT, LED3_PIN);
    HAL_GPIO_TogglePin(LED4_GPIO_PORT, LED4_PIN);

    GPIO_InitStruct.Pin = LED3_PIN;
    HAL_GPIO_Init(LED3_GPIO_PORT, &GPIO_InitStruct);
    HAL_GPIO_WritePin(GPIOC, GPIO_PIN_8, GPIO_PIN_RESET);

    GPIO_InitStruct.Pin = LED4_PIN;
    HAL_GPIO_Init(LED4_GPIO_PORT, &GPIO_InitStruct);
    HAL_GPIO_WritePin(GPIOC, GPIO_PIN_9, GPIO_PIN_RESET);


    __HAL_RCC_GPIOC_CLK_ENABLE();
    GPIO_InitStruct.Pull  = GPIO_NOPULL;
    GPIO_InitStruct.Pin = GPIO_PIN_6;
    HAL_GPIO_Init(GPIOC, &GPIO_InitStruct);

  /*##-1- Configure the UART peripheral ######################################*/

  /* Put the USART peripheral in the Asynchronous mode (UART Mode) */
  /* UART configured as follows:
      - Word Length = 8 Bits (8 data bit + 0 parity bit)
      - Stop Bit    = One Stop bit
      - Parity      = no parity
      - BaudRate    = 115200 baud
      - Hardware flow control disabled (RTS and CTS signals) */
  UartHandle.Instance        = USARTx;
  // TEST DE CHANGEMENT HORLOGE UART //
  UART_HandleTypeDef(UART_CLOCKSOURCE_HSE);

  UartHandle.Init.BaudRate   = 115200;
  UartHandle.Init.WordLength = UART_WORDLENGTH_8B;
  UartHandle.Init.StopBits   = UART_STOPBITS_1;
  UartHandle.Init.Parity     = UART_PARITY_NONE;
  UartHandle.Init.HwFlowCtl  = UART_HWCONTROL_NONE;
  UartHandle.Init.Mode       = UART_MODE_TX_RX;
  //UartHandle.Instance->BRR=69;

  if(HAL_UART_DeInit(&UartHandle) != HAL_OK)
  {
    Error_Handler();
  }
  if (HAL_UART_Init(&UartHandle) != HAL_OK)
  {
    /* Initialization Error */
    Error_Handler();
  }

  //Send welcoming message
  HAL_UART_Transmit(&UartHandle, (uint8_t *)welcome, strlen(welcome), 0xFFFF);

// initialisation d'un tableau de 20 cases


  /* Infinite loop */
  while (1)
  {
    ref[0]=1+48;
    ref[1]=2+48;
    ref[2]=3+48;
    ref[3]=4+48;


    for (id=0;id<30;id++)
  	{
  		tableau[id]=7*id;
  	}
    //read command
    if(HAL_UART_Receive(&UartHandle, &command[0], 1, 0xFFFF) == HAL_OK)
    {
      switch (command[0]) {
        case 't'://test
          sprintf(text, "STM32 AES Test OK\n");
          HAL_UART_Transmit(&UartHandle, (uint8_t *)text, strlen(text), 0xFFFF);
          break;
        case 'h' : // Code Pin double verification
          sprintf(text, "\n Entrez votre code PIN\n");
          HAL_UART_Transmit(&UartHandle, (uint8_t *)text, strlen(text), 0xFFFF);
          // reception du code PIN
          HAL_UART_Receive(&UartHandle, &PIN[0], 4, 0xFFFF);
          // PIN[0]=6;
          // PIN[1]=6;
          // PIN[2]=6;
          // PIN[3]=6;
      	  asm_reg(0x40011000,0x43444444); // reset de la valeur pour le trigger
      	  HAL_GPIO_WritePin(GPIOC, GPIO_PIN_6, GPIO_PIN_SET); // trigger on

      	  if (check_result(PIN)){
            test=0;
            HAL_GPIO_WritePin(GPIOC, GPIO_PIN_9, GPIO_PIN_SET);
            HAL_GPIO_WritePin(GPIOC, GPIO_PIN_8, GPIO_PIN_RESET);
            if (check_result(PIN)){
              test=1;
              sprintf(text, "%d\r Code PIN OK\n");
              HAL_GPIO_WritePin(GPIOC, GPIO_PIN_9, GPIO_PIN_SET);
        	  }
        	  else
            {
          		test=2;
              sprintf(text, "%d\r Code PIN OK not OK - Malfunction\n");
              HAL_GPIO_WritePin(GPIOC, GPIO_PIN_8, GPIO_PIN_SET);
        	  }
      	  }
      	  else
      	  {
            test=3;
            HAL_GPIO_WritePin(GPIOC, GPIO_PIN_8, GPIO_PIN_SET);
            HAL_GPIO_WritePin(GPIOC, GPIO_PIN_9, GPIO_PIN_RESET);

            if (check_result(PIN)){
              test=4;
              sprintf(text, "%d\r Code PIN not OK OK - Malfunction\n");
              HAL_GPIO_WritePin(GPIOC, GPIO_PIN_9, GPIO_PIN_SET);
        	  }
        	  else
        	  {
              test=5;
              sprintf(text, "%d\r Code PIN not OK\n");
              HAL_GPIO_WritePin(GPIOC, GPIO_PIN_8, GPIO_PIN_SET);
        	  }
      	  }
            // sprintf(text, "%d\n\r",test);
            HAL_UART_Transmit(&UartHandle, (uint8_t *)text, strlen(text), 0xFFFF); //envoie les données
          // Reset PIN
	        HAL_GPIO_WritePin(GPIOC, GPIO_PIN_6, GPIO_PIN_RESET); // trigger OFF
        	break;
        case 's':
          verbose = 0;
          break;
        case 'v':
          verbose = 1;
          break;
        default:
          sprintf(text, "Unknown command: %c\n", command[0]);
          HAL_UART_Transmit(&UartHandle, (uint8_t *)text, strlen(text), 0xFFFF);
          break;
      }

    }
    else
    {
      //sprintf(text, "Didn't heard\n");
      //HAL_UART_Transmit(&UartHandle, (uint8_t *)text, strlen(text), 0xFFFF);
    }
  }
}

/**
  * @brief  Retargets the C library printf function to the USART.
  * @param  None
  * @retval None
  */
PUTCHAR_PROTOTYPE
{
  /* Place your implementation of fputc here */
  /* e.g. write a character to the USART1 and Loop until the end of transmission */
  HAL_UART_Transmit(&UartHandle, (uint8_t *)&ch, 1, 0xFFFF);

  return ch;
}


// Verification code PIN
bool check_result(uint8_t * res) {
	if (res[0] == ref[0] & res[1]== ref[1] & res[2]== ref[2] & res[3]== ref[3] ){
		return true;
	  }
	  else
	  {
		return false;
	  }
}



/*****************************************************************************************
  * @brief  System Clock Configuration
  *         The system Clock is configured as follow :
  *            System Clock source            = PLL (HSE)
  *            SYSCLK(Hz)                     = 24000000
  *            HCLK(Hz)                       = 24000000
  *            AHB Prescaler                  = 1
  *            APB1 Prescaler                 = 1
  *            APB2 Prescaler                 = 1
  *            HSE Frequency(Hz)              = 8000000
  *            HSE PREDIV1                    = 2
  *            PLLMUL                         = 6
  *            Flash Latency(WS)              = 0
  * @param  None
  * @retval None
  */
void SystemClock_Config(void)
{
  RCC_ClkInitTypeDef clkinitstruct = {0};
  RCC_OscInitTypeDef oscinitstruct = {0};

  /**************** Horloge HSE + PLL ***********************/
  /* Enable HSE Oscillator and activate PLL with HSE as source */
  // oscinitstruct.OscillatorType  = RCC_OSCILLATORTYPE_HSE;
  // oscinitstruct.HSEState        = RCC_HSE_ON;
  // oscinitstruct.HSEPredivValue  = RCC_HSE_PREDIV_DIV2;
  // oscinitstruct.PLL.PLLState    = RCC_PLL_ON;
  // oscinitstruct.PLL.PLLSource   = RCC_PLLSOURCE_HSE;
  // oscinitstruct.PLL.PLLMUL      = RCC_PLL_MUL6;
  // if (HAL_RCC_OscConfig(&oscinitstruct)!= HAL_OK)
  // {
  //   /* Initialization Error */
  //   while(1);
  // }
  //
  // /* Select PLL as system clock source and configure the HCLK, PCLK1 and PCLK2
  //    clocks dividers */
  // clkinitstruct.ClockType = (RCC_CLOCKTYPE_SYSCLK | RCC_CLOCKTYPE_HCLK | RCC_CLOCKTYPE_PCLK1 | RCC_CLOCKTYPE_PCLK2);
  // clkinitstruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
  // clkinitstruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  // clkinitstruct.APB2CLKDivider = RCC_HCLK_DIV1;
  // clkinitstruct.APB1CLKDivider = RCC_HCLK_DIV1;
  // if (HAL_RCC_ClockConfig(&clkinitstruct, FLASH_LATENCY_0)!= HAL_OK)
  // {
  //   /* Initialization Error */
  //   while(1);
  // }
  /******************************************************************/

/**************** Horloge HSE sans PLL ***********************/
  /* Enable HSE Oscillator and activate PLL with HSE as source */
  oscinitstruct.OscillatorType  = RCC_OSCILLATORTYPE_HSE;
  oscinitstruct.HSEState        = RCC_HSE_ON;
  //oscinitstruct.HSEState        = RCC_HSE_BYPASS;
  oscinitstruct.HSEPredivValue  = RCC_HSE_PREDIV_DIV1;
  if (HAL_RCC_OscConfig(&oscinitstruct)!= HAL_OK)
  {
    /* Initialization Error */
    while(1);
  }

  /* Select PLL as system clock source and configure the HCLK, PCLK1 and PCLK2
     clocks dividers */
  clkinitstruct.ClockType = (RCC_CLOCKTYPE_SYSCLK | RCC_CLOCKTYPE_HCLK | RCC_CLOCKTYPE_PCLK1 | RCC_CLOCKTYPE_PCLK2);
  clkinitstruct.SYSCLKSource = RCC_SYSCLKSOURCE_HSE;
  clkinitstruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  clkinitstruct.APB2CLKDivider = RCC_HCLK_DIV1;
  clkinitstruct.APB1CLKDivider = RCC_HCLK_DIV1;
  if (HAL_RCC_ClockConfig(&clkinitstruct, FLASH_LATENCY_0)!= HAL_OK)
  {
    /* Initialization Error */
    while(1);
  }
  /******************************************************************/
}


/******************************************************************************
* Cortex M3 - STM32F1 Discovery board code spew out one of the clocks on PA.8
*******************************************************************************/

void OutputMCO() {
GPIO_InitTypeDef GPIO_InitStruct;

//RCC_AHBPeriphClockCmd(RCC_AHBPeriph_GPIOA, ENABLE);
//RCC_APB2PeriphClockCmd(HAL_RCC_GPIOA, ENABLE);
__HAL_RCC_GPIOA_CLK_ENABLE();
// Output clock on MCO pin ---------------------------------------------//

GPIO_InitStruct.Pin = GPIO_PIN_8;
GPIO_InitStruct.Mode = GPIO_MODE_AF_PP;
GPIO_InitStruct.Speed = GPIO_SPEED_HIGH;
//GPIO_InitStruct.OType = GPIO_OType_PP;
GPIO_InitStruct.Pull = GPIO_NOPULL;
HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);

// pick one of the clocks to spew
//RCC_MCOConfig(RCC_MCOSource_SYSCLK); // Put on MCO pin the: System clock selected
//HAL_RCC_MCOConfig(RCC_MCO1,RCC_MCOSOURCE_HSE, RCC_MCODIV_1); // Put on MCO pin the: freq. of external crystal
HAL_RCC_MCOConfig(RCC_MCO1,RCC_MCO1SOURCE_SYSCLK, RCC_MCODIV_1);
}



/**
  * @brief  This function is executed in case of error occurrence.
  * @param  None
  * @retval None
  */
static void Error_Handler(void)
{
  while (1)
  {
  }
}

#ifdef  USE_FULL_ASSERT

/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t* file, uint32_t line)
{
  /* User can add his own implementation to report the file name and line number,
     ex: printf("Wrong parameters value: file %s on line %d\n", file, line) */

  /* Infinite loop */
  while (1)
  {
  }
}
#endif

/**
  * @}
  */

/**
  * @}
  */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
